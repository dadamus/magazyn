<?php

namespace ERP\Travel\Domain;

class TravelProduct
{
    private int $id;

    private string $name;

    private int $planned;

    private int $scanned;

    private int $correctionValue;

    public function __construct(int $id, string $name, int $planned, int $scanned, int $correctionValue)
    {
        $this->id = $id;
        $this->name = $name;
        $this->planned = $planned;
        $this->scanned = $scanned;
        $this->correctionValue = $correctionValue;
    }

    public static function createFromDB(array $data): TravelProduct
    {
        return new self(
            $data['id'],
            $data['name'],
            (int)$data['planned'],
            (int)$data['scanned'],
            (int)$data['correction_value']
        );
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPlanned(): int
    {
        return $this->planned;
    }

    public function getScanned(): int
    {
        return $this->scanned;
    }

    public function getCorrectionValue(): int
    {
        return $this->correctionValue;
    }

    public function style(): string
    {
        if ($this->planned > 0 && $this->planned === $this->scanned) {
            return 'bg-green-turquoise';
        }

        if ($this->planned > 0 && $this->scanned > $this->planned) {
            return 'bg-red-mint';
        }

        if ($this->planned > 0 && $this->planned > $this->scanned) {
            return 'bg-yellow-lemon';
        }

        if ($this->planned === 0 && $this->scanned > 0) {
            return 'bg-grey-mint';
        }

        if ($this->planned > 0 && $this->scanned === 0) {
            return 'bg-blue-soft';
        }

        return '';
    }
}
