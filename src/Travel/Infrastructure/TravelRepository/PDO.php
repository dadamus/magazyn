<?php

namespace ERP\Travel\Infrastructure\TravelRepository;

use DateTimeImmutable;
use Doctrine\DBAL\Connection;
use ERP\SharedKernel\Infrastructure\Exception\TravelNotFound;
use ERP\Travel\Domain\TravelProduct;
use ERP\Travel\Infrastructure\TravelRepositoryInterface;

class PDO implements TravelRepositoryInterface
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function get(int $travelId): array
    {
        $data = $this->connection->fetchAssociative('
            SELECT
            t.*,
            tr.reported,
            tr.text as report_text
            FROM
            travel t
            LEFT JOIN travel_report tr on t.id = tr.travel_id
            WHERE
            t.id = :travelId
        ', [
            'travelId' => $travelId
        ]);

        if (!$data) {
            throw new TravelNotFound($travelId);
        }

        $data['started_at'] = new DateTimeImmutable($data['started_at']);

        return $data;
    }

    public function add(string $name, DateTimeImmutable $date, int $carId, int $authorId, string $description = ''): int
    {
        $this->connection->executeStatement('
            INSERT INTO
            travel
            (name, description, author_id, car_id, started_at, created_at, modified_at, modified_by)
            VALUES 
            (:name, :description, :authorId, :carId, :startedAt, :createdAt, :modifiedAt, :modifiedBy)
        ', [
            'name' => $name,
            'description' => $description,
            'authorId' => $authorId,
            'carId' => $carId,
            'startedAt' => $date->format('Y-m-d H:i:s'),
            'createdAt' => date('Y-m-d H:i:s'),
            'modifiedAt' => date('Y-m-d H:i:s'),
            'modifiedBy' => $authorId
        ]);

        return $this->connection->lastInsertId();
    }

    public function edit(
        int               $travelId,
        string            $name,
        DateTimeImmutable $date,
        int               $modifiedBy,
        int               $carId,
        string            $description = ''
    ): void
    {
        $this->connection->executeStatement('
            UPDATE travel
            SET name = :name, description = :description, car_id = :carId, 
                started_at = :startedAt, modified_at = :modifiedAt, modified_by = :modifiedBy
            WHERE
            id = :travelId
        ', [
            'travelId' => $travelId,
            'name' => $name,
            'description' => $description,
            'carId' => $carId,
            'startedAt' => $date->format('Y-m-d H:i:s'),
            'modifiedAt' => date('Y-m-d H:i:s'),
            'modifiedBy' => $modifiedBy
        ]);
    }

    public function addProduct(int $travelId, int $productId): void
    {
        $checkProductQuery = $this->connection->fetchAssociative('
            SELECT
            product_id
            FROM
            travel_product
            WHERE
            travel_id = :travelId
            and product_id = :productId
        ', [
            'travelId' => $travelId,
            'productId' => $productId
        ]);

        if ($checkProductQuery !== false) {
            return;
        }

        $this->connection->executeStatement('
            INSERT INTO
            travel_product (travel_id, product_id)
            VALUES (:travelId, :productId)
        ', [
            'travelId' => $travelId,
            'productId' => $productId
        ]);
    }

    public function removeProduct(int $travelId, int $productId): void
    {
        $this->connection->executeStatement(
            'DELETE FROM travel_product WHERE travel_id = :travelId AND product_id = :productId',
            [
                'travelId' => $travelId,
                'productId' => $productId
            ]);
    }

    /**
     * @return TravelProduct[]
     */
    public function getProducts(int $travelId): array
    {
        $data = $this->connection->fetchAllAssociative('
            SELECT
            (
                SELECT
                    count(*)
                FROM
                    product_code_view pcv
                LEFT JOIN `code` c ON c.id = pcv.code_id
                WHERE
                    pcv.travel_id = tp.travel_id
                    AND pcv.product_id = p.id
                    AND c.type_id = 1
                GROUP BY 
                    pcv.product_id
            ) as scanned,
            p.id,
            p.name,
            tp.planned,
            (
                SELECT
            	    SUM(psl2.amount)
            	FROM
            	    product_code_view pcv2
            	LEFT JOIN `code` c ON c.id = pcv2.code_id
            	LEFT JOIN product_state_log psl2 ON psl2.code_id = c.id
            	WHERE
            	    pcv2.travel_id = tp.travel_id
            	AND pcv2.product_id = tp.product_id
            	AND c.type_id = 5
            ) AS correction_value
            FROM
            travel_product tp
            LEFT JOIN `product` p ON p.id = tp.product_id
            WHERE
            tp.travel_id = :travelId
        ', [
            'travelId' => $travelId
        ]);

        $response = [];

        foreach ($data as $row) {
            $response[] = TravelProduct::createFromDB($row);
        }

        return $response;
    }

    public function setReport(int $travelId, bool $reported, int $userId, string $text = ''): void
    {
        $data = $this->connection->fetchAssociative('
            SELECT
            travel_id
            FROM
            travel_report
            WHERE
            travel_id = :travelId
        ', [
            'travelId' => $travelId
        ]);

        if (!$data) {
            $this->connection->executeStatement('
                INSERT INTO
                travel_report
                (travel_id, reported, text, modified_at, modified_by) 
                VALUES (:travelId, :reported, :text, :modifiedAt, :modifiedBy)
            ', [
                'travelId' => $travelId,
                'reported' => (int)$reported,
                'text' => $text,
                'modifiedAt' => date('Y-m-d H:i:s'),
                'modifiedBy' => $userId
            ]);

            return;
        }

        $this->connection->executeStatement('
                UPDATE travel_report 
                SET
                reported = :reported,
                text = :text,
                modified_at = :modifiedAt,
                modified_by = :modifiedBy
                WHERE
                travel_id = :travelId
            ', [
            'travelId' => $travelId,
            'reported' => (int)$reported,
            'text' => $text,
            'modifiedAt' => date('Y-m-d H:i:s'),
            'modifiedBy' => $userId
        ]);
    }

    public function setPlanned(int $travelId, int $productId, int $quantity): void
    {
        $this->connection->executeStatement('
            UPDATE travel_product 
            SET planned = :planned 
            WHERE travel_id = :travelId AND product_id = :productId
        ', [
            'planned' => $quantity,
            'travelId' => $travelId,
            'productId' => $productId
        ]);
    }

}
