<?php

namespace ERP\Travel\Infrastructure;

use DateTimeImmutable;

interface TravelRepositoryInterface
{
    public function get(int $travelId): array;

    public function add(
        string $name,
        DateTimeImmutable $date,
        int $carId,
        int $authorId,
        string $description = ''
    ): int;

    public function edit(
        int $travelId,
        string $name,
        DateTimeImmutable $date,
        int $modifiedBy,
        int $carId,
        string $description = ''
    ): void;

    public function addProduct(int $travelId, int $productId): void;

    public function removeProduct(int $travelId, int $productId): void;

    public function getProducts(int $travelId): array;

    public function setReport(int $travelId, bool $reported, int $userId, string $text = ''): void;

    public function setPlanned(int $travelId, int $productId, int $quantity): void;
}
