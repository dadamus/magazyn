<?php

declare(strict_types=1);

namespace ERP\Travel\Application\Controller;

use ERP\Code\Domain\CodeGenerators\TravelCodeGenerator;
use ERP\Code\Domain\Printer;
use ERP\Code\Infrastructure\PrinterInterface;
use ERP\Code\Infrastructure\ProductCodeRepositoryInterface;
use ERP\SharedKernel\Application\Controller\CoreController;
use ERP\SharedKernel\Domain\DataTable\Data\Post;
use ERP\SharedKernel\Domain\DataTable\Data\ReactJson;
use ERP\SharedKernel\Domain\DataTable\Engine;
use ERP\SharedKernel\Domain\DataTableTranslator\TravelProductCodeDataTranslatorAbstract;
use ERP\SharedKernel\Domain\DataTableTranslator\TravelProductsDataTranslatorAbstract;
use ERP\SharedKernel\Domain\DataTableTranslator\TravelsDataTranslatorAbstract;
use ERP\SharedKernel\Domain\Security\User;
use ERP\SharedKernel\Infrastructure\DataTableQuery\TravelProductCode;
use ERP\SharedKernel\Infrastructure\DataTableQuery\TravelProducts as TravelProductsDataTableQuery;
use ERP\Travel\Infrastructure\TravelRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CardApiController extends CoreController
{
    /**
     * @Route("/api/travel/card/{travelId}", methods={"GET"})
     */
    public function indexAction(TravelRepositoryInterface $travels, int $travelId): Response
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $allVisible = false;
        if (in_array('ROLE_USER', $user->getRoles())) {
            $allVisible = true;
        }

        return new JsonResponse([
            'travel' => $travels->get($travelId),
            'allVisible' => $allVisible
        ]);
    }

    /**
     * @Route("/api/travel/card/{travelId}/list/filter", methods={"POST"})
     */
    public function listAction(
        int                                  $travelId,
        Engine                               $dataTable,
        TravelProductsDataTableQuery         $query,
        TravelProductsDataTranslatorAbstract $translator,
        Request                              $request
    ): Response
    {
        $filters = new ReactJson($request);
        $filters->addColumnFilter('travel_id', 'number-strict', $travelId);

        $dataTable->setFilterData($filters);
        $dataTable->setQueryBuilder($query);
        $dataTable->init($translator);

        return new JsonResponse($dataTable->search());
    }


    /**
     * @Route("/api/travel/card/{travelId}/codes/{productId}/list", methods={"POST"})
     */
    public function productCodeListAction(int $travelId, int $productId, Engine $dataTable, Request $request): Response
    {
        $query = new TravelProductCode($travelId, $productId);
        $translator = new TravelProductCodeDataTranslatorAbstract();

        $dataTable->setFilterData(new Post($request));
        $dataTable->setQueryBuilder($query);
        $dataTable->init($translator);

        return new JsonResponse($dataTable->search());
    }

    /**
     * @Route("/api/travel/card/{travelId}/codes/{productId}/remove", methods={"DELETE"})
     */
    public function removeProductTravelCodeAction(
        int                       $travelId,
        int                       $productId,
        TravelRepositoryInterface $travelRepository
    ): Response
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $travelRepository->removeProduct($travelId, $productId);

        return new JsonResponse('Travel product code removed!');
    }

    /**
     * @Route("/api/travel/card/{travelId}/report", methods={"POST"})
     */
    public function reportTravelAction(TravelRepositoryInterface $travels, int $travelId, Request $request): Response
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        $reported = $request->request->getBoolean('reported');
        $reportText = $request->request->get('report-text');

        $travels->setReport($travelId, $reported, $user->getUserId(), $reportText);

        return new JsonResponse('Report changed!');
    }

    /**
     * @Route("/api/travel/card/{travelId}/product", methods={"POST"})
     */
    public function addProduct(Request $request, TravelRepositoryInterface $travels, int $travelId): Response
    {
        $requestData = json_decode($request->getContent(), true);

        $productId = (int)$requestData['product_id'];
        $travels->addProduct($travelId, $productId);
        $plannedValue = $requestData['planned'] ?? null;

        if ($plannedValue !== null) {
            $travels->setPlanned($travelId, $productId, $plannedValue);
        }

        return new JsonResponse('Product added!');
    }

    /**
     * @Route("/api/travel/card/{travelId}/code/generate", methods={"POST"})
     */
    public function generateTravelCodeAction(
        TravelRepositoryInterface $travelRepository,
        TravelCodeGenerator       $travelCodeGenerator,
        PrinterInterface          $printer,
        int                       $travelId
    ): Response
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $code = $travelCodeGenerator->generate($travelId, $user->getUserId());
        $travel = $travelRepository->get($travelId);

        $printer->printCode($travel['name'], $code);

        return new JsonResponse();
    }

    /**
     * @Route("/api/travel/card/{travelId}/{productId}/change/quantity", methods={"POST"})
     */
    public function changePlannedQuantity(
        int                       $travelId,
        int                       $productId,
        Request                   $request,
        TravelRepositoryInterface $travels
    ): Response
    {
        $request = json_decode($request->getContent(), true);

        $travels->setPlanned($travelId, $productId, (int)$request['planned_quantity']);

        return new JsonResponse("Saved!");
    }

    /**
     * @Route("/api/travel/card/{travelId}/correction", methods={"POST"})
     */
    public function addCorrectionAction(int $travelId, Request $request, ProductCodeRepositoryInterface $codeProducts): Response
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $productId = $request->request->getInt('product-id');

        $code = $codeProducts->get($productId);
        $code->addTravelCorrection($travelId, $request->request->getInt('correction-value'), $user->getUserId());
        $codeProducts->save($code);

        return new JsonResponse('Correction added!');
    }
}
