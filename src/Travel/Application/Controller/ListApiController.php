<?php

declare(strict_types=1);

namespace ERP\Travel\Application\Controller;

use DateTimeImmutable;
use ERP\Settings\Infrastructure\CarRepositoryInterface;
use ERP\SharedKernel\Application\Controller\CoreController;
use ERP\SharedKernel\Domain\DataTable\Data\Post;
use ERP\SharedKernel\Domain\DataTable\Data\ReactJson;
use ERP\SharedKernel\Domain\DataTable\Engine;
use ERP\SharedKernel\Domain\DataTableTranslator\TravelsDataTranslatorAbstract;
use ERP\SharedKernel\Domain\Security\User;
use ERP\SharedKernel\Infrastructure\DataTableQuery\Travels as TravelDataTableQuery;
use ERP\Travel\Infrastructure\TravelRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListApiController extends CoreController
{
    /**
     * @Route("/api/travels/list/filter", methods={"POST"})
     */
    public function filterAction(
        Engine $dataTable,
        TravelDataTableQuery $query,
        TravelsDataTranslatorAbstract $translator,
        Request $request
    ): Response {
        $dataTable->setFilterData(new ReactJson($request));
        $dataTable->setQueryBuilder($query);
        $dataTable->init($translator);

        return new JsonResponse($dataTable->search());
    }

    /**
     * @Route("/api/travels/cars", methods={"GET"})
     */
    public function getCarsAction(CarRepositoryInterface $cars): JsonResponse
    {
        return new JsonResponse($cars->getAll(true));
    }

    /**
     * @Route("/api/travel", methods={"POST"})
     */
    public function addTravel(TravelRepositoryInterface $travels, Request $request): JsonResponse
    {
        $requestData = json_decode($request->getContent(false), true);

        $name = $requestData['name'];
        $description = $requestData['description'] ?? '';

        $date = new DateTimeImmutable($requestData['date']);
        $carId = (int)$requestData['car_id'];

        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $travels->add($name, $date, $carId, $user->getUserId(), $description);

        return new JsonResponse('Travel added!');
    }
}
