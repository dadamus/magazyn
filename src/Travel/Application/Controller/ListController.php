<?php

namespace ERP\Travel\Application\Controller;

use ERP\SharedKernel\Application\Controller\CoreController;
use ERP\SharedKernel\Domain\DataTable\Data\Post;
use ERP\SharedKernel\Domain\DataTable\Engine;
use ERP\SharedKernel\Domain\DataTableTranslator\TravelsDataTranslatorAbstract;
use ERP\SharedKernel\Domain\Security\User;
use ERP\SharedKernel\Infrastructure\DataTableQuery\Travels as TravelDataTableQuery;
use ERP\Settings\Infrastructure\CarRepositoryInterface;
use ERP\Travel\Infrastructure\TravelRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTimeImmutable;

class ListController extends CoreController
{
    /**
     * @Route("/travels/list", methods={"GET"}, name="travels_list")
     */
    public function indexAction(CarRepositoryInterface $cars): Response
    {
        return $this->render('Travel/travels_list.html.twig', [
            'cars' => $cars->getAll(true)
        ]);
    }

    /**
     * @Route("/travels/list/filter", methods={"POST"}, name="travels_list_filter")
     */
    public function filterAction(
        Engine $dataTable,
        TravelDataTableQuery $query,
        TravelsDataTranslatorAbstract $translator,
        Request $request
    ): Response {
        $dataTable->setFilterData(new Post($request));
        $dataTable->setQueryBuilder($query);
        $dataTable->init($translator);

        return new JsonResponse($dataTable->search());
    }

    /**
     * @Route("/travels/modal/{travelId}", methods={"GET"}, defaults={"travelId" = 0}, name="travels_modal")
     */
    public function showProductModalAction(TravelRepositoryInterface $travels, CarRepositoryInterface $cars, int $travelId = 0): Response
    {
        $travel = [];

        if ($travelId > 0) {
            $travel = $travels->get($travelId);
        }

        return $this->render('Travel/modal_content.html.twig', [
            'travel' => $travel,
            'cars' => $cars->getAll(true)
        ]);
    }

    /**
     * @Route("/travels", methods={"POST"}, name="travels_add")
     */
    public function addTravel(TravelRepositoryInterface $travels, Request $request): Response
    {
        $name = $request->request->get('name');
        $description = $request->request->get('description');

        $date = DateTimeImmutable::createFromFormat('d-m-Y', $request->request->get('date'));
        $carId = $request->request->getInt('car_id');

        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $travels->add($name, $date, $carId, $user->getUserId(), $description);

        return new JsonResponse('Travel added!');
    }

    /**
     * @Route("/travels/{travelId}", methods={"PUT"}, name="travels_edit")
     */
    public function editTravel(TravelRepositoryInterface $travels, int $travelId, Request $request): Response
    {
        $name = $request->request->get('name');
        $description = $request->request->get('description');
        $date = new DateTimeImmutable($request->request->get('date'));
        $carId = $request->request->getInt('car_id');

        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $travels->edit($travelId, $name, $date, $user->getUserId(), $carId, $description);

        return new JsonResponse('Travel added!');
    }
}
