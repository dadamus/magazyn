<?php

namespace ERP\Travel\Application\Controller;

use ERP\SharedKernel\Application\Controller\CoreController;
use ERP\Code\Domain\CodeGenerators\TravelCodeGenerator;
use ERP\Code\Domain\Printer;
use ERP\Code\Infrastructure\ProductCodeRepositoryInterface;
use ERP\SharedKernel\Domain\DataTable\Data\Post;
use ERP\SharedKernel\Domain\DataTable\Engine;
use ERP\SharedKernel\Domain\DataTableTranslator\TravelProductCodeDataTranslatorAbstract;
use ERP\SharedKernel\Domain\Security\User;
use ERP\SharedKernel\Infrastructure\DataTableQuery\TravelProductCode;
use ERP\Travel\Infrastructure\TravelRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CardController extends CoreController
{
    /**
     * @Route("/travel/card/{travelId}", methods={"GET"}, name="travel_card")
     */
    public function indexAction(TravelRepositoryInterface $travels, int $travelId): Response
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $allVisible = false;
        if (in_array('ROLE_USER', $user->getRoles())) {
            $allVisible = true;
        }

        return $this->render('Travel/travel_card.html.twig', [
            'travelId' => $travelId,
            'travel' => $travels->get($travelId),
            'products' => $travels->getProducts($travelId),
            'allVisible' => $allVisible
        ]);
    }

    /**
     * @Route("/travel/card/{travelId}/list", methods={"GET"}, name="travel_card_list")
     */
    public function listAction(TravelRepositoryInterface $travels, int $travelId): Response
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $allVisible = false;
        if (in_array('ROLE_USER', $user->getRoles())) {
            $allVisible = true;
        }

        return $this->render('Travel/travel_card_product_list.html.twig', [
            'products' => $travels->getProducts($travelId),
            'allVisible' => $allVisible
        ]);
    }

    /**
     * @Route("/travel/card/{travelId}/codes/{productId}", name="travel_card_product_codes_list")
     */
    public function productCodeListIndexAction(int $travelId, int $productId): Response
    {
        return $this->render('Travel/travel_card_product_codes_view.html.twig', [
            'travelId' => $travelId,
            'productId' => $productId
        ]);
    }

    /**
     * @Route("/travel/card/{travelId}/codes/{productId}/list", methods={"POST"})
     */
    public function productCodeListAction(int $travelId, int $productId, Engine $dataTable, Request $request): Response
    {
        $query = new TravelProductCode($travelId, $productId);
        $translator = new TravelProductCodeDataTranslatorAbstract();

        $dataTable->setFilterData(new Post($request));
        $dataTable->setQueryBuilder($query);
        $dataTable->init($translator);

        return new JsonResponse($dataTable->search());
    }

    /**
     * @Route("/travel/card/{travelId}/codes/{productId}/remove", methods={"DELETE"})
     */
    public function removeProductTravelCodeAction(
        int $travelId,
        int $productId,
        Request $request,
        ProductCodeRepositoryInterface $codes
    ): Response {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $product = $codes->get((string)$productId);

        $product->removeTravelCodeScanned(
            $travelId,
            $request->request->getInt('code-id'),
            $user->getUserId()
        );
        $codes->save($product);

        return new JsonResponse('Travel product code removed!');
    }

    /**
     * @Route("/travel/card/{travelId}/report", methods={"POST"}, name="travel_card_report")
     */
    public function reportTravelAction(TravelRepositoryInterface $travels, int $travelId, Request $request): Response
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        $reported = $request->request->getBoolean('reported');
        $reportText = $request->request->get('report-text');

        $travels->setReport($travelId, $reported, $user->getUserId(), $reportText);

        return new JsonResponse('Report changed!');
    }

    /**
     * @Route("/travel/card/{travelId}/product", methods={"POST"}, name="travel_card_product_add")
     */
    public function addProduct(Request $request, TravelRepositoryInterface $travels, int $travelId): Response
    {
        $travels->addProduct($travelId, $request->request->getInt('product_id'));

        return new JsonResponse('Product added!');
    }

    /**
     * @Route("/travel/card/{travelId}/code/generate", methods={"POST"}, name="travel_card_code_generate")
     */
    public function generateTravelCodeAction(
        TravelCodeGenerator $travelCodeGenerator,
        Printer $printer,
        int $travelId
    ): Response {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $code = $travelCodeGenerator->generate($travelId, $user->getUserId());

        $printerGroupId = $printer->createPrintGroup([$code]);

        return new JsonResponse($printerGroupId);
    }

    /**
     * @Route("/travel/card/{travelId}/{productId}/change/quantity", methods={"POST"})
     */
    public function changePlannedQuantity(
        int $travelId,
        int $productId,
        Request $request,
        TravelRepositoryInterface $travels
    ): Response {
        $plannedQuantity = $request->request->getInt('planned_quantity');
        $travels->setPlanned($travelId, $productId, $plannedQuantity);

        return new JsonResponse("Saved!");
    }

    /**
     * @Route("/travel/card/{travelId}/correction", methods={"POST"})
     */
    public function addCorrectionAction(int $travelId, Request $request, ProductCodeRepositoryInterface $codeProducts): Response
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $productId = $request->request->getInt('product-id');

        $code = $codeProducts->get($productId);
        $code->addTravelCorrection($travelId, $request->request->getInt('correction-value'), $user->getUserId());
        $codeProducts->save($code);

        return new JsonResponse('Correction added!');
    }
}
