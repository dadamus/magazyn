<?php

declare(strict_types=1);

namespace ERP\ScannerConsumer\Domain;

use DateTimeImmutable;
use ERP\Code\Domain\AbstractCode;
use ERP\Code\Domain\ProductCode;
use ERP\Code\Domain\CodeType;
use ERP\Settings\Domain\Device\Device;

interface CodeHandlerInterface
{
    public function support(AbstractCode $code, Device $device): bool;

    public function handle(AbstractCode $code, Device $device, DateTimeImmutable $scannedAt): void;
}
