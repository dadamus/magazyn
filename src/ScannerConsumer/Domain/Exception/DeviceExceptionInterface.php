<?php

declare(strict_types=1);

namespace ERP\ScannerConsumer\Domain\Exception;

use ERP\Settings\Domain\Device\Device;

interface DeviceExceptionInterface
{
    public function getDevice(): Device;
}
