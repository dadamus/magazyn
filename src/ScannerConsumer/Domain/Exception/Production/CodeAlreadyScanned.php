<?php

declare(strict_types=1);

namespace ERP\ScannerConsumer\Domain\Exception\Production;

use ERP\Code\Domain\ProductCode;
use ERP\ScannerConsumer\Domain\Exception\DeviceExceptionInterface;
use ERP\Settings\Domain\Device\Device;
use Exception;
use Throwable;

class CodeAlreadyScanned extends Exception implements DeviceExceptionInterface
{
    private ProductCode $productionCode;

    private Device $device;

    public function __construct(ProductCode $code, Device $device)
    {
        parent::__construct('ProductCode already scanned!');

        $this->productionCode = $code;
        $this->device = $device;
    }

    public function getProductionCode(): ProductCode
    {
        return $this->productionCode;
    }

    public function getDevice(): Device
    {
        return $this->device;
    }
}
