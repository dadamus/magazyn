<?php

declare(strict_types=1);

namespace ERP\ScannerConsumer\Domain;

use DateTimeImmutable;
use ERP\SharedKernel\Domain\DataObject;

class MessageData extends DataObject
{
    public const EAN_FIELD = 'ean';
    public const DATE_FIELD = 'date';
    public const DEVICE_HASH_FIELD = 'device_hash';

    public static function createFromJson(string $json): self
    {
        $decodedMessage = json_decode($json, true);

        if (!is_array($decodedMessage)) {
            throw new \Exception(sprintf('Wrong message json: %s', $json));
        }

        return new self($decodedMessage);
    }

    public function getEan(): string
    {
        return $this->getData(self::EAN_FIELD);
    }

    public function getMessageDate(): DateTimeImmutable
    {
        return new DateTimeImmutable($this->getData(self::DATE_FIELD));
    }

    public function getDeviceHash(): string
    {
        return $this->getData(self::DEVICE_HASH_FIELD);
    }
}
