<?php

namespace ERP\ScannerConsumer\Domain;

use DateTimeImmutable;
use ERP\Code\Domain\AbstractCode;
use ERP\Code\Domain\ProductCode;
use ERP\Code\Service\CodeService;
use ERP\Settings\Infrastructure\DeviceRepositoryInterface;
use ERP\SharedKernel\Infrastructure\Rabbit\AbstractConsumer;
use Exception;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

class ScannedCodeConsumer extends AbstractConsumer
{
    private const QUEUE_NAME = 'scanned_codes';
    private const EXCHANGE_NAME = 'scanned_codes';

    private int $messageDoneCounter = 0;
    private DateTimeImmutable $startedAt;

    public function __construct(
        string                                     $rabbitHost,
        int                                        $rabbitPort,
        string                                     $rabbitUser,
        string                                     $rabbitPassword,
        string                                     $rabbitVhost,
        private readonly CodeService               $codeService,
        private readonly DeviceRepositoryInterface $devices,
        private readonly BlinkerResponseInterface  $responseBlinker,
        private readonly LoggerInterface           $logger,
        private readonly HandlerPool               $handlerPool)
    {
        parent::__construct($rabbitHost, $rabbitPort, $rabbitUser, $rabbitPassword, $rabbitVhost);
    }

    /**
     * @param AMQPMessage $message
     * @throws Exception
     *
     */
    protected function consume(AMQPMessage $message): void
    {
        try {
            /** @var AMQPChannel $messageChannel */
            $messageChannel = $message->delivery_info['channel'];

            $messageData = MessageData::createFromJson($message->getBody());

            if ($message->getBody() !== 'timer') {
                $code = $this->getCode($messageData);
                $device = $this->devices->getByHash($messageData->getDeviceHash());
                $this->handlerPool->execute($code, $device, $messageData->getMessageDate());
            }

            $this->messageDoneCounter++;
        } catch (Exception $exception) {
            echo 'Błąd! ' . $exception->getMessage() . PHP_EOL;
            $this->logger->critical('Scanned ProductCode Consumer Exception!', [
                'message' => $exception->getMessage(),
                'trace' => $exception->getTraceAsString()
            ]);
        }

        $messageChannel->basic_ack($message->delivery_info['delivery_tag'], true);
        $this->checkConsumerDeathDate();
    }

    private function getCode(MessageData $messageData): AbstractCode
    {
        $ean = substr($messageData->getEan(), 0, -1);
        $code = $this->codeService->getCode($ean);

        if (!$code) {
            $this->responseBlinker->productionFailResponse($messageData->getDeviceHash(), $messageData->getMessageDate());
            throw new Exception(sprintf('ProductCode for ean %s not found!', $ean));
        }

        return $code;
    }

    private function checkConsumerDeathDate(): void
    {
        $currentDate = new DateTimeImmutable();
        $consumerDieDate = $this->startedAt->modify('+5min 30second');
        if ($currentDate >= $consumerDieDate) {
            exit;
        }

        if ($this->messageDoneCounter > 1000) {
            exit;
        }
    }

    public function start(): void
    {
        $this->startedAt = new DateTimeImmutable();

        echo 'init connection' . PHP_EOL;
        $this->initConnection();

        echo 'create queue' . PHP_EOL;
        $this->createQueue(self::QUEUE_NAME, self::EXCHANGE_NAME, 'fanout');

        parent::startConsumer(self::QUEUE_NAME, function (AMQPMessage $message) {
            try {
                $this->consume($message);
            } catch (Exception $ex) {
                echo $ex->getMessage() . PHP_EOL;
                $this->responseBlinker->productionFailResponse($message['device_hash'], new DateTimeImmutable());
            }
        });
    }
}
