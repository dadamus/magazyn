<?php

declare(strict_types=1);

namespace ERP\ScannerConsumer\Domain;

use DateTimeImmutable;

interface BlinkerResponseInterface
{
    public function productionSuccessResponse(string $deviceId, DateTimeImmutable $responseTime): void;

    public function productionFailResponse(string $deviceId, DateTimeImmutable $responseTime): void;

    public function travelSuccessResponse(string $deviceId, DateTimeImmutable $responseTime): void;

    public function travelEndResponse(string $deviceId, DateTimeImmutable $responseTime): void;
}
