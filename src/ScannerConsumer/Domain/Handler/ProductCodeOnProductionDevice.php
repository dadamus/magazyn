<?php

declare(strict_types=1);

namespace ERP\ScannerConsumer\Domain\Handler;

use DateTimeImmutable;
use ERP\Code\Domain\AbstractCode;
use ERP\Code\Domain\ProductCode;
use ERP\Code\Domain\ProductCodeService;
use ERP\ScannerConsumer\Domain\BlinkerResponseInterface;
use ERP\ScannerConsumer\Domain\CodeHandlerInterface;
use ERP\Settings\Domain\Device\Device;
use ERP\Settings\Domain\Device\DeviceType;

class ProductCodeOnProductionDevice implements CodeHandlerInterface
{
    private BlinkerResponseInterface $responseBlinker;

    private ProductCodeService $productCodeService;

    public function __construct(BlinkerResponseInterface $responseBlinker, ProductCodeService $productCodeService)
    {
        $this->responseBlinker = $responseBlinker;
        $this->productCodeService = $productCodeService;
    }

    public function support(AbstractCode $code, Device $device): bool
    {
        return $code instanceof ProductCode && $device->getType()->getValue() === DeviceType::PRODUCTION;
    }

    public function handle(AbstractCode $code, Device $device, DateTimeImmutable $scannedAt): void
    {
        if ($this->productCodeService->hasProductionUsage($code)) {
            $this->responseBlinker->productionFailResponse($device->getHash(), $scannedAt);

            return;
        }

//        if ($codeViewData->getPositionId() !== $device->getPositionId()) {
//            $this->responseBlinker->productionFailResponse($device->getHash(), $scannedAt);
//
//            return;
//        }

        $this->productCodeService->storeProductionUsage($code, $device, $scannedAt);

        $this->responseBlinker->productionSuccessResponse($device->getHash(), $scannedAt);
    }
}
