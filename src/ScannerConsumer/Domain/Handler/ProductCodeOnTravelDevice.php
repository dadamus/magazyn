<?php

declare(strict_types=1);

namespace ERP\ScannerConsumer\Domain\Handler;

use DateTimeImmutable;
use ERP\Code\Domain\AbstractCode;
use ERP\Code\Domain\ProductCode;
use ERP\Code\Domain\CodeType;
use ERP\Code\Domain\ProductCodeService;
use ERP\Code\Infrastructure\ProductCodeRepositoryInterface;
use ERP\ScannerConsumer\Domain\BlinkerResponseInterface;
use ERP\ScannerConsumer\Domain\CodeHandlerInterface;
use ERP\Settings\Domain\Device\Device;
use ERP\Settings\Domain\Device\DeviceType;

class ProductCodeOnTravelDevice implements CodeHandlerInterface
{
    private BlinkerResponseInterface $responseBlinker;

    private ProductCodeService $productCodeService;

    public function __construct(BlinkerResponseInterface $responseBlinker, ProductCodeService $productCodeService)
    {
        $this->responseBlinker = $responseBlinker;
        $this->productCodeService = $productCodeService;
    }

    public function support(AbstractCode $code, Device $device): bool
    {
        return $code instanceof ProductCode && $device->getType()->getValue() === DeviceType::TRAVEL;
    }

    public function handle(AbstractCode $code, Device $device, DateTimeImmutable $scannedAt): void
    {
        if (!$device->isActive()) {
            $this->responseBlinker->productionFailResponse($device->getHash(), $scannedAt);

            return;
        }

        if (!$device->getLastTravelId()) {
            $this->responseBlinker->productionFailResponse($device->getHash(), $scannedAt);

            return;
        }

        if ($this->productCodeService->hasTravelUsage($code)) {
            $this->responseBlinker->productionFailResponse($device->getHash(), $scannedAt);

            return;
        }

        $this->productCodeService->storeTravelUsage($code, $device, $scannedAt);

        $this->responseBlinker->productionSuccessResponse($device->getHash(), $scannedAt);
    }
}
