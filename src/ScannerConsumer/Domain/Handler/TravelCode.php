<?php

declare(strict_types=1);

namespace ERP\ScannerConsumer\Domain\Handler;

use DateTimeImmutable;
use ERP\Code\Domain\AbstractCode;
use ERP\Code\Domain\ProductCode;
use ERP\Code\Domain\CodeType;
use ERP\Code\Infrastructure\TravelCodeStorageInterface;
use ERP\ScannerConsumer\Domain\BlinkerResponseInterface;
use ERP\ScannerConsumer\Domain\CodeHandlerInterface;
use ERP\Settings\Domain\Device\Device;
use ERP\Settings\Domain\Device\DeviceType;
use ERP\Settings\Infrastructure\DeviceRepositoryInterface;

class TravelCode implements CodeHandlerInterface
{
    private DeviceRepositoryInterface $devices;

    private TravelCodeStorageInterface $travelCodeStorage;

    private BlinkerResponseInterface $responseBlinker;

    public function __construct(DeviceRepositoryInterface $devices, TravelCodeStorageInterface $travelCodeStorage, BlinkerResponseInterface $responseBlinker)
    {
        $this->devices = $devices;
        $this->travelCodeStorage = $travelCodeStorage;
        $this->responseBlinker = $responseBlinker;
    }

    public function support(AbstractCode $code, Device $device): bool
    {
        return $code instanceof \ERP\Code\Domain\TravelCode
            && $device->getType()->getValue() === DeviceType::TRAVEL;
    }

    public function handle(AbstractCode $code, Device $device, DateTimeImmutable $scannedAt): void
    {
        $travelCodeData = $this->travelCodeStorage->getByCode($code->getEan());

        if (!(int)$device->isActive() || (int)$device->getType()->getValue() != DeviceType::TRAVEL) {
            return;
        }

        $this->devices->addTravel($device->getId(), $travelCodeData->getTravelId());

        $this->responseBlinker->travelSuccessResponse($device->getHash(), $scannedAt);
    }
}
