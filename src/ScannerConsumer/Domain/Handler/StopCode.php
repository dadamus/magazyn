<?php

declare(strict_types=1);

namespace ERP\ScannerConsumer\Domain\Handler;

use DateTimeImmutable;
use ERP\Code\Domain\AbstractCode;
use ERP\Code\Domain\ProductCode;
use ERP\Code\Domain\CodeType;
use ERP\ScannerConsumer\Domain\BlinkerResponseInterface;
use ERP\ScannerConsumer\Domain\CodeHandlerInterface;
use ERP\Settings\Domain\Device\Device;
use ERP\Settings\Domain\Device\DeviceType;
use ERP\Settings\Infrastructure\DeviceRepositoryInterface;

class StopCode implements CodeHandlerInterface
{
    private BlinkerResponseInterface $responseBlinker;

    private DeviceRepositoryInterface $devices;

    /**
     * @param BlinkerResponseInterface $responseBlinker
     * @param DeviceRepositoryInterface $devices
     */
    public function __construct(BlinkerResponseInterface $responseBlinker, DeviceRepositoryInterface $devices)
    {
        $this->responseBlinker = $responseBlinker;
        $this->devices = $devices;
    }

    public function support(AbstractCode $code, Device $device): bool
    {
        return false;
//        return $codeType->getValue() === CodeType::STOP
//            && $device->getType()->getValue() === DeviceType::TRAVEL;
    }

    public function handle(AbstractCode $code, Device $device, DateTimeImmutable $scannedAt): void
    {
        $this->devices->clearTravel($device->getId());

        $this->responseBlinker->travelEndResponse($device->getHash(), $scannedAt);
    }
}
