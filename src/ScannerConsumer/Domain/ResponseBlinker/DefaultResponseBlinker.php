<?php

namespace ERP\ScannerConsumer\Domain\ResponseBlinker;

use DateTimeImmutable;
use ERP\ScannerConsumer\Domain\BlinkerResponseInterface;
use ERP\ScannerConsumer\Infrastructure\DeviceMessengersInterface;

class DefaultResponseBlinker implements BlinkerResponseInterface
{

    public function __construct(
        private readonly DeviceMessengersInterface $deviceMessenger
    )
    {
    }

    public function productionSuccessResponse(string $deviceId, DateTimeImmutable $responseTime): void
    {
        if ($responseTime->modify('+30 seconds') < (new DateTimeImmutable())) {
            return;
        }

        $message = [
            'type' => 'green_blink'
        ];

        echo 'GREEN_BLINK' . PHP_EOL;

        $this->deviceMessenger->sendMessage($deviceId, $message);
    }

    public function productionFailResponse(string $deviceId, DateTimeImmutable $responseTime): void
    {
        if ($responseTime->modify('+30 seconds') < (new DateTimeImmutable())) {
            return;
        }

        $message = [
            'type' => 'red_blink'
        ];

        echo 'RED_BLINK' . PHP_EOL;

        $this->deviceMessenger->sendMessage($deviceId, $message);
    }

    public function travelSuccessResponse(string $deviceId, DateTimeImmutable $responseTime): void
    {
        if ($responseTime->modify('+30 seconds') < (new DateTimeImmutable())) {
            return;
        }

        $message = [
            'type' => 'blue_start'
        ];

        echo 'BLUE_START' . PHP_EOL;

        $this->deviceMessenger->sendMessage($deviceId, $message);
    }

    public function travelEndResponse(string $deviceId, DateTimeImmutable $responseTime): void
    {
        if ($responseTime->modify('+30 seconds') < (new DateTimeImmutable())) {
            return;
        }

        $message = [
            'type' => 'blue_stop'
        ];

        echo 'BLUE_STOP' . PHP_EOL;

        $this->deviceMessenger->sendMessage($deviceId, $message);
    }
}
