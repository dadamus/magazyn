<?php

declare(strict_types=1);

namespace ERP\ScannerConsumer\Domain;

use DateTimeImmutable;
use ERP\Code\Domain\AbstractCode;
use ERP\Code\Domain\ProductCode;
use ERP\Settings\Domain\Device\Device;

class HandlerPool
{
    /**
     * @var CodeHandlerInterface[]
     */
    private array $handlers;

    private BlinkerResponseInterface $responseBlinker;

    /**
     * @param CodeHandlerInterface[] $handlers
     * @param BlinkerResponseInterface $responseBlinker
     */
    public function __construct(array $handlers, BlinkerResponseInterface $responseBlinker)
    {
        $this->handlers = $handlers;
        $this->responseBlinker = $responseBlinker;
    }

    public function execute(AbstractCode $code, Device $device, DateTimeImmutable $scannedAt): void
    {
        foreach ($this->handlers as $handler) {
            if ($handler->support($code, $device)) {
                $handler->handle($code, $device, $scannedAt);

                return;
            }
        }

        $this->responseBlinker->productionFailResponse($device->getHash(), $scannedAt);
    }
}
