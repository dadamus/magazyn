<?php

namespace ERP\ScannerConsumer\Infrastructure\DeviceMessenger;

use ERP\ScannerConsumer\Infrastructure\DeviceMessengersInterface;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

class RabbitMQ implements DeviceMessengersInterface
{
    private ?AMQPStreamConnection $connection = null;

    public function __construct(
        private readonly string          $rabbitHost,
        private readonly int             $rabbitPort,
        private readonly string          $rabbitUser,
        private readonly string          $rabbitPassword,
        private readonly string          $rabbitVhost,
        private readonly LoggerInterface $logger
    )
    {
    }

    private function connect(): void
    {
        $this->connection = new AMQPStreamConnection(
            $this->rabbitHost,
            $this->rabbitPort,
            $this->rabbitUser,
            $this->rabbitPassword,
            $this->rabbitVhost
        );
    }

    private function disconnect(): void
    {
        if ($this->connection !== null && $this->connection->isConnected()) {
            try {
                $this->connection->close();
            } catch (\Exception $ex) {
            }
        }
    }

    public function sendMessage(string $deviceId, array $message): void
    {
        $this->connect();

        $channel = $this->connection->channel();

        $this->logger->debug('Sending message: ' . json_encode($message) . ' -> ' . $deviceId);

        $message = new AMQPMessage(json_encode($message));

        $channel->basic_publish($message, 'device_finder', $deviceId);

        $this->disconnect();
    }
}
