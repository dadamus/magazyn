<?php


namespace ERP\ScannerConsumer\Infrastructure;


interface DeviceMessengersInterface
{
    public function sendMessage(string $deviceId, array $message): void;
}
