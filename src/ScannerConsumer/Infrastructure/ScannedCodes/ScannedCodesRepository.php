<?php

namespace ERP\ScannerConsumer\Infrastructure\ScannedCodes;

use DateTimeImmutable;
use Doctrine\DBAL\Connection;

class ScannedCodesRepository
{
    public function __construct(
        private readonly Connection $connection
    )
    {
    }

    public function insertCode(string $code, string $deviceCode, DateTimeImmutable $scannedAt): void
    {
        $this->connection->executeQuery('
            INSERT INTO 
                `scanned_codes` 
                (code, device_code, scanned_at) 
            VALUES (:code, :deviceCode, :scannedAt)
            ', [
            'code' => $code,
            'deviceCode' => $deviceCode,
            'scannedAt' => $scannedAt->format('Y-m-d H:i:s')
        ]);
    }
}