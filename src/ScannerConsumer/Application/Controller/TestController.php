<?php

declare(strict_types=1);

namespace ERP\ScannerConsumer\Application\Controller;

use ERP\Code\Infrastructure\ProductCodeRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /**
     * @Route("/test/aggregate/{productId}")
     */
    public function testAction(int $productId, ProductCodeRepositoryInterface $productCodeRepository)
    {
        $product = $productCodeRepository->get($productId);

        var_dump($product);

        return $this->json(['test']);
    }
}
