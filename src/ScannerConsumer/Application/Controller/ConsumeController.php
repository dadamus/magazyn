<?php

namespace ERP\ScannerConsumer\Application\Controller;

use ERP\ScannerConsumer\Domain\MessageData;
use ERP\ScannerConsumer\Infrastructure\ScannedCodes\ScannedCodesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConsumeController extends AbstractController
{
    public function __construct(
        private readonly ScannedCodesRepository $scannedCodesRepository
    )
    {
    }

    /**
     * @Route("/api/consume", methods={"POST"})
     */
    public function consumeAction(Request $request): Response
    {
        $messageData = MessageData::createFromJson($request->getContent());

        $this->scannedCodesRepository->insertCode($messageData->getEan(), $messageData->getDeviceHash(), $messageData->getMessageDate());
        return $this->json([]);
    }
}