<?php

namespace ERP\ScannerConsumer\Application\Command;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Process\Process;

class ConsumerKillerCommand extends Command
{
    protected static $defaultName = 'consumer:killer';

    /** @var KernelInterface */
    private $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;

        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('START');

        $client = $this->connect();
        $consumers = $this->getConsumers($client, getenv('RABBIT_VHOST'));
        $consumersCount = $this->count($consumers);

        $host = getenv('RABBIT_HOST');
        $port = getenv('RABBIT_PORT');
        $user = getenv('RABBIT_USER');
        $password = getenv('RABBIT_PASSWORD');
        $vhost = getenv('RABBIT_VHOST');

        $rabbitConnection = new AMQPStreamConnection($host, $port, $user, $password, $vhost);
        $channel = $rabbitConnection->channel();

        for ($i = 0; $i < $consumersCount; $i++) {
            $message = new AMQPMessage('timer');

            $channel->basic_publish($message, 'scanned_codes');
        }

        $channel->close();
    }

    /**
     * @return Client
     */
    private function connect(): Client
    {
        $host = getenv('RABBIT_HOST');
        $user = getenv('RABBIT_USER');
        $password = getenv('RABBIT_PASSWORD');

        $url = $host . ':15672/api/';

        return new Client([
            "base_uri" => $url,
            'auth' => [
                $user,
                $password
            ]
        ]);
    }

    private function count(array $consumers): int
    {
        $consumerCount = [];

        foreach ($consumers as $consumer) {
            if (!isset($consumerCount[$consumer['queue']['name']])) {
                $consumerCount[$consumer['queue']['name']] = 0;
            }

            $consumerCount[$consumer['queue']['name']] += 1;
        }

        if (isset($consumerCount['scanned_codes'])) {
            return $consumerCount['scanned_codes'];
        }

        return 0;
    }

    /**
     * @throws GuzzleException
     *
     * @param string $vhost
     *
     * @return array
     *
     * @param Client $client
     */
    private function getConsumers(Client $client, string $vhost): array
    {
        $request = $client->request("GET", "consumers/$vhost");
        $response = \GuzzleHttp\json_decode($request->getBody()->getContents(), true);

        return $response;
    }
}
