<?php

namespace ERP\ScannerConsumer\Application\Command;

use ERP\ScannerConsumer\Domain\ScannedCodeConsumer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ScannedCodesConsumerCommand extends Command
{
    private const PID_FILE_NAME_OPTION = 'pid_file_name';

    private ScannedCodeConsumer $scannedCodeConsumer;

    private string $projectRootDir;

    public function __construct(ScannedCodeConsumer $scannedCodeConsumer, string $projectRootDir)
    {
        $this->scannedCodeConsumer = $scannedCodeConsumer;
        $this->projectRootDir = $projectRootDir;
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('scanned-code-consumer:start');
        $this->addOption(self::PID_FILE_NAME_OPTION, 'pfn', InputOption::VALUE_OPTIONAL);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $pidFileName = str_replace(['\'', '"'], ['', ''], $input->getOption(self::PID_FILE_NAME_OPTION));
        if (!empty($pidFileName)) {
            file_put_contents($this->projectRootDir . '/var/consumers/' . $pidFileName, getmypid());
        }

        $output->writeln('START');
        $this->scannedCodeConsumer->start();

        return 0;
    }
}
