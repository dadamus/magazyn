<?php

namespace ERP\ScannerRestServer\Application\Controller;

use ERP\SharedKernel\Application\Controller\CoreController;
use ERP\Settings\Infrastructure\DeviceRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InitController extends CoreController
{
    /**
     * @Route("/server/device/{deviceHash}/init_data", methods={"GET"})
     */
    public function getInitState(string $deviceHash, DeviceRepositoryInterface $devices, Request $request): Response
    {
        $deviceData = $devices->getByHash($deviceHash);

        $devices->setLastSynchronizationDate($deviceData->getId(), $request->getClientIp());

        return new JsonResponse([
            'device_data' => $deviceData->getData(),
            'date' => date('Y-m-d H:i:s')
        ]);
    }
}
