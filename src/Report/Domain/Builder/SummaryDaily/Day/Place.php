<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\SummaryDaily\Day;

use DateTimeImmutable;
use ERP\Report\Domain\Data\SummaryDaily\Place as PlaceData;
use ERP\Report\Domain\Data\SummaryDaily\Detail as DetailData;
use ERP\Report\Infrastructure\SummaryDaily\Repository;
use Generator;

class Place
{
    private Repository $repository;

    private Detail $detailBuilder;

    public function __construct(Repository $repository, Detail $detailBuilder)
    {
        $this->repository = $repository;
        $this->detailBuilder = $detailBuilder;
    }

    public function build(string $name, int $id, DateTimeImmutable $date): PlaceData
    {
        $details = $this->buildDetails($date, $id);

        return new PlaceData($name, ...$details);
    }

    /**
     * @return Generator|DetailData[]
     */
    private function buildDetails(DateTimeImmutable $date, int $deviceId): Generator
    {
        $products = $this->repository->getProductsNameFromCodeRange($date);

        foreach ($products as $product) {
            $detail = $this->detailBuilder->build(
                $date,
                (int)$product[Repository::PRODUCT_ID],
                $deviceId,
                (string)$product[Repository::PRODUCT_NAME]
            );

            if (
                $detail->getBalance()->getQuantity() > 0
                || $detail->getBalance()->getQuantityWithCorrection() > 0
                || $detail->getCharged()->getQuantity() > 0
                || $detail->getCharged()->getQuantity() > 0
                || $detail->getMade()->getQuantity() > 0
                || $detail->getMade()->getQuantityWithCorrection() > 0
            ) {
                yield $detail;
            }
        }
    }
}
