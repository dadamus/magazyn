<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\SummaryDaily\Day;

use ERP\Code\Domain\CodeType;
use ERP\Report\Domain\Data\SummaryDaily\Record as RecordData;
use ERP\Report\Infrastructure\SummaryDaily\Repository;

class Record
{
    public function build(array $row): RecordData
    {
        return new RecordData(
            (string)$row[Repository::PRODUCT_NAME],
            (int)$this->isProduction($row),
            (int)($this->isProduction($row) || $this->isProductCorrection($row)),
            (int)$this->isTravel($row),
            (int)($this->isTravel($row) || $this->isTravelCorrection($row))
        );
    }

    private function isProduction(array $row): bool
    {
        return (int)$row[Repository::CODE_TYPE] === CodeType::PRODUCTION
            && $row[Repository::PRODUCTION_SCANNED_AT] !== null;
    }

    private function isProductCorrection(array $row): bool
    {
        return (int)$row[Repository::CODE_TYPE] == CodeType::PRODUCT_CORRECTION;
    }

    private function isTravel(array $row): bool
    {
        return (int)$row[Repository::CODE_TYPE] === CodeType::TRAVEL
            && $row[Repository::TRAVEL_SCANNED_AT] !== null;
    }

    private function isTravelCorrection(array $row): bool
    {
        return (int)$row[Repository::CODE_TYPE] === CodeType::TRAVEL_CORRECTION;
    }
}
