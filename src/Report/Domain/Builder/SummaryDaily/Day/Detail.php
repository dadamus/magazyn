<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\SummaryDaily\Day;

use DateTimeImmutable;
use ERP\Report\Domain\Builder\SummaryDaily\Day\Detail\Balance;
use ERP\Report\Domain\Builder\SummaryDaily\Day\Detail\Charged;
use ERP\Report\Domain\Builder\SummaryDaily\Day\Detail\Made;
use ERP\Report\Domain\Data\SummaryDaily\Detail as DetailData;
use ERP\Report\Domain\Data\SummaryDaily\Record as RecordData;
use ERP\Report\Infrastructure\SummaryDaily\Repository;
use Generator;

class Detail
{
    private Repository $repository;

    private Record $recordBuilder;

    private Balance $balanceBuilder;

    private Charged $chargedBuilder;

    private Made $madeBuilder;

    public function __construct(
        Repository $repository,
        Record $recordBuilder,
        Balance $balanceBuilder,
        Charged $chargedBuilder,
        Made $madeBuilder
    ) {
        $this->repository = $repository;
        $this->recordBuilder = $recordBuilder;
        $this->balanceBuilder = $balanceBuilder;
        $this->chargedBuilder = $chargedBuilder;
        $this->madeBuilder = $madeBuilder;
    }

    public function build(DateTimeImmutable $date, int $productId, int $deviceId, string $productName): DetailData
    {
        $records = iterator_to_array($this->createRecords($date, $productId, $deviceId));

        $made = $this->madeBuilder->build(...$records);
        $charged = $this->chargedBuilder->build(...$records);
        $balance = $this->balanceBuilder->build($made, $charged);

        return new DetailData(
            $productName,
            $made,
            $charged,
            $balance,
            ...$records
        );
    }

    /**
     * @return Generator|RecordData[]
     */
    private function createRecords(DateTimeImmutable $date, int $productId, int $deviceId): Generator
    {
        $rawData = $this->repository->getRawData($productId, $deviceId, $date);

        foreach ($rawData as $row) {
            yield $this->recordBuilder->build($row);
        }
    }
}
