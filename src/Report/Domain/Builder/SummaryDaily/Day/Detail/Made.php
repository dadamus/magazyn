<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\SummaryDaily\Day\Detail;

use ERP\Report\Domain\Data\SummaryDaily\Made as MadeData;
use ERP\Report\Domain\Data\SummaryDaily\Record;

class Made
{
    public function build(Record ...$records): MadeData
    {
        $quantity = 0;
        $quantityWithCorrection = 0;

        foreach ($records as $record) {
            $quantity += $record->getProductionQty();
            $quantityWithCorrection += $record->getProductionQtyWithCorrection();
        }

        return new MadeData($quantity, $quantityWithCorrection);
    }
}
