<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\SummaryDaily\Day\Detail;

use ERP\Report\Domain\Data\SummaryDaily\Made as MadeData;
use ERP\Report\Domain\Data\SummaryDaily\Charged as ChargedData;
use ERP\Report\Domain\Data\SummaryDaily\Balance as BalanceData;

class Balance
{
    public function build(MadeData $made, ChargedData $charged): BalanceData
    {
        return new BalanceData($made, $charged);
    }
}
