<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\SummaryDaily\Day\Detail;

use ERP\Report\Domain\Data\SummaryDaily\Record;
use ERP\Report\Domain\Data\SummaryDaily\Charged as ChargedData;

class Charged
{
    public function build(Record ...$records): ChargedData
    {
        $quantity = 0;
        $quantityWithCorrection = 0;

        foreach ($records as $record) {
            $quantity += $record->getTravelQty();
            $quantityWithCorrection += $record->getTravelQtyWithCorrection();
        }

        return new ChargedData($quantity, $quantityWithCorrection);
    }
}
