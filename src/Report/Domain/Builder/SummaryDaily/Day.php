<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\SummaryDaily;

use DateTimeImmutable;
use ERP\Report\Domain\Builder\SummaryDaily\Day\Place;
use ERP\Report\Domain\Data\SummaryDaily\Day as DayData;
use ERP\Settings\Infrastructure\DeviceRepositoryInterface;
use Generator;

class Day
{
    private DeviceRepositoryInterface $devices;

    private Place $placeBuilder;

    public function __construct(DeviceRepositoryInterface $devices, Place $placeBuilder)
    {
        $this->devices = $devices;
        $this->placeBuilder = $placeBuilder;
    }

    public function build(DateTimeImmutable $date): DayData
    {
        $places = $this->buildPlaces($date);

        $places = iterator_to_array($places);

        return new DayData($date, ...$places);
    }

    private function buildPlaces(DateTimeImmutable $date): Generator
    {
        $devices = $this->devices->getAll();

        foreach ($devices as $device) {
            yield $this->placeBuilder->build(
                (string)$device[DeviceRepositoryInterface::DEVICE_NAME],
                (int)$device[DeviceRepositoryInterface::DEVICE_ID],
                $date
            );
        }
    }
}
