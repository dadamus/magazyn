<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\SummaryDaily\SummaryDetails;

use ERP\Report\Domain\Data\SummaryDaily\Place as PlaceData;
use ERP\Report\Domain\Data\SummaryDaily\Made as MadeData;

class Made
{
    public function build(string $productName, PlaceData $place): MadeData
    {
        $quantity = 0;
        $quantityWithCorrection = 0;

        foreach ($place->getDetails() as $detail) {
            if ($detail->getName() !== $productName) {
                continue;
            }

            $quantity += $detail->getMade()->getQuantity();
            $quantityWithCorrection += $detail->getMade()->getQuantityWithCorrection();
        }

        return new MadeData($quantity, $quantityWithCorrection);
    }
}
