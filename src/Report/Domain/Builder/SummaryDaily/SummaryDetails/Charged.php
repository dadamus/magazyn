<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\SummaryDaily\SummaryDetails;

use ERP\Report\Domain\Data\SummaryDaily\Place as PlaceData;
use ERP\Report\Domain\Data\SummaryDaily\Charged as ChargedData;

class Charged
{
    public function build(string $productName, PlaceData $place): ChargedData
    {
        $quantity = 0;
        $quantityWithCorrection = 0;

        foreach ($place->getDetails() as $detail) {
            if ($detail->getName() !== $productName) {
                continue;
            }

            $quantity += $detail->getCharged()->getQuantity();
            $quantityWithCorrection += $detail->getCharged()->getQuantityWithCorrection();
        }

        return new ChargedData($quantity, $quantityWithCorrection);
    }
}
