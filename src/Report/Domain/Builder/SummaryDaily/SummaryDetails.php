<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\SummaryDaily;

use ERP\Report\Domain\Builder\SummaryDaily\SummaryDetails\Balance;
use ERP\Report\Domain\Builder\SummaryDaily\SummaryDetails\Charged;
use ERP\Report\Domain\Builder\SummaryDaily\SummaryDetails\Made;
use ERP\Report\Domain\Data\SummaryDaily\Day as DayData;
use ERP\Report\Domain\Data\SummaryDaily\Place as PlaceData;
use ERP\Report\Domain\Data\SummaryDaily\Detail as DetailData;
use Generator;

class SummaryDetails
{
    private Made $madeBuilder;

    private Charged $chargedBuilder;

    private Balance $balanceBuilder;

    public function __construct(Made $madeBuilder, Charged $chargedBuilder, Balance $balanceBuilder)
    {
        $this->madeBuilder = $madeBuilder;
        $this->chargedBuilder = $chargedBuilder;
        $this->balanceBuilder = $balanceBuilder;
    }

    /**
     * @return Generator|DetailData[]
     */
    public function build(PlaceData $place): Generator
    {
        $productNames = $this->getProductNames($place);

        foreach ($productNames as $productName) {
            yield $this->createSummaryDetail($productName, $place);
        }
    }

    private function createSummaryDetail(string $productName, PlaceData $place): DetailData
    {
        $made = $this->madeBuilder->build($productName, $place);
        $charged = $this->chargedBuilder->build($productName, $place);
        $balance = $this->balanceBuilder->build($made, $charged);

        return new DetailData($productName, $made, $charged, $balance);
    }

    /**
     * @return string[]
     */
    private function getProductNames(PlaceData $place): array
    {
        $names = [];

        foreach ($place->getDetails() as $detail) {
            if (!in_array($detail->getName(), $names)) {
                $names[] = $detail->getName();
            }
        }

        return $names;
    }
}
