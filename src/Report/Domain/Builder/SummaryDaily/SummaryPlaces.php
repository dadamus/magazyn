<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\SummaryDaily;

use ERP\Report\Domain\Data\SummaryDaily\Place as PlaceData;
use ERP\Report\Domain\Data\SummaryDaily\Detail as DetailData;
use ERP\Report\Domain\Data\SummaryDaily\Day as DayData;
use Generator;

class SummaryPlaces
{
    private SummaryDetails $summaryDetailsBuilder;

    public function __construct(SummaryDetails $summaryDetailsBuilder)
    {
        $this->summaryDetailsBuilder = $summaryDetailsBuilder;
    }

    /**
     * @return Generator|PlaceData[]
     */
    public function build(DayData ...$days): Generator
    {
        $places = $this->combinePlaces(...$days);

        foreach ($places as $place) {
            $details = $this->summaryDetailsBuilder->build($place);
            yield new PlaceData($place->getName(), ...$details);
        }
    }

    /**
     * @return PlaceData[]
     */
    private function combinePlaces(DayData ...$days): array
    {
        $places = [];

        foreach ($days as $day) {
            foreach ($day->getPlaces() as $place) {
                if (!isset($places[$place->getName()])) {
                    $places[$place->getName()] = $place;
                    continue;
                }

                $places[$place->getName()] = $this->combineDetails($places[$place->getName()], $place);
            }
        }

        return $places;
    }

    private function combineDetails(PlaceData $oldData, PlaceData $newData): PlaceData
    {
        $details = array_merge($oldData->getDetails(), $newData->getDetails());

        return new PlaceData($oldData->getName(), ...$details);
    }
}
