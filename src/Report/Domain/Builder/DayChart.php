<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder;

use ERP\Report\Domain\Data\Daily\Chart\Data;
use ERP\Report\Domain\Data\Daily\Day;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class DayChart
{
    public function build(Day $day, int $chart): string
    {
        $data = $this->createChartData($day, $chart);

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        return $serializer->serialize($data, JsonEncoder::FORMAT);
    }

    /**
     * @return Data[]
     */
    private function createChartData(Day $day, int $change): array
    {
        $data = [];

        $change = $day->getChanges()[$change];
        foreach ($change->getRecords() as $record) {
            $data[] = new Data(
                $record->getLabel(),
                $record->getProductionQty(),
                $record->getProductionQtyWithCorrection()
            );
        }

        return $data;
    }
}
