<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\Daily;

use ERP\Report\Domain\Data\Daily\Charged as ChargedData;
use ERP\Report\Domain\Data\Daily\Day as DayData;

class Charged
{
    /**
     * @param DayData[] $days
     *
     * @return ChargedData
     */
    public function build(array $days): ChargedData
    {
        $quantity = 0;
        $quantityWithCorrections = 0;

        foreach ($days as $day) {
            $quantity += $day->getCharged()->getQuantity();
            $quantityWithCorrections += $day->getCharged()->getQuantityWithCorrection();
        }

        return new ChargedData($quantity, $quantityWithCorrections);
    }
}
