<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\Daily\Day;

use DateTimeImmutable;
use ERP\Code\Domain\CodeType;
use ERP\Report\Domain\Data\Daily\Record as DayTimeRangeData;
use ERP\Report\Infrastructure\Daily\PDO;

class Record
{
    public const RECORD_INTERVAL_MIN = '15';
    private const DATE_LABEL_FORMAT = 'Y-m-d H:i:s';

    public function build(array $data, DateTimeImmutable $minDate): DayTimeRangeData
    {
        $maxDate = $minDate->modify('+' . self::RECORD_INTERVAL_MIN . 'min');
        $data = $this->filter($data, $minDate, $maxDate);

        $productionQty = 0;
        $productionQtyWithCorrection = 0;

        $travelQty = 0;
        $travelQtyWithCorrection = 0;

        foreach ($data as $value) {
            switch ($value[PDO::CODE_TYPE]) {
                case CodeType::PRODUCTION()->getValue():
                    if ($value[PDO::PRODUCTION_SCANNED_AT] !== null) {
                        $productionQty++;
                        $productionQtyWithCorrection++;
                    }

                    if ($value[PDO::TRAVEL_SCANNED_AT] !== null) {
                        $travelQty++;
                        $travelQtyWithCorrection++;
                    }
                    break;
                case CodeType::PRODUCT_CORRECTION()->getValue():
                    $productionQtyWithCorrection++;
                    break;

                case CodeType::TRAVEL_CORRECTION()->getValue():
                    $travelQtyWithCorrection++;
                    break;
            }
        }

        return new DayTimeRangeData(
            $maxDate->format(self::DATE_LABEL_FORMAT),
            $productionQty,
            $travelQty,
            $productionQtyWithCorrection,
            $travelQtyWithCorrection
        );
    }

    private function filter(array $data, DateTimeImmutable $minDate, DateTimeImmutable $maxDate): array
    {
        $filtered = [];

        foreach ($data as $value) {
            if (
                $this->dateInRange($value[PDO::PRODUCTION_SCANNED_AT], $minDate, $maxDate)
                || $this->dateInRange($value[PDO::TRAVEL_SCANNED_AT], $minDate, $maxDate)
            ) {
                if (!$this->dateInRange($value[PDO::PRODUCTION_SCANNED_AT], $minDate, $maxDate)) {
                    $value[PDO::PRODUCTION_SCANNED_AT] = null;
                }

                if (!$this->dateInRange($value[PDO::TRAVEL_SCANNED_AT], $minDate, $maxDate)) {
                    $value[PDO::TRAVEL_SCANNED_AT] = null;
                }

                $filtered[] = $value;
            }
        }

        return $filtered;
    }

    private function dateInRange(?string $date, DateTimeImmutable $min, DateTimeImmutable $max): bool
    {
        if ($date === null) {
            return false;
        }

        $dateTime = new DateTimeImmutable($date);

        return $dateTime >= $min && $dateTime <= $max;
    }
}
