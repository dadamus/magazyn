<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\Daily\Day;

use ERP\Report\Domain\Data\Daily\Charged as ChargedData;
use ERP\Report\Domain\Data\Daily\Change as ChangeData;

class Charged
{
    /**
     * @param ChangeData[] $changes
     */
    public function build(array $changes): ChargedData
    {
        $charged = 0;
        $chargedWithCorrection = 0;

        foreach ($changes as $change) {
            $records = $change->getRecords();

            foreach ($records as $record) {
                $charged += $record->getTravelQty();
                $chargedWithCorrection += $record->getTravelQtyWithCorrection();
            }
        }

        return new ChargedData($charged, $chargedWithCorrection);
    }
}
