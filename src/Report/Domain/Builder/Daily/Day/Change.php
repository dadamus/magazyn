<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\Daily\Day;

use DateTimeImmutable;
use ERP\Report\Domain\Data\Daily\Change as ChangeData;
use ERP\Report\Domain\Data\Daily\Record as DayTimeRangeData;
use ERP\Report\Domain\Data\DateRange;
use ERP\Report\Infrastructure\Daily\DailyRepositoryInterface;

class Change
{
    private DailyRepositoryInterface $repository;

    private Record $recordBuilder;

    public function __construct(DailyRepositoryInterface $repository, Record $recordBuilder)
    {
        $this->repository = $repository;
        $this->recordBuilder = $recordBuilder;
    }

    public function build(DateRange $range, int $productId): ChangeData
    {
        $rawData = $this->repository->getRawData($productId, $range->getFrom(), $range->getTo());

        $intervalsQuantity = $this->getIntervalsQuantity($range);

        $startDate = $range->getFrom();
        $cutFromLeft = true;

        $records = [];
        for ($i = 0; $i < $intervalsQuantity; $i++) {
            $record = $this->recordBuilder->build($rawData, $startDate);
            $startDate = $this->incrementTime($startDate);

            if ($cutFromLeft && $record->isEmpty()) {
                continue;
            }

            $cutFromLeft = false;
            $records[] = $record;
        }

        $records = $this->clearFromTheRight($records);

        return new ChangeData($records);
    }

    /**
     * @param DayTimeRangeData[] $dayTimeRanges
     *
     * @return DayTimeRangeData[]
     */
    private function clearFromTheRight(array $dayTimeRanges): array
    {
        $sorted = [];

        $quantity = count($dayTimeRanges) - 1;
        $cut = true;
        for ($i = $quantity; $i >= 0; $i--) {
            $dayRimeRange = $dayTimeRanges[$i];

            if ($cut && $dayRimeRange->isEmpty()) {
                continue;
            }

            $sorted[$i] = $dayRimeRange;
            $cut = false;
        }

        ksort($sorted);

        return $sorted;
    }

    private function getIntervalsQuantity(DateRange $range): int
    {
        $fromTimestamp = $range->getFrom()->getTimestamp();
        $toTimestamp = $range->getTo()->getTimestamp();

        $rangeTimestamp = $toTimestamp - $fromTimestamp;

        return ($rangeTimestamp / 60) * Record::RECORD_INTERVAL_MIN;
    }

    private function incrementTime(DateTimeImmutable $date): DateTimeImmutable
    {
        return $date->modify("+" . Record::RECORD_INTERVAL_MIN . 'min');
    }
}
