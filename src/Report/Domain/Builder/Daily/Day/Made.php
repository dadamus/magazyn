<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\Daily\Day;

use ERP\Report\Domain\Data\Daily\Made as MadeData;
use ERP\Report\Domain\Data\Daily\Change;

class Made
{
    /**
     * @param Change[] $changes
     */
    public function build(array $changes): MadeData
    {
        $quantity = 0;
        $quantityWithCorrections = 0;

        foreach ($changes as $change) {
            $records = $change->getRecords();

            foreach ($records as $record) {
                $quantity += $record->getProductionQty();
                $quantityWithCorrections += $record->getProductionQtyWithCorrection();
            }
        }

        return new MadeData($quantity, $quantityWithCorrections);
    }
}
