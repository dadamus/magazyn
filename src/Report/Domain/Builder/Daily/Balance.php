<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\Daily;

use ERP\Report\Domain\Data\Daily\Balance as BalanceData;
use ERP\Report\Domain\Data\Daily\Charged as ChargedData;
use ERP\Report\Domain\Data\Daily\Made as MadeData;

class Balance
{
    public function build(MadeData $made, ChargedData $charged): BalanceData
    {
        return new BalanceData($made, $charged);
    }
}
