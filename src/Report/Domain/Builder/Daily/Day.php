<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\Daily;

use DateTimeImmutable;
use ERP\Report\Domain\Builder\Daily\Day\Balance;
use ERP\Report\Domain\Builder\Daily\Day\Change;
use ERP\Report\Domain\Builder\Daily\Day\Charged;
use ERP\Report\Domain\Builder\Daily\Day\Made;
use ERP\Report\Domain\Builder\Daily\Day\Performance;
use ERP\Report\Domain\Data\Daily\Day as DayData;
use \ERP\Report\Domain\Data\Daily\Change as ChangeData;
use ERP\Report\Domain\Data\DateRange;

class Day
{
    private Made $madeBuilder;

    private Performance $performanceBuilder;

    private Charged $chargedBuilder;

    private Balance $balanceBuilder;

    private Change $changeBuilder;

    /**
     * @param Made $madeBuilder
     * @param Performance $performanceBuilder
     * @param Charged $chargedBuilder
     * @param Balance $balanceBuilder
     * @param Change $changeBuilder
     */
    public function __construct(
        Made $madeBuilder,
        Performance $performanceBuilder,
        Charged $chargedBuilder,
        Balance $balanceBuilder,
        Change $changeBuilder
    ) {
        $this->madeBuilder = $madeBuilder;
        $this->performanceBuilder = $performanceBuilder;
        $this->chargedBuilder = $chargedBuilder;
        $this->balanceBuilder = $balanceBuilder;
        $this->changeBuilder = $changeBuilder;
    }

    public function build(DateTimeImmutable $date, int $productId, int $standard): DayData
    {
        $changes = $this->createChanges($date, $productId);

        $made = $this->madeBuilder->build($changes);
        $performance = $this->performanceBuilder->build($made, $standard);
        $charged = $this->chargedBuilder->build($changes);
        $balance = $this->balanceBuilder->build($made, $charged);

        return new DayData(
            $date,
            $made,
            $performance,
            $charged,
            $balance,
            $changes
        );
    }

    private function createChanges(DateTimeImmutable $date, int $productId): array
    {
        $changeDateFrom = $date->setTime(ChangeData::CHANGE_START_AT_HOUR, 0, 0);

        $changes = [];

        for ($i = 0; $i < ChangeData::CHANGE_QUANTITY; $i++) {
            $changeDateTo = $this->incrementChangeDate($changeDateFrom);
            $changeDateRange = new DateRange(
                $changeDateFrom,
                $changeDateTo
            );

            $changeDateFrom = $this->incrementChangeDate($changeDateFrom);
            $changes[] = $this->changeBuilder->build($changeDateRange, $productId);
        }

        return $changes;
    }

    private function incrementChangeDate(DateTimeImmutable $currentChangeDate): DateTimeImmutable
    {
        return $currentChangeDate->modify('+' . ChangeData::CHANGE_LENGTH_HOURS . 'hours');
    }
}
