<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\Daily;

use ERP\Report\Domain\Data\Daily\Made as MadeData;
use ERP\Report\Domain\Data\Daily\Day;

class Made
{
    /**
     * @param Day[] $days
     */
    public function build(array $days): MadeData
    {
        $quantity = 0;
        $quantityWithCorrection = 0;

        foreach ($days as $day) {
            $quantity += $day->getMade()->getQuantity();
            $quantityWithCorrection += $day->getMade()->getQuantityWithCorrection();
        }

        return new MadeData($quantity, $quantityWithCorrection);
    }
}
