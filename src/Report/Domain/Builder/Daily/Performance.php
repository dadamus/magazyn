<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder\Daily;

use ERP\Report\Domain\Data\Daily\Made as MadeData;
use ERP\Report\Domain\Data\Daily\Performance as PerformanceData;

class Performance
{
    public function build(MadeData $made, int $standard): PerformanceData
    {
        return new PerformanceData($made, $standard);
    }
}
