<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder;

use ERP\Report\Domain\Daily as DailyReport;
use ERP\Report\Domain\Data\Daily\Day;
use ERP\Report\Domain\Builder\Daily\Day as DayBuilder;
use ERP\Report\Domain\Data\DateRange;
use ERP\Report\Domain\Builder\Daily\Made as MadeBuilder;
use ERP\Report\Domain\Builder\Daily\Performance as PerformanceBuilder;
use ERP\Report\Domain\Builder\Daily\Charged as ChargedBuilder;
use ERP\Report\Domain\Builder\Daily\Balance as BalanceBuilder;

class Daily
{
    private DayBuilder $dayBuilder;

    private MadeBuilder $madeBuilder;

    private PerformanceBuilder $performanceBuilder;

    private ChargedBuilder $chargedBuilder;

    private BalanceBuilder $balanceBuilder;

    public function __construct(
        DayBuilder $dayBuilder,
        MadeBuilder $madeBuilder,
        PerformanceBuilder $performanceBuilder,
        ChargedBuilder $chargedBuilder,
        BalanceBuilder $balanceBuilder
    ) {
        $this->dayBuilder = $dayBuilder;
        $this->madeBuilder = $madeBuilder;
        $this->performanceBuilder = $performanceBuilder;
        $this->chargedBuilder = $chargedBuilder;
        $this->balanceBuilder = $balanceBuilder;
    }

    public function build(DateRange $range, int $productId, int $standard): DailyReport
    {
        $days = $this->buildDays($range, $productId, $standard);
        $made = $this->madeBuilder->build($days);
        $performance = $this->performanceBuilder->build($made, $standard);
        $charged = $this->chargedBuilder->build($days);
        $balance = $this->balanceBuilder->build($made, $charged);

        return new DailyReport($days, $standard, $made, $performance, $charged, $balance);
    }

    /**
     * @return Day[]
     */
    private function buildDays(DateRange $range, int $productId, int $standard): array
    {
        $dayDate = $range->getFrom();
        $daysNumber = $range->calculateRangeDays();

        $days = [];

        for ($i = 0; $i < $daysNumber; $i++) {
            $days[] = $this->dayBuilder->build($dayDate, $productId, $standard);

            $dayDate = $dayDate->modify("+1 day");
        }

        return $days;
    }
}
