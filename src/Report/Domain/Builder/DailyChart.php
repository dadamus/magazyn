<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder;

use ERP\Report\Domain\Daily as DailyReport;
use ERP\Report\Domain\Data\Chart\Data;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class DailyChart
{
    public function build(DailyReport $daily): string
    {
        $data = $this->createChartData($daily);

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        return $serializer->serialize($data, JsonEncoder::FORMAT);
    }

    private function createChartData(DailyReport $daily): array
    {
        $data = [];

        foreach ($daily->getDays() as $day) {
            $production = $day->getMade();

            $data[] = new Data(
                $day->getDate()->format('Y-m-d H:i:s'),
                $production->getQuantity(),
                $production->getQuantityWithCorrection()
            );
        }

        return $data;
    }
}
