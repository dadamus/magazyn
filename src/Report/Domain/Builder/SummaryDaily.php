<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Builder;

use ERP\Report\Domain\Builder\SummaryDaily\Day;
use ERP\Report\Domain\Builder\SummaryDaily\SummaryPlaces;
use ERP\Report\Domain\Data\DateRange;
use ERP\Report\Domain\SummaryDaily as SummaryDailyData;
use Generator;
use ERP\Report\Domain\Data\SummaryDaily\Day as DayData;

class SummaryDaily
{
    private Day $dayBuilder;

    private SummaryPlaces $summaryPalcesBuilder;

    public function __construct(Day $dayBuilder, SummaryPlaces $summaryPalcesBuilder)
    {
        $this->dayBuilder = $dayBuilder;
        $this->summaryPalcesBuilder = $summaryPalcesBuilder;
    }

    public function build(DateRange $range): SummaryDailyData
    {
        $days = iterator_to_array($this->buildDays($range));
        $summaryDetails = iterator_to_array($this->summaryPalcesBuilder->build(...$days));

        return new SummaryDailyData($summaryDetails, $days);
    }

    /**
     * @return Generator|DayData[]
     */
    private function buildDays(DateRange $range): Generator
    {
        $dayDate = $range->getFrom();
        $daysNumber = $range->calculateRangeDays();

        for ($i = 0; $i < $daysNumber; $i++) {
            yield $this->dayBuilder->build($dayDate);

            $dayDate = $dayDate->modify("+1 day");
        }
    }
}
