<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Data\Daily;

use DateTimeImmutable;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class Day
{
    private DateTimeImmutable $date;

    private Made $made;

    private Performance $performance;

    private Charged $charged;

    private Balance $balance;

    /**
     * @var Change[]
     */
    private array $changes;

    /**
     * @param Change[] $changes
     */
    public function __construct(
        DateTimeImmutable $date,
        Made $made,
        Performance $performance,
        Charged $charged,
        Balance $balance,
        array $changes
    ) {
        $this->date = $date;
        $this->made = $made;
        $this->performance = $performance;
        $this->charged = $charged;
        $this->balance = $balance;
        $this->changes = $changes;
    }

    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }

    public function getMade(): Made
    {
        return $this->made;
    }

    public function getPerformance(): Performance
    {
        return $this->performance;
    }

    public function getCharged(): Charged
    {
        return $this->charged;
    }

    public function getBalance(): Balance
    {
        return $this->balance;
    }

    /**
     * @return Change[]
     */
    public function getChanges(): array
    {
        return $this->changes;
    }

    public function serializeBalanceByTime(): string
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        return $serializer->serialize($this->getChanges(), JsonEncoder::FORMAT);
    }
}
