<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Data\Daily;

use ERP\Report\Domain\Data\Daily\Record;

class Change
{
    public const CHANGE_LENGTH_HOURS = 8;
    public const CHANGE_START_AT_HOUR = 6;
    public const CHANGE_QUANTITY = 3;

    /**
     * @var Record[]
     */
    private array $records;

    /**
     * @param Record[] $records
     */
    public function __construct(array $records)
    {
        $this->records = $records;
    }

    /**
     * @return Record[]
     */
    public function getRecords(): array
    {
        return $this->records;
    }

    public function isEmpty(): bool
    {
        return count($this->records) === 0;
    }
}
