<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Data\Daily;

class Performance
{
    private float $percentage;

    private float $percentageWithCorrection;

    public function __construct(Made $made, int $standard)
    {
        $this->percentage = $made->getQuantity() / $standard * 100;
        $this->percentageWithCorrection = $made->getQuantityWithCorrection() / $standard * 100;
    }

    public function getPercentage(): float
    {
        return $this->percentage;
    }

    public function getPercentageWithCorrection(): float
    {
        return $this->percentageWithCorrection;
    }
}
