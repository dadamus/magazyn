<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Data\Daily\Chart;

class Data
{
    private string $hour;

    private float $value;

    private float $valueWithCorrection;

    public function __construct(string $hour, float $value, float $valueWithCorrection)
    {
        $this->hour = $hour;
        $this->value = $value;
        $this->valueWithCorrection = $valueWithCorrection;
    }

    public function getHour(): string
    {
        return $this->hour;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function getValueWithCorrection(): float
    {
        return $this->valueWithCorrection;
    }
}
