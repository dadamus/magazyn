<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Data\Daily;

class Balance
{
    private int $quantity;

    private int $quantityWithCorrection;

    public function __construct(Made $made, Charged $charged)
    {
        $this->quantity = $made->getQuantity() - $charged->getQuantity();
        $this->quantityWithCorrection = $made->getQuantityWithCorrection() - $charged->getQuantityWithCorrection();
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getQuantityWithCorrection(): int
    {
        return $this->quantityWithCorrection;
    }
}
