<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Data\Daily;

class Record
{
    private string $label;

    private int $productionQty;

    private int $travelQty;

    private int $productionQtyWithCorrection;

    private int $travelQtyWithCorrection;

    public function __construct(
        string $label,
        int $productionQty,
        int $travelQty,
        int $productionQtyWithCorrection,
        int $travelQtyWithCorrection
    ) {
        $this->label = $label;
        $this->productionQty = $productionQty;
        $this->travelQty = $travelQty;
        $this->productionQtyWithCorrection = $productionQtyWithCorrection;
        $this->travelQtyWithCorrection = $travelQtyWithCorrection;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getProductionQty(): int
    {
        return $this->productionQty;
    }

    public function getTravelQty(): int
    {
        return $this->travelQty;
    }

    public function getProductionQtyWithCorrection(): int
    {
        return $this->productionQtyWithCorrection;
    }

    public function getTravelQtyWithCorrection(): int
    {
        return $this->travelQtyWithCorrection;
    }

    public function isEmpty(): bool
    {
        return $this->getProductionQty() === 0
            && $this->getTravelQty() === 0
            && $this->getProductionQtyWithCorrection() === 0
            && $this->getTravelQtyWithCorrection() === 0;
    }
}
