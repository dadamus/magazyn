<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Data;

use DateInterval;
use DatePeriod;
use DateTime;
use DateTimeImmutable;

class DateRange
{
    private DateTimeImmutable $from;

    private DateTimeImmutable $to;

    public function __construct(DateTimeImmutable $from, DateTimeImmutable $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public function getFrom(): DateTimeImmutable
    {
        return $this->from;
    }

    public function getTo(): DateTimeImmutable
    {
        return $this->to;
    }

    public function calculateRangeDays(): int
    {
        $period = new DatePeriod(
            $this->from,
            new DateInterval('P1D'),
            $this->to
        );

        return iterator_count($period);
    }
}
