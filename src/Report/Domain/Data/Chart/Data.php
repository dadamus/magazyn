<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Data\Chart;

class Data
{
    private string $date;

    private float $value;

    private float $valueWithCorrection;

    public function __construct(string $date, float $value, float $valueWithCorrection)
    {
        $this->date = $date;
        $this->value = $value;
        $this->valueWithCorrection = $valueWithCorrection;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function getValueWithCorrection(): float
    {
        return $this->valueWithCorrection;
    }
}
