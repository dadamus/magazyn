<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Data\SummaryDaily;

class Store
{
    private int $quantity;

    private int $quantityWithCorrection;

    public function __construct(int $quantity, int $quantityWithCorrection)
    {
        $this->quantity = $quantity;
        $this->quantityWithCorrection = $quantityWithCorrection;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getQuantityWithCorrection(): int
    {
        return $this->quantityWithCorrection;
    }
}
