<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Data\SummaryDaily;

use DateTimeImmutable;

class Day
{
    private DateTimeImmutable $date;

    /**
     * @var Place[]
     */
    private array $places;

    public function __construct(DateTimeImmutable $date, Place ...$places)
    {
        $this->date = $date;
        $this->places = $places;
    }

    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @return Place[]
     */
    public function getPlaces(): array
    {
        return $this->places;
    }
}
