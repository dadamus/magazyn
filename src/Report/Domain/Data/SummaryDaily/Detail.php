<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Data\SummaryDaily;

class Detail
{
    private string $name;

    private Made $made;

    private Charged $charged;

    private Balance $balance;

    /**
     * @var Record[]
     */
    private array $records = [];

    public function __construct(string $name, Made $made, Charged $charged, Balance $balance, Record ...$records)
    {
        $this->name = $name;
        $this->made = $made;
        $this->charged = $charged;
        $this->balance = $balance;
        $this->records = $records;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getMade(): Made
    {
        return $this->made;
    }

    public function getCharged(): Charged
    {
        return $this->charged;
    }

    public function getBalance(): Balance
    {
        return $this->balance;
    }

    public function isEmpty(): bool
    {
        return $this->made->getQuantity() === 0
            && $this->made->getQuantityWithCorrection() === 0;
    }
}
