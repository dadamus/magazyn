<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Data\SummaryDaily;

class Place
{
    private string $name;

    /**
     * @var Detail[]
     */
    private array $details;

    public function __construct(string $name, Detail ...$details)
    {
        $this->name = $name;
        $this->details = $details;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Detail[]
     */
    public function getDetails(): array
    {
        return $this->details;
    }

    public function isEmpty(): bool
    {
        foreach ($this->details as $detail) {
            if (!$detail->isEmpty()) {
                return false;
            }
        }

        return true;
    }
}
