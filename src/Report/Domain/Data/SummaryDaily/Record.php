<?php

declare(strict_types=1);

namespace ERP\Report\Domain\Data\SummaryDaily;

class Record
{
    private string $productName;

    private int $productionQty;

    private int $productionQtyWithCorrection;

    private int $travelQty;

    private int $travelQtyWithCorrection;

    public function __construct(
        string $productName,
        int $productionQty,
        int $productionQtyWithCorrection,
        int $travelQty,
        int $travelQtyWithCorrection
    ) {
        $this->productName = $productName;
        $this->productionQty = $productionQty;
        $this->productionQtyWithCorrection = $productionQtyWithCorrection;
        $this->travelQty = $travelQty;
        $this->travelQtyWithCorrection = $travelQtyWithCorrection;
    }

    public function getProductName(): string
    {
        return $this->productName;
    }

    public function getProductionQty(): int
    {
        return $this->productionQty;
    }

    public function getProductionQtyWithCorrection(): int
    {
        return $this->productionQtyWithCorrection;
    }

    public function getTravelQty(): int
    {
        return $this->travelQty;
    }

    public function getTravelQtyWithCorrection(): int
    {
        return $this->travelQtyWithCorrection;
    }
}
