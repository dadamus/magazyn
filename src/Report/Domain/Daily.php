<?php

declare(strict_types=1);

namespace ERP\Report\Domain;

use ERP\Report\Domain\Data\Daily\Balance;
use ERP\Report\Domain\Data\Daily\Charged;
use ERP\Report\Domain\Data\Daily\Day;
use ERP\Report\Domain\Data\Daily\Made;
use ERP\Report\Domain\Data\Daily\Performance;

class Daily
{
    /**
     * @var Day[]
     */
    private array $days;

    private int $standard;

    private Made $made;

    private Performance $performance;

    private Charged $charged;

    private Balance $balance;

    public function __construct(
        $days,
        int $standard,
        Made $made,
        Performance $performance,
        Charged $charged,
        Balance $balance
    ) {
        $this->days = $days;
        $this->standard = $standard;
        $this->made = $made;
        $this->performance = $performance;
        $this->charged = $charged;
        $this->balance = $balance;
    }

    /**
     * @return Day[]
     */
    public function getDays(): array
    {
        return $this->days;
    }

    public function getStandard(): int
    {
        return $this->standard;
    }

    public function getMade(): Made
    {
        return $this->made;
    }

    public function getPerformance(): Performance
    {
        return $this->performance;
    }

    public function getCharged(): Charged
    {
        return $this->charged;
    }

    public function getBalance(): Balance
    {
        return $this->balance;
    }
}
