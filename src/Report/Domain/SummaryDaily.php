<?php

declare(strict_types=1);

namespace ERP\Report\Domain;

use ERP\Report\Domain\Data\SummaryDaily\Day;
use ERP\Report\Domain\Data\SummaryDaily\Place;

class SummaryDaily
{
    /**
     * @var Place[]
     */
    private array $summaryPlaces;

    /**
     * @var Day[]
     */
    private array $days;

    /**
     * @param Place[] $summaryPlaces
     * @param Day[] $days
     */
    public function __construct(array $summaryPlaces, array $days)
    {
        $this->summaryPlaces = $summaryPlaces;
        $this->days = $days;
    }

    /**
     * @return Place[]
     */
    public function getSummaryPlaces(): array
    {
        return $this->summaryPlaces;
    }

    /**
     * @return Day[]
     */
    public function getDays(): array
    {
        return $this->days;
    }
}
