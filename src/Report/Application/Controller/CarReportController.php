<?php

namespace ERP\Report\Application\Controller;

use ERP\Report\Infrastructure\CarsReportData;
use ERP\SharedKernel\Application\Controller\CoreController;
use ERP\Settings\Infrastructure\CarRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CarReportController extends CoreController
{
    /**
     * @Route("/report/car", name="report_car")
     */
    public function indexAction(CarRepositoryInterface $cars): Response
    {
        return $this->render('Report/cars_report.html.twig', [
            'cars' => $cars->getAll(true)
        ]);
    }

    /**
     * @Route("/report/car/list", name="report_car_list")
     */
    public function generateReportList(Request $request, CarsReportData $carsReportData): Response
    {
        $from = new \DateTimeImmutable($request->request->get('from'));
        $to = new \DateTimeImmutable($request->request->get('to') . ' 23:59:59');
        $carId = $request->request->get('car_id');

        return $this->render('Report/cars_report_list.twig', [
            'list' => $carsReportData->getData($from, $to, $carId)
        ]);
    }

    /**
     * @Route("/report/car/json", name="report_car_json")
     */
    public function generateReportImage(Request $request, CarsReportData $carsReportData): Response
    {
        $from = new \DateTimeImmutable($request->query->get('from'));
        $to = new \DateTimeImmutable($request->query->get('to') . ' 23:59:59');
        $carId = $request->query->get('car_id');
        $productId = $request->query->get('product_id');

        $data = $carsReportData->getDataWithoutGroup($from, $to, $carId, $productId);

        $scanned = [];

        $dateSubtraction = $to->diff($from);

        if ((int)$dateSubtraction->d < 1) {
            $minValue = 1;
            $maxValue = 24;
            $dateValueKey = 'H';
        } else if ((int)$dateSubtraction->m < 1) {
            $minValue = (int)$from->format('d');
            $maxValue = (int)$to->format('d');
            $dateValueKey = 'd';
        } else {
            $minValue = (int)$from->format('m');
            $maxValue = (int)$to->format('m');
            $dateValueKey = 'm';
        }

        //Add empty rows
        for ($i = $minValue; $i <= $maxValue; $i++) {
            if (!isset($scanned[$i])) {
                $scanned[$i] = ['scanned' => 0, 'planned' => 0];
            }
        }

        foreach ($data as $row) {
            $date = new \DateTimeImmutable($row['travel_scanned_at']);

            if (isset($scanned[(int)$date->format($dateValueKey)])) {
                $data = $scanned[(int)$date->format($dateValueKey)];

                $scanned[(int)$date->format($dateValueKey)] = [
                    'scanned' => $data['scanned'] + 1,
                    'planned' => (int)$row['planned']
                ];
            }
        }

        $responseData = [];

        foreach ($scanned as $date => $value) {
            $responseData[] = [
                'date' => $date,
                'scanned' => $value['scanned'],
                'planned' => $value['planned']
            ];
        }

        return new JsonResponse($responseData);
    }
}
