<?php

declare(strict_types=1);

namespace ERP\Report\Application\Controller;

use DateTimeImmutable;
use ERP\Report\Domain\Builder\Daily as DailyBuilder;
use ERP\Report\Domain\Builder\DayChart as DayChartBuilder;
use ERP\Report\Domain\Builder\DailyChart as DailyChartBuilder;
use ERP\Report\Domain\Data\DateRange;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DailyController extends AbstractController
{
    /**
     * @Route("/report/daily", name="report_daily_form")
     */
    public function indexAction(): Response
    {
        return $this->render('Report/daily_report_form.html.twig');
    }

    /**
     * @Route("/report/daily/create", methods={"POST"})
     */
    public function createReport(
        Request $request,
        DailyBuilder $dailyBuilder,
        DayChartBuilder $dayChartBuilder,
        DailyChartBuilder $dailyChartBuilder
    ): Response {
        $dateRange = new DateRange(
            new DateTimeImmutable((string)$request->request->get('from')),
            new DateTimeImmutable((string)$request->request->get('to')),
        );

        $performance = $request->request->getInt('performance');
        $productId = $request->request->getInt('product_id');

        $daily = $dailyBuilder->build($dateRange, $productId, $performance);

        return $this->render('Report/daily_report.html.twig', [
            'daily' => $daily,
            'dayChartBuilder' => $dayChartBuilder,
            'dailyChartBuilder' => $dailyChartBuilder,
        ]);
    }
}
