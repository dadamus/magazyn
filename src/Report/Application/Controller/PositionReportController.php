<?php


namespace ERP\Report\Application\Controller;


use ERP\Report\Infrastructure\PositionReportData;
use ERP\SharedKernel\Application\Controller\CoreController;
use ERP\Settings\Infrastructure\PositionRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PositionReportController extends CoreController
{
    /**
     * @Route("/report/position", name="report_position")
     */
    public function indexAction(PositionRepositoryInterface $positions): Response
    {
        return $this->render('Report/position_report.html.twig', [
            'positions' => $positions->getAll(true)
        ]);
    }

    /**
     * @Route("/report/position/list", name="report_position_list")
     */
    public function getListAction(Request $request, PositionReportData $positionReportData): Response
    {
        $from = new \DateTimeImmutable($request->request->get('from'));
        $to = new \DateTimeImmutable($request->request->get('to') . ' 23:59:59');
        $positionId = $request->request->get('position_id');

        $dateSubtraction = $to->diff($from);

        if ((int)$dateSubtraction->d === 0 && (int)$dateSubtraction->m === 0) {
            $minValue = 1;
            $maxValue = 24;
            $dateValueKey = 'H';
        } else if ((int)$dateSubtraction->m < 1) {
            $minValue = (int)$from->format('d');
            $maxValue = (int)$to->format('d');
            $dateValueKey = 'd';
        } else {
            $minValue = (int)$from->format('m');
            $maxValue = (int)$to->format('m');
            $dateValueKey = 'm';
        }

        $positionsData = [];

        //Add empty rows
        for ($i = $minValue; $i <= $maxValue; $i++) {
            if (!isset($positionsData[$i])) {
                $positionsData[$i] = [
                    'travel' => 0,
                    'production' => 0,
                    'production_correction' => 0,
                    'production_without_correction' => 0,
                    'travel_correction' => 0,
                    'travel_without_correction' => 0
                ];
            }
        }

        //Catch data
        $data = $positionReportData->getData($from, $to, $positionId);
        $allProductionQuantity = 0;

        foreach ($data as $product) {

        }

        return $this->render('Report/position_report_list.html.twig', [
            'data' => $data
        ]);
    }
}
