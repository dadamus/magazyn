<?php

declare(strict_types=1);

namespace ERP\Report\Application\Controller;

use ERP\Product\Infrastructure\ProductRepositoryInterface;
use ERP\Report\Domain\Builder\Daily as DailyBuilder;
use ERP\Report\Domain\Builder\DailyChart as DailyChartBuilder;
use ERP\Report\Domain\Builder\DayChart as DayChartBuilder;
use ERP\Report\Domain\Data\DateRange;
use ERP\SharedKernel\Application\Controller\CoreController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ProductReportApiController extends CoreController
{
    /**
     * @Route("/api/report/product/list", methods={"GET"})
     */
    public function getProductList(Request $request, ProductRepositoryInterface $productRepository): JsonResponse
    {
        $productName = $request->get('search');

        return new JsonResponse($productRepository->find($productName));
    }

    /**
     * @Route("/api/report/product/daily", methods={"GET"})
     */
    public function getDailyReport(
        Request $request,
        DailyBuilder $dailyBuilder,
        DayChartBuilder $dayChartBuilder,
        DailyChartBuilder $dailyChartBuilder
    ): JsonResponse {
        $requestData = json_decode($request->getContent(), true);

        $dateRange = new DateRange(
            new \DateTimeImmutable((string)$request->query->get('from')),
            new \DateTimeImmutable((string)$request->query->get('to')),
        );

        $performance = $request->query->getInt('standard');
        $productId = $request->query->getInt('product');

        $daily = $dailyBuilder->build($dateRange, $productId, $performance);

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        return new JsonResponse(json_decode($serializer->serialize($daily, 'json'), true));
    }
}
