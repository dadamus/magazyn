<?php

namespace ERP\Report\Application\Controller;

use ERP\Report\Infrastructure\ProductReportData;
use ERP\SharedKernel\Application\Controller\CoreController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends CoreController
{
    /**
     * @Route("/old/report/product", name="report_product")
     */
    public function indexAction(): Response
    {
        return $this->render('Report/products_report.html.twig');
    }

    /**
     * @Route("/report/product/list", name="report_product_list")
     * @throws \Exception
     * @return Response
     *
     * @param Request $request
     */
    public function getListAction(Request $request, ProductReportData $productReportData): Response
    {
        $from = new \DateTimeImmutable($request->request->get('from'));
        $to = new \DateTimeImmutable($request->request->get('to') . ' 23:59:59');
        $productId = $request->request->get('product_id');

        $dateSubtraction = $to->diff($from);

        if ((int)$dateSubtraction->d === 0 && (int)$dateSubtraction->m === 0) {
            $minValue = 1;
            $maxValue = 24;
            $dateValueKey = 'H';
        } else if ((int)$dateSubtraction->m < 1) {
            $minValue = (int)$from->format('d');
            $maxValue = (int)$to->format('d');
            $dateValueKey = 'd';
        } else {
            $minValue = (int)$from->format('m');
            $maxValue = (int)$to->format('m');
            $dateValueKey = 'm';
        }

        $productsData = [];

        //Add empty rows
        for ($i = $minValue; $i <= $maxValue; $i++) {
            if (!isset($productsData[$i])) {
                $productsData[$i] = [
                    'travel' => 0,
                    'production' => 0,
                    'production_correction' => 0,
                    'production_without_correction' => 0,
                    'travel_correction' => 0,
                    'travel_without_correction' => 0
                ];
            }
        }

        //Catch data
        $data = $productReportData->getData($from, $to, $productId);
        $allProductionQuantity = 0;

        foreach ($data as $product) {
            $travelDate = strlen($product['travel_scanned_at']) > 0 ? new \DateTimeImmutable($product['travel_scanned_at']) : null;
            $productionDate = strlen($product['production_scanned_at']) > 0 ? new \DateTimeImmutable($product['production_scanned_at']) : null;

            if ($travelDate !== null && $travelDate >= $from && $travelDate <= $to) {
                $travelKeyValue = (int)$travelDate->format($dateValueKey);

                if (!isset($travelKeyValue)) {
                    $productsData[$travelKeyValue] = [];
                }

                if (!isset($productsData[$travelKeyValue]['travel'])) {
                    $productsData[$travelKeyValue]['travel'] = 0;
                }

                $productsData[$travelKeyValue]['travel'] += $product['amount'];

                if ((int)$product['type_id'] === 5) {
                    $productsData[$travelKeyValue]['travel_correction'] += $product['amount'];
                } else {
                    $productsData[$travelKeyValue]['travel_without_correction'] += $product['amount'];
                }

                $productsData[$travelKeyValue]['date'] = $travelDate->format('Y-m-d H:i:s');
            }

            if ($productionDate !== null && $productionDate >= $from && $productionDate <= $to) {
                $productionKeyValue = (int)$productionDate->format($dateValueKey);

                if (!isset($productsData[$productionKeyValue])) {
                    $productsData[$productionKeyValue] = [];
                }

                if (!isset($productsData[$productionKeyValue]['production'])) {
                    $productsData[$productionKeyValue]['production'] = 0;
                }

                $productsData[$productionKeyValue]['production'] += $product['amount'];

                if ((int)$product['type_id'] === 4) {
                    $productsData[$productionKeyValue]['production_correction'] += $product['amount'];
                } else {
                    $productsData[$productionKeyValue]['production_without_correction'] += $product['amount'];
                }

                $productsData[$productionKeyValue]['date'] = $productionDate->format('Y-m-d H:i:s');
                $allProductionQuantity += 1;
            }
        }

        $productionAverage = $allProductionQuantity / count($productsData);

        foreach ($productsData as $dateKey => $row) {
            $productsData[$dateKey]['diff'] = $row['production'] - $row['travel'];
            $productsData[$dateKey]['date_start'] = $dateKey;
            $productsData[$dateKey]['date_end'] = $dateKey + 1;
            $productsData[$dateKey]['percent'] = round((-1 + ($row['production'] / $productionAverage)) * 100, 2);
        }

        return $this->render('Report/products_report_list.html.twig', [
            'data' => $productsData
        ]);
    }
}
