<?php

declare(strict_types=1);

namespace ERP\Report\Application\Controller;

use DateTimeImmutable;
use ERP\Report\Domain\Builder\SummaryDaily as SummaryDailyBuilder;
use ERP\Report\Domain\Data\DateRange;
use ERP\SharedKernel\Application\Controller\CoreController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class DailySummaryApiController extends CoreController
{
    /**
     * @Route("/api/report/product/summary-daily", methods={"GET"})
     */
    public function createReport(Request $request, SummaryDailyBuilder $builder): Response
    {
        $dateRange = new DateRange(
            new DateTimeImmutable((string)$request->query->get('from')),
            new DateTimeImmutable((string)$request->query->get('to')),
        );

        $summaryDaily = $builder->build($dateRange);

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        return new JsonResponse(json_decode($serializer->serialize($summaryDaily, 'json'), true));
    }
}
