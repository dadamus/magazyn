<?php

declare(strict_types=1);

namespace ERP\Report\Application\Controller;

use DateTimeImmutable;
use ERP\Report\Domain\Data\DateRange;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use ERP\Report\Domain\Builder\SummaryDaily as SummaryDailyBuilder;

class DailySummaryController extends AbstractController
{
    /**
     * @Route("/report/summary-daily", name="report_summary_daily_form")
     */
    public function indexAction(): Response
    {
        return $this->render('Report/summary_daily_report_form.html.twig');
    }

    /**
     * @Route("/report/summary-daily/create", methods={"POST"})
     */
    public function createReport(Request $request, SummaryDailyBuilder $builder): Response
    {
        $dateRange = new DateRange(
            new DateTimeImmutable((string)$request->request->get('from')),
            new DateTimeImmutable((string)$request->request->get('to')),
        );

        $summaryDaily = $builder->build($dateRange);

        return $this->render('Report/summary_daily_report.html.twig', [
            'summaryDaily' => $summaryDaily,
            'range' => $dateRange
        ]);
    }
}
