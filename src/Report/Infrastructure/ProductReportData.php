<?php

namespace ERP\Report\Infrastructure;

use Doctrine\DBAL\Connection;

class ProductReportData
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getData(\DateTimeImmutable $from, \DateTimeImmutable $to, int $productId): array
    {
        return $this->connection->fetchAllAssociative('
            SELECT
            	pcv.product_id,
            	pcv.travel_scanned_at,
            	pcv.production_scanned_at,
                psl.amount,
                c.type_id
            FROM
            	product_code_view pcv
            	LEFT JOIN travel_product tp ON tp.product_id = pcv.product_id
            	LEFT JOIN product_state_log psl ON psl.product_id = tp.product_id AND psl.code_id = pcv.code_id
            	LEFT JOIN `code` c ON c.id = pcv.code_id
            	LEFT JOIN travel t ON t.id = tp.travel_id
            	LEFT JOIN product p ON p.id = pcv.product_id
            WHERE
            	pcv.product_id = :productId
            	AND
                (
            	    (
            	        pcv.travel_scanned_at >= :from 
            	        AND pcv.travel_scanned_at <= :to
            	    )
            	    OR
            	    (
            	        pcv.production_scanned_at >= :from
            		    AND pcv.production_scanned_at <= :to
            	    )
            	)
        ', [
            'from' => $from->format('Y-m-d H:i:s'),
            'to' => $to->format('Y-m-d H:i:s'),
            'productId' => $productId
        ]);
    }
}
