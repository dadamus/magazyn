<?php

namespace ERP\Report\Infrastructure;

use Doctrine\DBAL\Connection;

class PositionReportData
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getData(\DateTimeImmutable $from, \DateTimeImmutable $to, int $positionId): array
    {
        return $this->connection->fetchAllAssociative('
            SELECT
            p.name,
            (
                SELECT
                COUNT(*)
                FROM
                product_code_view pcv2
                WHERE
                pcv2.product_id = pcv.product_id
                AND pcv2.production_scanned_at >= :from AND pcv2.production_scanned_at <= :to
            ) as production_quantity,
            (
                SELECT
                COUNT(*)
                FROM
                product_code_view pcv2
                WHERE
                pcv2.product_id = pcv.product_id
                AND pcv2.travel_scanned_at >= :from AND pcv2.travel_scanned_at <= :to
            ) as travel_quantity       
            FROM
            product_code_view pcv
            LEFT JOIN device d ON d.id = pcv.production_device_id
            LEFT JOIN product p on pcv.product_id = p.id
            WHERE
            d.position_id = :positionId
            AND pcv.production_scanned_at >= :from AND pcv.production_scanned_at <= :to
            GROUP BY pcv.product_id
        ', [
            'positionId' => $positionId,
            'from' => $from->format('Y-m-d H:i:s'),
            'to' => $to->format('Y-m-d H:i:s')
        ]);
    }
}
