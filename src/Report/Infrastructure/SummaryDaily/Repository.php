<?php

declare(strict_types=1);

namespace ERP\Report\Infrastructure\SummaryDaily;

use DateTimeImmutable;
use Doctrine\DBAL\Connection;

class Repository
{
    public const PRODUCT_NAME = 'name';
    public const PRODUCT_ID = 'id';
    public const PRODUCTION_SCANNED_AT = 'production_scanned_at';
    public const TRAVEL_SCANNED_AT = 'travel_scanned_at';
    public const CODE_TYPE = 'type_id';

    private const DATE_FORMAT = 'Y-m-d H:i:s';

    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getRawData(int $productId, int $deviceId, DateTimeImmutable $from, ?DateTimeImmutable $to = null): array
    {
        if ($to === null) {
            $to = new DateTimeImmutable($from->format('Y-m-d') . ' 23:59:59');
        }

        return $this->connection->fetchAllAssociative('
            SELECT
            p.name,
            pcv.production_scanned_at,
            pcv.travel_scanned_at,
            c.type_id
            FROM
            product_code_view pcv
            LEFT JOIN `code` c ON c.id = pcv.code_id
            LEFT JOIN `product` p ON p.id = pcv.product_id
            WHERE
            pcv.product_id = :productId
            AND pcv.production_device_id = :deviceId
            AND (
                (pcv.production_scanned_at >= :from AND pcv.production_scanned_at <= :to)
                OR (pcv.travel_scanned_at >= :from AND pcv.travel_scanned_at <= :to)
            )
        ', [
            'productId' => $productId,
            'deviceId' => $deviceId,
            'from' => $from->format(self::DATE_FORMAT),
            'to' => $to->format(self::DATE_FORMAT)
        ]);
    }

    public function getProductsNameFromCodeRange(DateTimeImmutable $from, ?DateTimeImmutable $to = null): array
    {
        if ($to === null) {
            $to = new DateTimeImmutable($from->format('Y-m-d') . ' 23:59:59');
        }

        return $this->connection->fetchAllAssociative('
            SELECT
            p.name,
            p.id
            FROM
            product_code_view pcv
            LEFT JOIN `code` c ON c.id = pcv.code_id
            LEFT JOIN `product` p ON p.id = pcv.product_id
            WHERE
            (pcv.production_scanned_at >= :from AND pcv.production_scanned_at <= :to)
            OR 
            (pcv.travel_scanned_at >= :from AND pcv.travel_scanned_at <= :to)
            GROUP BY p.id
        ', [
            'from' => $from->format(self::DATE_FORMAT),
            'to' => $to->format(self::DATE_FORMAT)
        ]);
    }
}
