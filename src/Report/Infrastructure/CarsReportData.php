<?php

namespace ERP\Report\Infrastructure;

use Doctrine\DBAL\Connection;

class CarsReportData
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getData(\DateTimeImmutable $from, \DateTimeImmutable $to, int $carId): array
    {
        return $this->connection->fetchAllAssociative('
            SELECT
            count(*) as scanned,
            p.`name`,
            pcv.product_id,
            (
            	SELECT 
            	tpc.planned
            	FROM
            	travel_product tpc
            	WHERE
            	tpc.travel_id = tp.travel_id
            	AND tpc.product_id = pcv.product_id
            ) as planned 
            FROM
            product_code_view pcv
            LEFT JOIN travel_product tp ON tp.product_id = pcv.product_id
            LEFT JOIN travel t ON t.id = tp.travel_id
            LEFT JOIN product p ON p.id = pcv.product_id
            WHERE
            t.car_id = :carId
            AND pcv.travel_scanned_at >= :from AND pcv.travel_scanned_at <= :to
            GROUP BY pcv.product_id, tp.travel_id
        ', [
            'carId' => $carId,
            'from' => $from->format('Y-m-d H:i:s'),
            'to' => $to->format('Y-m-d H:i:s'),
        ]);
    }

    public function getDataWithoutGroup(
        \DateTimeImmutable $from,
        \DateTimeImmutable $to,
        int $carId,
        int $productId
    ): array {
        return $this->connection->fetchAllAssociative('
            SELECT
            p.`name`,
            pcv.product_id,
            (
            	SELECT 
            	tpc.planned
            	FROM
            	travel_product tpc
            	WHERE
            	tpc.travel_id = tp.travel_id
            	AND tpc.product_id = pcv.product_id
            ) as planned,
            pcv.travel_scanned_at
            FROM
            product_code_view pcv
            LEFT JOIN travel_product tp ON tp.product_id = pcv.product_id
            LEFT JOIN travel t ON t.id = tp.travel_id
            LEFT JOIN product p ON p.id = pcv.product_id
            WHERE
            t.car_id = :carId
            AND pcv.travel_scanned_at >= :from AND pcv.travel_scanned_at <= :to
            AND pcv.product_id = :productId
        ', [
            'carId' => $carId,
            'from' => $from->format('Y-m-d H:i:s'),
            'to' => $to->format('Y-m-d H:i:s'),
            'productId' => $productId
        ]);
    }
}
