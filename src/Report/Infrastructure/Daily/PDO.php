<?php

declare(strict_types=1);

namespace ERP\Report\Infrastructure\Daily;

use DateTimeImmutable;
use Doctrine\DBAL\Connection;

class PDO implements DailyRepositoryInterface
{
    public const PRODUCTION_SCANNED_AT = 'production_scanned_at';
    public const TRAVEL_SCANNED_AT = 'travel_scanned_at';
    public const CODE_TYPE = 'type_id';

    private const DATE_FORMAT = 'Y-m-d H:i:s';

    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getRawData(int $productId, DateTimeImmutable $from, ?DateTimeImmutable $to = null): array
    {
        if ($to === null) {
            $to = $from;
        }

        return $this->connection->fetchAllAssociative('
            SELECT
            pcv.production_scanned_at,
            pcv.travel_scanned_at,
            c.type_id
            FROM
            product_code_view pcv
            LEFT JOIN `code` c ON c.id = pcv.code_id
            WHERE
            pcv.product_id = :productId
            AND (
                (pcv.production_scanned_at >= :from AND pcv.production_scanned_at <= :to)
                OR (pcv.travel_scanned_at >= :from AND pcv.travel_scanned_at <= :to)
            )
        ', [
            'productId' => $productId,
            'from' => $from->format('Y-m-d H:i:s'),
            'to' => $to->format('Y-m-d H:i:s')
        ]);
    }
}
