<?php

declare(strict_types=1);

namespace ERP\Report\Infrastructure\Daily;

use DateTimeImmutable;

class InMemory implements DailyRepositoryInterface
{
    private array $memory = [];

    public function __construct(array $memory)
    {
        $this->memory = $memory;
    }

    public function getRawData(int $productId, DateTimeImmutable $from, ?DateTimeImmutable $to = null): array
    {
        $response = [];

        foreach ($this->memory as $item) {
            $productionScannedAt = new DateTimeImmutable($item['production_scanned_at']);
            $travelScannedAt = new DateTimeImmutable($item['travel_scanned_at']);

            if (
                ($productionScannedAt >= $from && $productionScannedAt <= $to)
                || ($travelScannedAt >= $from && $travelScannedAt <= $to)
            ) {
                $response[] = $item;
            }
        }

        return $response;
    }
}
