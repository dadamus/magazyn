<?php

declare(strict_types=1);

namespace ERP\Report\Infrastructure\Daily;

use DateTimeImmutable;

interface DailyRepositoryInterface
{
    public function getRawData(int $productId, DateTimeImmutable $from, ?DateTimeImmutable $to = null): array;
}
