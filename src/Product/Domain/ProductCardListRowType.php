<?php

namespace ERP\Product\Domain;

enum ProductCardListRowType: string
{
    case PRODUCTION = 'production';
    case TRAVEL = 'travel';
    case CORRECTION = 'correction';
}