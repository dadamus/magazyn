<?php

namespace ERP\Product\Domain;

use ERP\Code\Infrastructure\ProductCodeUsageStorageInterface;
use ERP\Code\Infrastructure\ProductCorrectionsStorageInterface;
use ERP\SharedKernel\Domain\DataTable\AbstractFilterData;

class ProductCardListService
{
    private ProductCodeUsageStorageInterface $productCodeUsageStorage;
    private ProductCorrectionsStorageInterface $productCorrectionsStorage;

    public function __construct(ProductCodeUsageStorageInterface $productCodeUsageStorage, ProductCorrectionsStorageInterface $productCorrectionsStorage)
    {
        $this->productCodeUsageStorage = $productCodeUsageStorage;
        $this->productCorrectionsStorage = $productCorrectionsStorage;
    }

    public function filter(int $productId, AbstractFilterData $abstractFilterData): array
    {
        $filters = $this->getFilters($abstractFilterData);
        $results = [];

        $typeFilter = $this->getFilter(ProductCardListColumns::TYPE, $filters);
        $typeFilterValue = $typeFilter === null ? null : $typeFilter['value'];

        if ($typeFilterValue === null
            || $typeFilterValue === ProductCardListRowType::PRODUCTION->value
            || $typeFilterValue === ProductCardListRowType::TRAVEL->value
        ) {
            $results = array_merge($results, self::standardiseProductCodeUsage($this->productCodeUsageStorage->find($productId)));
        }
        if ($typeFilterValue === null || $typeFilterValue === ProductCardListRowType::CORRECTION->value) {
            $results = array_merge($results, self::standardiseCorrections($this->productCorrectionsStorage->find($productId)));
        }

        $results = $this->normalizeResults($results);
        $results = self::filterResults($results, $filters);
        $results = $this->sortResults($results, $abstractFilterData->getOrder());

        return $this->formatResults($results, $abstractFilterData);
    }

    private static function filterResults(array $results, array $filters): array
    {
        foreach ($filters as $filter) {
            if ($filter['name'] === ProductCardListColumns::DATE->value) {
                $results = self::filterDateRangeColumn($results, $filter);
            } else {
                $results = array_filter($results, fn(array $row) => $row[$filter['name']] === $filter['value']);
            }
        }

        return $results;
    }

    private static function filterDateRangeColumn(array $results, array $dateFilter): array
    {
        $dateRange = explode('|', $dateFilter['value']);

        return array_filter($results, function (array $row) use ($dateRange) {
            $inLeftRange = true;
            $inRightRange = true;

            if ($dateRange[0] !== '') {
                $inLeftRange = strtotime($row[ProductCardListColumns::DATE->value]) >= strtotime($dateRange[0]);
            }
            if ($dateRange[1] !== '') {
                $inRightRange = strtotime($row[ProductCardListColumns::DATE->value]) <= strtotime($dateRange[1]);
            }

            return $inLeftRange && $inRightRange;
        });
    }

    private static function standardiseCorrections(array $corrections): array
    {
        return array_map(fn(array $correction) => [
            ProductCardListColumns::ID->value => $correction['id'],
            ProductCardListColumns::TYPE->value => ProductCardListRowType::CORRECTION->value,
            ProductCardListColumns::VALUE->value => $correction['value'],
            ProductCardListColumns::DATE->value => $correction['date'],
        ], $corrections);
    }

    private static function standardiseProductCodeUsage(array $usages): array
    {
        foreach ($usages as $key => $usage) {
            if ($usage['travel_id'] !== null) {
                $usages[$key]['travel_id'] = null;
                $usages[] = $usage;
            }
        }

        return array_map(fn(array $codeUsage) => [
            ProductCardListColumns::ID->value => $codeUsage['id'],
            ProductCardListColumns::TYPE->value => $codeUsage['travel_id'] === null ? ProductCardListRowType::PRODUCTION->value : ProductCardListRowType::TRAVEL->value,
            ProductCardListColumns::DEVICE_NAME->value => $codeUsage['device_name'],
            ProductCardListColumns::VALUE->value => $codeUsage['ean'],
            ProductCardListColumns::DATE->value => $codeUsage['travel_id'] === null ? $codeUsage['production_scanned_at'] : $codeUsage['travel_scanned_at'],
        ], $usages);
    }

    private function formatResults(array $results, AbstractFilterData $filters): array
    {
        $totalRecords = count($results);
        $results = array_slice($results, $filters->getStart(), $filters->getLength());

        return [
            'data' => $results,
            'draw' => 50,
            'recordsFiltered' => count($results),
            'recordsTotal' => $totalRecords
        ];
    }

    private function sortResults(array $results, array $orders): array
    {
        if (empty($orders)) {
            $order = [
                'column' => ProductCardListColumns::ID->value,
                'dir' => 'asc'
            ];
        } else {
            $order = reset($orders);
        }

        $sortDirection = $order['dir'];
        $sortColumn = $order['column'];
        if ($sortColumn === ProductCardListColumns::DATE->value) {
            usort($results, function (array $a, array $b) use ($sortColumn, $sortDirection) {
                if (strtotime($a[$sortColumn]) < strtotime($b[$sortColumn])) {
                    if ($sortDirection === 'asc') {
                        return -1;
                    } else {
                        return 1;
                    }
                } else {
                    if ($sortDirection === 'asc') {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            });
        } else {
            usort($results, function (array $a, array $b) use ($sortColumn, $sortDirection) {
                if ((int)$a[$sortColumn] < (int)$b[$sortColumn]) {
                    if ($sortDirection === 'asc') {
                        return -1;
                    } else {
                        return 1;
                    }
                } else {
                    if ($sortDirection === 'asc') {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            });
        }

        return $results;
    }

    private function normalizeResults(array $results): array
    {
        $rowDefaults = [
            ProductCardListColumns::ID->value => null,
            ProductCardListColumns::TYPE->value => null,
            ProductCardListColumns::DEVICE_NAME->value => null,
            ProductCardListColumns::DATE->value => null,
            ProductCardListColumns::VALUE->value => null
        ];

        foreach ($results as $key => $value) {
            $results[$key] = array_merge($rowDefaults, $value);
        }

        return $results;
    }

    private function getFilters(AbstractFilterData $filterData): array
    {
        $filters = [];

        foreach ($filterData->getColumns() as $column) {
            $searchValue = $column['search']['value'];
            $filterType = '';

            if (isset($column['search']['type'])) {
                $filterType = $column['search']['type'];
            }

            if ($searchValue === null || $searchValue === '') {
                continue;
            }

            $filters[] = [
                'name' => $column['data'],
                'value' => $searchValue,
                'type' => $filterType
            ];
        }

        return $filters;
    }

    private function getFilter(ProductCardListColumns $column, array $filters): ?array
    {
        foreach ($filters as $filter) {
            if ($filter['name'] === $column->value) {
                return $filter;
            }
        }

        return null;
    }
}