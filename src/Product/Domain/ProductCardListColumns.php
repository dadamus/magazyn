<?php

namespace ERP\Product\Domain;

enum ProductCardListColumns: string
{
    case ID = 'id';
    case TYPE = 'type';
    case DEVICE_NAME = 'device_name';
    case DATE = 'date';
    case VALUE = 'value';
}