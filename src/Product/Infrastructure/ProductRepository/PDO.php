<?php

namespace ERP\Product\Infrastructure\ProductRepository;

use Doctrine\DBAL\Connection;
use ERP\Product\Infrastructure\Exception\ProductNotFound;
use ERP\Product\Infrastructure\ProductRepositoryInterface;

class PDO implements ProductRepositoryInterface
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function get(int $productId): array
    {
        $data = $this->connection->fetchAssociative('
            SELECT
            p.*,
            ps.id as position_id,
            t.name as type_name,
            c.name as category_name
            FROM
            product p 
            LEFT JOIN type t on p.type_id = t.id
            LEFT JOIN category c on p.category_id = c.id
            LEFT JOIN product_position pp ON pp.product_id = p.id
            LEFT JOIN position ps ON ps.id = pp.position_id
            WHERE
            p.id = :id
        ', [
            'id' => $productId
        ]);

        if (!$data) {
            throw new ProductNotFound($productId);
        }

        return $data;
    }

    public function find(string $productName): array
    {
        return $this->connection->fetchAllAssociative('
            SELECT
            p.name as text,
            p.id as id
            FROM
            product p 
            WHERE
            p.name LIKE "%' . $productName . '%"
        ');
    }

    public function add(string $name, int $categoryId, int $typeId, string $description = ''): int
    {
        $this->connection->executeStatement('
            INSERT INTO product
            (name, description, category_id, type_id)
            VALUES (:name, :description, :categoryId, :typeId)
        ', [
            'name' => $name,
            'description' => $description,
            'categoryId' => $categoryId,
            'typeId' => $typeId
        ]);

        return $this->connection->lastInsertId();
    }

    public function edit(int $productId, string $name, int $categoryId, int $typeId, string $description = ''): void
    {
        $this->connection->executeStatement('
            UPDATE product SET
            name = :name,
            description = :description,
            category_id = :categoryId,
            type_id = :typeId
            WHERE
            id = :id
        ', [
            'id' => $productId,
            'name' => $name,
            'description' => $description,
            'categoryId' => $categoryId,
            'typeId' => $typeId
        ]);
    }

    public function assignPosition(int $productId, array $positions): void
    {
        $this->connection->executeStatement('
            DELETE FROM product_position WHERE product_id = :productId
        ', [
            'productId' => $productId
        ]);

        foreach ($positions as $positionId) {
            $this->connection->executeStatement('
                INSERT INTO product_position
                (product_id, position_id) 
                VALUES (:productId, :positionId)
            ', [
                'productId' => $productId,
                'positionId' => $positionId
            ]);
        }
    }

    public function getList(): array
    {
        return $this->connection->fetchAllAssociative('
            SELECT
            id, name
            FROM product
        ');
    }
}
