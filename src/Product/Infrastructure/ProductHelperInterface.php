<?php

namespace ERP\Product\Infrastructure;

interface ProductHelperInterface
{
    public function getAllVisibleCategories(): array;

    public function getAllVisibleTypes(): array;
}
