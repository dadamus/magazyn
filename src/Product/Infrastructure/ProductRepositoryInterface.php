<?php

namespace ERP\Product\Infrastructure;

interface ProductRepositoryInterface
{
    public function get(int $productId): array;

    public function find(string $productName): array;

    public function add(string $name, int $categoryId, int $typeId, string $description = ''): int;

    public function edit(int $productId, string $name, int $categoryId, int $typeId, string $description = ''): void;

    public function assignPosition(int $productId, array $positions): void;

    public function getList(): array;
}
