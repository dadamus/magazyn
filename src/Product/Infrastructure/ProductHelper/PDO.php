<?php

namespace ERP\Product\Infrastructure\ProductHelper;

use Doctrine\DBAL\Connection;
use ERP\Product\Infrastructure\ProductHelperInterface;

class PDO implements ProductHelperInterface
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getAllVisibleCategories(): array
    {
        return $this->connection->fetchAllAssociative('
            SELECT 
            *
            FROM 
            category
            WHERE
            visible = 1
        ');
    }

    public function getAllVisibleTypes(): array
    {
        return $this->connection->fetchAllAssociative('
            SELECT 
            *
            FROM 
            type
            WHERE
            visible = 1
        ');
    }

}
