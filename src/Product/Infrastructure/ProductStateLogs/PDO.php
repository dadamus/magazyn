<?php

namespace ERP\Product\Infrastructure\ProductStateLogs;

use DateTimeImmutable;
use Doctrine\DBAL\Connection;
use ERP\Product\Infrastructure\ProductStateLogsInterface;

class PDO implements ProductStateLogsInterface
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function get(int $productId): array
    {
        return [];
    }

    public function getAll(int $limit = 15): array
    {
        return $this->connection->fetchAllAssociative('
            SELECT
            psl.date,
            p.name as product_name,
            d.name as device_name
            FROM
            product_state_log psl
            LEFT JOIN product p ON p.id = psl.product_id
            LEFT JOIN device d ON d.id = psl.device_id
            WHERE
            psl.code_id > 0
            ORDER BY psl.`date` DESC
            LIMIT ' . $limit . '
        ');
    }

    public function save(
        int $productId,
        int $amount,
        DateTimeImmutable $date,
        array $codeIds = [],
        int $deviceId = null
    ) {
        foreach ($codeIds as $codeId) {
            $this->connection->executeStatement('
            INSERT INTO
            product_state_log
            (product_id, code_id, device_id, amount, date) 
            VALUES (:product_id, :code_id, :device_id, :amount, :date) 
        ', [
                'product_id' => $productId,
                'code_id' => $codeId,
                'device_id' => $deviceId,
                'amount' => $amount,
                'date' => $date->format('Y-m-d H:i:s')
            ]);
        }
    }

    public function remove(int $codeId): void
    {
        $this->connection->executeStatement('
            DELETE FROM product_state_log WHERE code_id = :codeId
        ', [
            'codeId' => $codeId
        ]);
    }

}
