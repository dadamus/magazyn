<?php

namespace ERP\Product\Infrastructure;

use DateTimeImmutable;

interface ProductStateLogsInterface
{
    public function get(int $productId): array;

    public function getAll(int $limit = 15): array;

    public function save(
        int $productId,
        int $amount,
        DateTimeImmutable $date,
        array $codeIds = [],
        int $deviceId = null
    );

    public function remove(int $codeId): void;
}
