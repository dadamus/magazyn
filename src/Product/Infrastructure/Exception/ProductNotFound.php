<?php

namespace ERP\Product\Infrastructure\Exception;

use Exception;

class ProductNotFound extends Exception
{
    public function __construct(int $productId)
    {
        parent::__construct('Produkt o id: ' . $productId . ' nie został znaleziony!');
    }
}
