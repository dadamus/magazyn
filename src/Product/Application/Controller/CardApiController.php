<?php

declare(strict_types=1);

namespace ERP\Product\Application\Controller;

use ERP\Code\Domain\CodeCorrectionType;
use ERP\Code\Domain\CodeGenerators\ProductCodeGenerator;
use ERP\Code\Domain\Printer;
use ERP\Code\Domain\ProductCode;
use ERP\Code\Infrastructure\PrinterInterface;
use ERP\Code\Infrastructure\ProductCodeRepositoryInterface;
use ERP\Code\Infrastructure\ProductCorrectionsStorageInterface;
use ERP\Product\Domain\ProductCardListService;
use ERP\Settings\Infrastructure\DeviceRepositoryInterface;
use ERP\Settings\Infrastructure\PositionRepositoryInterface;
use ERP\SharedKernel\Application\Controller\CoreController;
use ERP\SharedKernel\Domain\DataTable\Data\Post;
use ERP\SharedKernel\Domain\DataTable\Data\ReactJson;
use ERP\SharedKernel\Domain\DataTable\Engine;
use ERP\SharedKernel\Domain\DataTableTranslator\ProductStateCorrectionsDataTranslatorAbstract;
use ERP\SharedKernel\Domain\DataTableTranslator\ProductStateDataTranslatorAbstract;
use ERP\SharedKernel\Domain\Security\User;
use ERP\SharedKernel\Infrastructure\DataTableQuery\ProductStates;
use ERP\SharedKernel\Infrastructure\DataTableQuery\ProductStatesCorrections;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CardApiController extends CoreController
{
    /**
     * @Route("/api/product/card/{productId}/generate", methods={"POST"})
     */
    public function generateAction(
        int                  $productId,
        Request              $request,
        ProductCodeGenerator $productCodeGenerator,
        Printer              $printer,
        PrinterInterface     $codePrinter,
        $isPrinterEnabled
    ): Response
    {
        $requestData = json_decode($request->getContent(false), true);
        $codeLength = (int)$requestData['code_length'];

        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        $codes = [];

        for ($i = 0; $i < $codeLength; $i++) {
            $codes[] = $productCodeGenerator->generate($productId, $user->getUserId());
        }

        $printerGroupId = $printer->createPrintGroup($codes);

        if ($isPrinterEnabled) {
            $codePrinter->printGroup($printerGroupId);
        }

        return new JsonResponse([
            'printerGroupId' => $printerGroupId,
            'ipPrinterEnabled' => $isPrinterEnabled
        ]);
    }

    /**
     * @Route("/api/product/card/{productId}/correction", methods={"POST"})
     */
    public function addCorrection(
        int                                $productId,
        Request                            $request,
        ProductCorrectionsStorageInterface $correctionsStorage
    ): Response
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $requestContent = json_decode($request->getContent(), true);
        $correctionValue = (int)$requestContent['correction-value'];

        $correctionsStorage->addProductCorrection(
            $productId,
            $correctionValue,
            $user->getUserId()
        );

        return new JsonResponse('Added correction!');
    }

    /**
     * @Route("/api/product/card/{productId}/state", methods={"POST"})
     */
    public function productStateList(int $productId, ProductCardListService $productCardListService, Request $request): JsonResponse
    {
        $filters = new ReactJson($request);

        return new JsonResponse($productCardListService->filter($productId, $filters));
    }

    /**
     * @Route("/api/product/card/device-list", methods={"GET"})
     */
    public function devicesList(DeviceRepositoryInterface $devices): JsonResponse
    {
        return new JsonResponse($devices->getAll());
    }
}
