<?php

namespace ERP\Product\Application\Controller;

use ERP\Code\Infrastructure\PrinterInterface;
use ERP\Settings\Infrastructure\DeviceRepositoryInterface;
use ERP\Settings\Infrastructure\PositionRepositoryInterface;
use ERP\SharedKernel\Application\Controller\CoreController;
use ERP\Code\Domain\CodeGenerators\ProductCodeGenerator;
use ERP\Code\Domain\Printer;
use ERP\Code\Domain\ProductCode;
use ERP\Code\Infrastructure\ProductCodeRepositoryInterface;
use ERP\Product\Infrastructure\ProductRepositoryInterface;
use ERP\SharedKernel\Domain\DataTable\Data\Post;
use ERP\SharedKernel\Domain\DataTable\Engine;
use ERP\SharedKernel\Domain\DataTableTranslator\ProductStateDataTranslatorAbstract;
use ERP\SharedKernel\Domain\Security\User;
use ERP\SharedKernel\Infrastructure\DataTableQuery\ProductStates;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CardController extends CoreController
{

    public function indexAction(
        ProductRepositoryInterface $products,
        DeviceRepositoryInterface $devices,
        PositionRepositoryInterface $positions,
        int $productId
    ): Response {
        return $this->render('Product/product_card.html.twig', [
            'productId' => $productId,
            'devices' => $devices->getAll(),
            'positions' => $positions->getAll(true)
        ]);
    }


    public function generateAction(
        int $productId,
        Request $request,
        ProductCodeGenerator $productCodeGenerator,
        Printer $printer,
        PrinterInterface $codePrinter
    ): Response {
        $codeLength = $request->request->getInt('code_length');

        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        $codes = [];

        for ($i = 0; $i < $codeLength; $i++) {
            $codes[] = $productCodeGenerator->generate($productId, $user->getUserId());
        }

        $printerGroupId = $printer->createPrintGroup($codes);

        $ipPrinterEnabled = getenv('CODE_IP_PRINTER_ENABLED');
        if ($ipPrinterEnabled) {
            $codePrinter->printGroup($printerGroupId);
        }

        return new JsonResponse([
            'printerGroupId' => $printerGroupId,
            'ipPrinterEnabled' => $ipPrinterEnabled
        ]);
    }


    public function productStateList(int $productId, Engine $dataTable, Request $request): Response
    {
        $productStatesDataTableQuery = new ProductStates($productId);
        $productStatesDataTranslate = new ProductStateDataTranslatorAbstract();

        $dataTable->setFilterData(new Post($request));
        $dataTable->setQueryBuilder($productStatesDataTableQuery);
        $dataTable->init($productStatesDataTranslate);

        return new JsonResponse($dataTable->search());
    }


    public function findProduct(Request $request, ProductRepositoryInterface $products): Response
    {
        return new JsonResponse([
            'results' => $products->find($request->query->get('term'))
        ]);
    }


    public function addCorrection(
        int $productId,
        Request $request,
        ProductCodeRepositoryInterface $codeProducts
    ): Response {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $product = $codeProducts->get($productId);

        if (!$product) {
            $product = ProductCode::create($productId);
        }

        $product->addProductionCorrection($request->request->getInt('correction-value'), $user->getUserId());

        $codeProducts->save($product);

        return new JsonResponse('Added correction!');
    }
}
