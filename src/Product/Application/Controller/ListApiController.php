<?php

declare(strict_types=1);

namespace ERP\Product\Application\Controller;

use ERP\Product\Infrastructure\ProductHelperInterface;
use ERP\Product\Infrastructure\ProductRepositoryInterface;
use ERP\Settings\Infrastructure\PositionRepositoryInterface;
use ERP\SharedKernel\Application\Controller\CoreController;
use ERP\SharedKernel\Domain\DataTable\Data\ReactJson;
use ERP\SharedKernel\Domain\DataTable\Engine;
use ERP\SharedKernel\Domain\DataTableTranslator\ProductDataTranslatorAbstract;
use ERP\SharedKernel\Infrastructure\DataTableQuery\Products as ProductDataTableQuery;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListApiController extends CoreController
{
    /**
     * @Route("/api/product/types", methods={"GET"})
     */
    public function getTypesAction(ProductHelperInterface $productsHelpers): JsonResponse
    {
        return new JsonResponse($productsHelpers->getAllVisibleTypes());
    }

    /**
     * @Route("/api/product/category", methods={"GET"})
     */
    public function getCategoryAction(ProductHelperInterface $productHelper): JsonResponse
    {
        return new JsonResponse($productHelper->getAllVisibleCategories());
    }

    /**
     * @Route("/api/product/positions", methods={"GET"})
     */
    public function getPositionAction(PositionRepositoryInterface $positions): JsonResponse
    {
        return new JsonResponse($positions->getAll(true));
    }

    /**
     * @Route("/api/products/list/filter", methods={"POST"})
     */
    public function filterListAction(
        Request $request,
        Engine $dataTable,
        ProductDataTableQuery $query,
        ProductDataTranslatorAbstract $translator
    ): JsonResponse {
        $dataTable->setFilterData(new ReactJson($request));
        $dataTable->setQueryBuilder($query);
        $dataTable->init($translator);

        return new JsonResponse($dataTable->search());
    }

    /**
     * @Route("/api/product", methods={"POST"})
     */
    public function addProduct(ProductRepositoryInterface $products, Request $request): Response
    {
        $content = json_decode($request->getContent(), true);

        $productName = $content['name'];
        $productDescription = $content['description'] ?? '';
        $positionId = (int)$content['position_id'];
        $categoryId = (int)$content['category_id'];
        $typeId = (int)$content['type_id'];

        $productId = $products->add($productName, $categoryId, $typeId, $productDescription);
        $products->assignPosition($productId, [$positionId]);

        return new JsonResponse('Product added!');
    }

    /**
     * @Route("/api/product", methods={"PUT"})
     */
    public function editProduct(ProductRepositoryInterface $products, Request $request): Response
    {
        $content = json_decode($request->getContent(), true);

        $productId = (int)$content['id'];
        $productName = $content['name'];
        $productDescription = $content['description'] ?? '';
        $positionId = (int)$content['position_id'];
        $categoryId = (int)$content['category_id'];
        $typeId = (int)$content['type_id'];

        $products->edit($productId, $productName, $categoryId, $typeId, $productDescription);
        $products->assignPosition($productId, [$positionId]);

        return new JsonResponse('Product edited!');
    }
}
