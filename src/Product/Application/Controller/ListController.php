<?php

namespace ERP\Product\Application\Controller;

use ERP\Settings\Infrastructure\PositionRepositoryInterface;
use  ERP\SharedKernel\Application\Controller\CoreController;
use ERP\SharedKernel\Domain\DataTable\Data\Post;
use ERP\SharedKernel\Domain\DataTable\Engine;
use ERP\SharedKernel\Domain\DataTableTranslator\ProductDataTranslatorAbstract;
use ERP\SharedKernel\Infrastructure\DataTableQuery\Products as ProductDataTableQuery;
use ERP\Product\Infrastructure\ProductHelperInterface;
use ERP\Product\Infrastructure\ProductRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends CoreController
{
    public function indexAction(ProductHelperInterface $productsHelpers): Response
    {
        return $this->render('Product/product_list.html.twig', [
            'categories' => $productsHelpers->getAllVisibleCategories(),
            'types' => $productsHelpers->getAllVisibleTypes()
        ]);
    }

    public function filterAction(
        Engine $dataTable,
        ProductDataTableQuery $query,
        ProductDataTranslatorAbstract $translator,
        Request $request
    ): Response {
        $dataTable->setFilterData(new Post($request));
        $dataTable->setQueryBuilder($query);
        $dataTable->init($translator);

        return new JsonResponse($dataTable->search());
    }

    public function showProductModalAction(
        ProductRepositoryInterface $products,
        ProductHelperInterface $productsHelpers,
        PositionRepositoryInterface $positions,
        int $productId = 0
    ): Response {
        $product = [];

        if ($productId > 0) {
            $product = $products->get($productId);
        }

        return $this->render('Product/modal_content.html.twig', [
            'product' => $product,
            'positions' => $positions->getAll(true),
            'categories' => $productsHelpers->getAllVisibleCategories(),
            'types' => $productsHelpers->getAllVisibleTypes()
        ]);
    }

    public function addProduct(ProductRepositoryInterface $products, Request $request): Response
    {
        $productName = $request->request->get('name');
        $productDescription = $request->request->get('description');
        $positionId = $request->request->getInt('position_id');
        $categoryId = $request->request->getInt('category_id');
        $typeId = $request->request->getInt('type_id');

        $productId = $products->add($productName, $categoryId, $typeId, $productDescription);
        $products->assignPosition($productId, [$positionId]);

        return new JsonResponse('Product added!');
    }

    public function editProduct(ProductRepositoryInterface $products, int $productId, Request $request): Response
    {
        $productName = $request->request->get('name');
        $productDescription = $request->request->get('description');
        $positionId = $request->request->getInt('position_id');
        $categoryId = $request->request->getInt('category_id');
        $typeId = $request->request->getInt('type_id');

        $products->edit($productId, $productName, $categoryId, $typeId, $productDescription);
        $products->assignPosition($productId, [$positionId]);

        return new JsonResponse('Product added!');
    }
}
