<?php

declare(strict_types=1);

namespace ERP\Product\Application\Command;

use ERP\Code\Domain\CodeGenerators\ProductCodeGenerator;
use ERP\Code\Domain\CodeGenerators\TravelCodeGenerator;
use ERP\Product\Infrastructure\ProductRepositoryInterface;
use ERP\Settings\Infrastructure\CarRepositoryInterface;
use ERP\Travel\Infrastructure\TravelRepositoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProductCodeGeneratorCommand extends Command
{
    private const PRODUCT_QUANTITY = 10;
    private const CODE_QUANTITY = 50;
    private const TRAVEL_QUANTITY = 3;
    private const AUTHOR_ID = 3;
    private const PRODUCT_TYPE_ID = 2;
    private const PRODUCT_CATEGORY_ID = 2;

    private ProductRepositoryInterface $productRepository;

    private TravelRepositoryInterface $travelRepository;

    private CarRepositoryInterface $carRepository;

    private ProductCodeGenerator $productCodeGenerator;

    private TravelCodeGenerator $travelCodeGenerator;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        TravelRepositoryInterface $travelRepository,
        ProductCodeGenerator $productCodeGenerator,
        TravelCodeGenerator $travelCodeGenerator,
        CarRepositoryInterface $carRepository
    ) {
        $this->productRepository = $productRepository;
        $this->travelRepository = $travelRepository;
        $this->productCodeGenerator = $productCodeGenerator;
        $this->travelCodeGenerator = $travelCodeGenerator;
        $this->carRepository = $carRepository;

        parent::__construct('product:generate');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $travels = $this->generateTravels();
        $products = $this->generateProducts($travels);

        $productCodes = $this->generateProductCode($products);
        $travelCodes = $this->generateTravelCode($travels);
    }

    private function generateTravelCode(array $travels): array
    {
        $codes = [];

        foreach ($travels as $travelId) {
            if (!isset($codes[$travelId])) {
                $codes[$travelId] = [];
            }

            for ($i = 0; $i < self::CODE_QUANTITY; $i++) {
                $codes[$travelId][] = $this->travelCodeGenerator->generate($travelId, self::AUTHOR_ID);
            }
        }

        return $codes;
    }

    private function generateProductCode(array $products): array
    {
        $codes = [];

        foreach ($products as $productId) {
            if (!isset($codes[$productId])) {
                $codes[$productId] = [];
            }

            for ($i = 0; $i < self::CODE_QUANTITY; $i++) {
                $codes[$productId][] = $this->productCodeGenerator->generate($productId, self::AUTHOR_ID);
            }
        }

        return $codes;
    }

    private function generateProducts(array $travels): array
    {
        $products = [];

        for ($i = 0; $i < self::PRODUCT_QUANTITY; $i++) {
            $product = $this->productRepository->add(
                'Product ' . $i,
                self::PRODUCT_CATEGORY_ID,
                self::PRODUCT_TYPE_ID
            );

            foreach ($travels as $travel) {
                $this->travelRepository->addProduct($travel, $product);
            }

            $products[] = $product;
        }

        return $products;
    }

    private function generateTravels(): array
    {
        $carId = $this->carRepository->add('KR 000000');

        $travels = [];

        for ($i = 0; $i < self::TRAVEL_QUANTITY; $i++) {
            $travels[] = $this->travelRepository->add(
                'travel ' . $i,
                new \DateTimeImmutable(),
                $carId,
                self::AUTHOR_ID
            );
        }

        return $travels;
    }
}
