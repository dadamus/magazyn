<?php

namespace ERP\Code\Domain;

use DateTimeImmutable;

abstract class AbstractCode
{
    protected ?int $id;

    protected string $ean;

    protected DateTimeImmutable $createdAt;

    protected int $createdBy;

    public function __construct(?int $id, string $ean, DateTimeImmutable $createdAt, int $createdBy)
    {
        $this->id = $id;
        $this->ean = $ean;
        $this->createdAt = $createdAt;
        $this->createdBy = $createdBy;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEan(): string
    {
        return $this->ean;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCreatedBy(): int
    {
        return $this->createdBy;
    }
}