<?php

namespace ERP\Code\Domain;

use MyCLabs\Enum\Enum;

/**
 * @method static CodeCorrectionType TRAVEL()
 * @method static CodeCorrectionType PRODUCTION()
 */
final class CodeCorrectionType extends Enum
{
    public const TRAVEL = 'travel';
    public const PRODUCTION = 'production';
}