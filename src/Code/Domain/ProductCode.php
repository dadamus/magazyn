<?php

namespace ERP\Code\Domain;

use DateTimeImmutable;

class ProductCode extends AbstractCode
{
    protected int $productId;

    public function __construct(int $id, string $ean, int $productId, DateTimeImmutable $createdAt, int $createdBy)
    {
        parent::__construct($id, $ean, $createdAt, $createdBy);

        $this->productId = $productId;
    }

    public function getProductId(): int
    {
        return $this->productId;
    }
}