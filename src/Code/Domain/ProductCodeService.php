<?php

namespace ERP\Code\Domain;

use DateTimeImmutable;
use ERP\Code\Infrastructure\ProductCodeUsageStorageInterface;
use ERP\Settings\Domain\Device\Device;

class ProductCodeService
{
    private ProductCodeUsageStorageInterface $productCodeUsageStorage;

    public function __construct(ProductCodeUsageStorageInterface $productCodeUsageStorage)
    {
        $this->productCodeUsageStorage = $productCodeUsageStorage;
    }

    public function storeProductionUsage(AbstractCode $code, Device $device, DateTimeImmutable $scannedAt): void
    {
        $this->productCodeUsageStorage->storeProductionUsage($code->getEan(), $device->getId(), $scannedAt);
    }

    public function storeTravelUsage(ProductCode $code, Device $device, DateTimeImmutable $scannedAt): void
    {
        $this->productCodeUsageStorage->storeTravelUsage($code->getEan(), $device->getLastTravelId(), $device->getId(), $scannedAt);
    }

    public function hasProductionUsage(AbstractCode $code): bool
    {
        return $this->productCodeUsageStorage->hasProductionUsage($code->getEan());
    }

    public function hasTravelUsage(AbstractCode $code): bool
    {
        return $this->productCodeUsageStorage->hasTravelUsage($code->getEan());
    }
}