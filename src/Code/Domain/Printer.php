<?php

declare(strict_types=1);

namespace ERP\Code\Domain;

use ERP\Code\Infrastructure\PrinterGroupRepositoryInterface;
use Ramsey\Uuid\Uuid;

class Printer
{
    private PrinterGroupRepositoryInterface $printerGroups;

    public function __construct(PrinterGroupRepositoryInterface $printerGroups)
    {
        $this->printerGroups = $printerGroups;
    }

    public function createPrintGroup(array $codes): string
    {
        $id = Uuid::uuid4()->toString();

        $this->printerGroups->save($id, $codes);

        return $id;
    }

    public function getCodes(string $uuid): array
    {
        return $this->printerGroups->getCodeList($uuid);
    }
}
