<?php

namespace ERP\Code\Domain;

use DateTimeImmutable;

class TravelCode extends AbstractCode
{
    private int $travelId;

    public function __construct(string $ean, int $travelId, int $createdBy)
    {
        parent::__construct(null, $ean, new DateTimeImmutable(), $createdBy);

        $this->travelId = $travelId;
    }

    public function getTravelId(): int
    {
        return $this->travelId;
    }
}