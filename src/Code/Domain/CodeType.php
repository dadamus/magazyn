<?php

declare(strict_types=1);

namespace ERP\Code\Domain;

use MyCLabs\Enum\Enum;

/**
 * @method static CodeType PRODUCTION()
 * @method static CodeType TRAVEL()
 * @method static CodeType STOP()
 * @method static CodeType PRODUCT_CORRECTION()
 * @method static CodeType TRAVEL_CORRECTION()
 */
class CodeType extends Enum
{
    const PRODUCTION = 1;
    const TRAVEL = 2;
    const STOP = 3;
    const PRODUCT_CORRECTION = 4;
    const TRAVEL_CORRECTION = 5;
}
