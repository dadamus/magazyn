<?php

namespace ERP\Code\Domain\CodeGenerators;

use ERP\Code\Infrastructure\ProductCodeStorageInterface;

class ProductCodeGenerator
{
    private ProductCodeStorageInterface $productCodeStorage;

    public function __construct(ProductCodeStorageInterface $productCodeStorage)
    {
        $this->productCodeStorage = $productCodeStorage;
    }

    public function generate(int $productId, int $userId): string
    {
        $dailyNumber = $this->productCodeStorage->getDailyCodeNumber() + 1;
        $dailyNumber = str_pad($dailyNumber, 5, "0", STR_PAD_LEFT);

        $code = "1";
        $code .= str_pad(date('d'), 2, "0", STR_PAD_LEFT);
        $code .= str_pad(date('m'), 2, "0", STR_PAD_LEFT);
        $code .= date('Y') - 2000;
        $code .= $dailyNumber;

        $this->productCodeStorage->saveCode($code, $productId, $userId);

        return $code;
    }
}
