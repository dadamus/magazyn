<?php

namespace ERP\Code\Domain\CodeGenerators;


use ERP\Code\Domain\TravelCode;
use ERP\Code\Infrastructure\TravelCodeStorageInterface;

class TravelCodeGenerator
{
    private TravelCodeStorageInterface $travelCodeStorage;

    public function __construct(TravelCodeStorageInterface $travelCodeStorage)
    {
        $this->travelCodeStorage = $travelCodeStorage;
    }

    public function generate(int $travelId, int $userId): string
    {
        $hasTravelCode = $this->travelCodeStorage->getByTravel($travelId);

        if ($hasTravelCode !== null) {
            return $hasTravelCode->getEan();
        }

        $dailyNumber = $this->travelCodeStorage->getDailyCodeNumber() + 1;
        $dailyNumber = str_pad($dailyNumber, 3, "0", STR_PAD_LEFT);

        $code = "2";
        $code .= str_pad(date('d'), 2, "0", STR_PAD_LEFT);
        $code .= str_pad(date('m'), 2, "0", STR_PAD_LEFT);
        $code .= date('Y') - 2000;
        $code .= $dailyNumber;
        $code .= "00";

        $travelCode = new TravelCode($code, $travelId, $userId);

        $this->travelCodeStorage->saveCode($travelCode);

        return $code;
    }
}
