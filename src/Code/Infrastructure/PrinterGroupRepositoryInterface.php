<?php

declare(strict_types=1);

namespace ERP\Code\Infrastructure;

interface PrinterGroupRepositoryInterface
{
    public function save(string $uuid, array $codes);

    public function getCodeList(string $uuid): array;
}
