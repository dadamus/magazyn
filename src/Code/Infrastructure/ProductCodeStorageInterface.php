<?php

namespace ERP\Code\Infrastructure;

use ERP\Code\Domain\ProductCode;

interface ProductCodeStorageInterface
{
    public function getCode(string $ean): ?ProductCode;

    public function getDailyCodeNumber(): int;

    public function saveCode(string $code, string $productId, int $userId): void;
}