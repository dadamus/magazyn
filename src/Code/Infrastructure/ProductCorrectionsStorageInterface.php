<?php

namespace ERP\Code\Infrastructure;

interface ProductCorrectionsStorageInterface
{
    public function addProductCorrection(int $productId, int $value, int $userId): void;

    public function find(int $productId): array;
}