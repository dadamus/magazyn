<?php

declare(strict_types=1);

namespace ERP\Code\Infrastructure\Printer;

use ERP\Code\Infrastructure\PrinterGroupRepositoryInterface;
use ERP\Code\Infrastructure\PrinterInterface;

class SocketPrinter implements PrinterInterface
{
    private const TEMPLATE_PATH = __DIR__ . '/printer_template.template';

    public function __construct(
        private PrinterGroupRepositoryInterface $printerGroupRepository,
        private string                          $printerIp,
        private string                          $printerPort)
    {
    }

    public function printCode(string $name, string $ean): void
    {
        $template = file_get_contents(self::TEMPLATE_PATH);

        $socket = $this->connectToPrinter();

        $barcode = sprintf("%s%s", $ean, $this->getChecksum($ean));
        $data = str_replace('{detail_name}', $name, $template);
        $data = str_replace('{barcode}', $barcode, $data);
        fputs($socket, $data);

        fclose($socket);
    }

    public function printGroup(string $printerGroupId): void
    {
        $template = file_get_contents(self::TEMPLATE_PATH);
        $codeList = $this->printerGroupRepository->getCodeList($printerGroupId);

        $socket = $this->connectToPrinter();

        foreach ($codeList as $codeData) {
            $barcode = sprintf("%s%s", $codeData['code'], $this->getChecksum($codeData['code']));
            $data = str_replace('{detail_name}', $codeData['name'], $template);
            $data = str_replace('{barcode}', $barcode, $data);
            fputs($socket, $data);
        }

        fclose($socket);
    }

    private function getChecksum(string $ean): int
    {
        $even = true;
        $esum = 0;
        $osum = 0;
        for ($i = strlen($ean) - 1; $i >= 0; $i--) {
            if ($even) $esum += (int)$ean[$i]; else $osum += (int)$ean[$i];
            $even = !$even;
        }

        return (10 - ((3 * $esum + $osum) % 10)) % 10;
    }

    private function connectToPrinter(): mixed
    {
        $host = $this->printerIp;
        $port = (int)$this->printerPort;

        $errno = null;
        $errst = null;

        return fSockOpen($host, $port, $errno, $errst, 10);
    }
}
