<?php

namespace ERP\Code\Infrastructure\ProductCodeStorage;

use Doctrine\DBAL\Connection;
use ERP\Code\Domain\ProductCode;
use ERP\Code\Infrastructure\ProductCodeStorageInterface;

class PDO implements ProductCodeStorageInterface
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getCode(string $ean): ?ProductCode
    {
        $result = $this->connection->executeQuery(
            'SELECT id, ean, product_id, user_id, created_at FROM product_code_storage WHERE ean = :ean',
            [
                'ean' => $ean
            ]
        );

        $codeData = $result->fetchAssociative();

        if ($codeData === false) {
            return null;
        }

        return new ProductCode(
            $codeData['id'],
            $codeData['ean'],
            $codeData['product_id'],
            new \DateTimeImmutable($codeData['created_at']),
            $codeData['user_id'],
        );
    }

    public function getDailyCodeNumber(): int
    {$today = date('Y-m-d 00:00:00');

        $result = $this->connection->executeQuery('
            SELECT
                count(*) as `count`
            FROM
                product_code_storage
            WHERE
                created_at >= :today
        ', [
            'today' => $today
        ]);

        $data = $result->fetchAssociative();

        return $data['count'];

    }

    public function saveCode(string $code, string $productId, int $userId): void
    {
        $this->connection->executeStatement('
            INSERT INTO 
                product_code_storage
            (`ean`, `product_id`, `user_id`) VALUES (:ean, :productId, :userId)
        ', [
            'ean' => $code,
            'productId' => $productId,
            'userId' => $userId
        ]);
    }
}