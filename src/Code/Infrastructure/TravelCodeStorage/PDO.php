<?php

namespace ERP\Code\Infrastructure\TravelCodeStorage;

use Doctrine\DBAL\Connection;
use ERP\Code\Domain\TravelCode;
use ERP\Code\Infrastructure\TravelCodeStorageInterface;

class PDO implements TravelCodeStorageInterface
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getDailyCodeNumber(): int
    {
        $today = date('Y-m-d 00:00:00');

        $result = $this->connection->executeQuery('
            SELECT
                count(*) as `count`
            FROM
                travel_code_storage
            WHERE
                created_at >= :today
        ', [
            'today' => $today
        ]);

        $data = $result->fetchAssociative();

        return $data['count'];
    }

    public function saveCode(TravelCode $travelCode): void
    {
        $this->connection->executeStatement(
            'INSERT INTO travel_code_storage (`ena`, `travel_id`, `user_id`) VALUES (:ena, :travelId, :userId)',
            [
                'ean' => $travelCode->getEan(),
                'travelId' => $travelCode->getTravelId(),
                'userId' => $travelCode->getCreatedBy()
            ]
        );
    }

    public function getByCode(string $ean): ?TravelCode
    {
        $result = $this->connection->executeQuery(
            'SELECT id, ean, travel_id, user_id, created_at FROM travel_code_storage WHERE ean = :ean',
            [
                'ean' => $ean
            ]
        );

        if ($data = $result->fetchAssociative() === false) {
            return null;
        }

        return self::crateTravelCode($data);
    }

    public function getByTravel(int $travelId): ?TravelCode
    {
        $result = $this->connection->executeQuery(
            'SELECT id, ean, travel_id, user_id, created_at FROM travel_code_storage WHERE travel_id = :travelId',
            [
                'travelId' => $travelId
            ]
        );

        if ($data = $result->fetchAssociative() === false) {
            return null;
        }

        return self::crateTravelCode($data);
    }

    private static function crateTravelCode(array $data): TravelCode
    {
        return new TravelCode(
            $data['id'],
            $data['ean'],
            $data['travel_id'],
            new \DateTimeImmutable($data['created_at']),
            $data['user_id']
        );
    }
}