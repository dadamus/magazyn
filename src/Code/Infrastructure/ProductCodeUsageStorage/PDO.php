<?php

namespace ERP\Code\Infrastructure\ProductCodeUsageStorage;

use DateTimeImmutable;
use Doctrine\DBAL\Connection;
use ERP\Code\Infrastructure\ProductCodeUsageStorageInterface;

class PDO implements ProductCodeUsageStorageInterface
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function find(string $productId): array
    {
        $queryResult = $this->connection->executeQuery('
            SELECT
            pcs.id,
            pcu.ean,
            pcu.production_device_id,
            pcu.travel_device_id,
            pcu.travel_id,
            pcu.production_scanned_at,
            pcu.travel_scanned_at,
            d.name as device_name
            FROM
            product_code_usage pcu
            LEFT JOIN product_code_storage pcs ON pcs.ean = pcu.ean
            LEFT JOIN device d ON d.id = pcu.production_device_id
            WHERE
            pcs.product_id = :productId
        ', [
            'productId' => $productId
        ]);

        return $queryResult->fetchAllAssociative();
    }

    public function storeProductionUsage(string $codeId, int $deviceId, DateTimeImmutable $scannedAt): void
    {
        $this->connection->executeStatement('
            INSERT INTO 
                product_code_usage (`ean`, `production_device_id`, `production_scanned_at`)
            VALUES 
                (:ean, :deviceId, :scannedAt)
        ', [
            'ean' => $codeId,
            'deviceId' => $deviceId,
            'scannedAt' => $scannedAt->format('Y-m-d H:i:s')
        ]);
    }

    public function storeTravelUsage(string $codeId, int $travelId, int $deviceId, DateTimeImmutable $scannedAt): void
    {
        $this->connection->executeStatement('
            INSERT INTO 
                product_code_usage (`ean`, `device_id`, `travel_device_id`, `travel_scanned_at`)
            VALUES 
                (:ean, :deviceId, :scannedAt)
        ', [
            'ean' => $codeId,
            'deviceId' => $deviceId,
            'travel_id' => $travelId,
            'scannedAt' => $scannedAt->format('Y-m-d H:i:s')
        ]);
    }

    public function hasProductionUsage(string $codeId): bool
    {
        return $this->codeIsRecorded($codeId);
    }

    public function hasTravelUsage(string $codeId): bool
    {
        return $this->codeIsRecorded($codeId);
    }

    private function codeIsRecorded(string $codeId): bool
    {
        $response = $this->connection->executeQuery('
            SELECT `ean` FROM product_code_usage WHERE ean = :ean
        ', [
            'ean' => $codeId
        ]);

        return count($response->fetchAllAssociative()) > 0;
    }
}