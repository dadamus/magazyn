<?php

namespace ERP\Code\Infrastructure;

use ERP\Code\Domain\TravelCode;

interface TravelCodeStorageInterface
{
    public function getDailyCodeNumber(): int;

    public function saveCode(TravelCode $travelCode): void;

    public function getByCode(string $ean): ?TravelCode;

    public function getByTravel(int $travelId): ?TravelCode;
}