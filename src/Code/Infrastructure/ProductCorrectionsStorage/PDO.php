<?php

namespace ERP\Code\Infrastructure\ProductCorrectionsStorage;

use Doctrine\DBAL\Connection;
use ERP\Code\Domain\CodeCorrectionType;
use ERP\Code\Infrastructure\ProductCorrectionsStorageInterface;

class PDO implements ProductCorrectionsStorageInterface
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function addProductCorrection(int $productId, int $value, int $userId): void
    {
        $this->connection->executeStatement('
            INSERT INTO `product_corrections` 
                (`product_id`, `value`, `user_id`)
            VALUES
                (:productId, :value, :userId)
        ', [
            'productId' => $productId,
            'value' => $value,
            'userId' => $userId
        ]);
    }

    public function find(int $productId): array
    {
        $query = $this->connection->executeQuery('
            SELECT
            pc.id,
            pc.value,
            pc.created_at as `date`
            FROM
            product_corrections pc
            WHERE
            pc.product_id = :productId
        ', [
            'productId' => $productId
        ]);

        return $query->fetchAllAssociative();
    }
}