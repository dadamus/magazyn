<?php

namespace ERP\Code\Infrastructure;

interface PrinterInterface
{
    public function printCode(string $name, string $ean): void;

    public function printGroup(string $printerGroupId): void;
}
