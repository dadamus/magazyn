<?php

declare(strict_types=1);

namespace ERP\Code\Infrastructure\PrinterGroup;

use Doctrine\DBAL\Connection;
use ERP\Code\Infrastructure\PrinterGroupRepositoryInterface;

class PDO implements PrinterGroupRepositoryInterface
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function save(string $uuid, array $codes)
    {
        $inserts = '';

        foreach ($codes as $codeValue) {
            $inserts .= "INSERT INTO print_group (id, code) VALUE ('$uuid', '$codeValue');";
        }

        $this->connection->executeStatement($inserts);
    }

    public function getCodeList(string $uuid): array
    {
        return $this->connection->fetchAllAssociative('
            SELECT
            pg.code,
            p.name
            FROM
            print_group pg
            LEFT JOIN product_code_storage pcs ON pcs.ean = pg.code
            LEFT JOIN product p ON p.id = pcs.product_id
            WHERE
            pg.id =  :uid
        ', [
            'uid' => $uuid
        ]);
    }
}
