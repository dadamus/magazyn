<?php

namespace ERP\Code\Infrastructure;

use DateTimeImmutable;

interface ProductCodeUsageStorageInterface
{
    public function find(string $productId): array;

    public function storeProductionUsage(string $codeId, int $deviceId, DateTimeImmutable $scannedAt): void;

    public function storeTravelUsage(string $codeId, int $travelId, int $deviceId, DateTimeImmutable $scannedAt): void;

    public function hasProductionUsage(string $codeId): bool;

    public function hasTravelUsage(string $codeId): bool;
}