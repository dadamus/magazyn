<?php

namespace ERP\Code\Infrastructure;

use ERP\Code\Domain\ProductCode;

interface ProductCodeRepositoryInterface
{
    public function get(int $productId): ?ProductCode;

    public function save(ProductCode $productCode): void;
}
