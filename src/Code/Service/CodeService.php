<?php

namespace ERP\Code\Service;

use ERP\Code\Domain\AbstractCode;
use ERP\Code\Infrastructure\ProductCodeStorageInterface;
use ERP\Code\Infrastructure\TravelCodeStorageInterface;

class CodeService
{
    private ProductCodeStorageInterface $productCodeStorage;
    private TravelCodeStorageInterface $travelCodeStorage;

    public function __construct(ProductCodeStorageInterface $productCodeStorage, TravelCodeStorageInterface $travelCodeStorage)
    {
        $this->productCodeStorage = $productCodeStorage;
        $this->travelCodeStorage = $travelCodeStorage;
    }

    public function getCode(string $ean): ?AbstractCode
    {
        $productCode = $this->productCodeStorage->getCode($ean);

        if ($productCode !== null) {
            return $productCode;
        }

        $travelCode = $this->travelCodeStorage->getByCode($ean);

        if ($travelCode !== null) {
            return $travelCode;
        }

        return null;
    }
}