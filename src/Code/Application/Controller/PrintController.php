<?php

namespace ERP\Code\Application\Controller;

use ERP\Code\Infrastructure\Barcode;
use ERP\SharedKernel\Application\Controller\CoreController;
use ERP\Code\Domain\Printer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class PrintController extends CoreController
{
    /**
     * @Route("/print/codes/{printGroupId}", methods={"GET"}, name="code_print")
     *
     * @param Printer $printer
     * @param string $printGroupId
     *
     * @return Response
     */
    public function printAction(Printer $printer, string $printGroupId): Response
    {
        $codes = $printer->getCodes($printGroupId);

        return $this->render('print.html.twig', [
            'codes' => $codes
        ]);
    }

    /**
     * @Route("/print/render/{code}", methods={"GET"}, name="render_code")
     *
     * @param string $code
     *
     * @return Response
     */
    public function renderImage(string $code): Response
    {
        $barcode = new Barcode($code, 13);

        ob_start();
        ob_clean();
        $barcode->display();
        $content = ob_get_contents();
        ob_end_clean();

        $response = new Response($content);
        $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $code . '.png');
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', 'image/png');

        return $response;
    }
}
