<?php

namespace ERP\Code\Application\Controller;

use ERP\Settings\Infrastructure\PositionRepositoryInterface;
use ERP\SharedKernel\Application\Controller\CoreController;
use ERP\SharedKernel\Domain\DataTableTranslator\CodesDataTranslatorAbstract;
use ERP\SharedKernel\Domain\DataTable\Data\Post;
use ERP\SharedKernel\Domain\DataTable\Engine;
use ERP\SharedKernel\Infrastructure\DataTableQuery\Codes;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ListController extends CoreController
{
    public function indexAction(PositionRepositoryInterface $positions): Response
    {
        return $this->render('ProductCode/codes_list.html.twig', [
            'positions' => $positions->getAll(true)
        ]);
    }

    public function filterAction(Engine $engine, Request $request): Response
    {
        $engine->setQueryBuilder(new Codes());
        $engine->setFilterData(new Post($request));
        $engine->init(new CodesDataTranslatorAbstract());

        return new JsonResponse($engine->search());
    }
}
