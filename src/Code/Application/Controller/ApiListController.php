<?php

declare(strict_types=1);

namespace ERP\Code\Application\Controller;

use ERP\SharedKernel\Application\Controller\CoreController;
use ERP\SharedKernel\Domain\DataTable\Data\ReactJson;
use ERP\SharedKernel\Domain\DataTable\Engine;
use ERP\SharedKernel\Domain\DataTableTranslator\CodesDataTranslatorAbstract;
use ERP\SharedKernel\Infrastructure\DataTableQuery\Codes;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiListController extends CoreController
{
    /**
     * @Route("/api/codes/list/filter")
     */
    public function filterAction(Engine $engine, Request $request): Response
    {
        $engine->setQueryBuilder(new Codes());
        $engine->setFilterData(new ReactJson($request));
        $engine->init(new CodesDataTranslatorAbstract());

        return new JsonResponse($engine->search());
    }
}
