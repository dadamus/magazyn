<?php

namespace ERP\Settings\Application\Controller;

use  ERP\SharedKernel\Application\Controller\CoreController;
use ERP\Settings\Infrastructure\CarRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CarsController extends CoreController
{
    /**
     * @Route("/settings/cars", methods={"GET"}, name="settings_cars")
     */
    public function indexAction(CarRepositoryInterface $cars): Response
    {
        return $this->render('Settings/cars.html.twig', [
            'cars' => $cars->getAll()
        ]);
    }

    /**
     * @Route("/settings/cars/list", methods={"GET"}, name="settings_cars_list")
     */
    public function listAction(CarRepositoryInterface $cars): Response
    {
        return $this->render('Settings/Cars/list.html.twig', [
            'cars' => $cars->getAll()
        ]);
    }

    /**
     * @Route("/settings/cars/modal/{carId}", methods={"GET"}, defaults={"carId" = 0}, name="settings_car_modal")

     */
    public function showCarModalAction(CarRepositoryInterface $cars, int $carId = 0): Response
    {
        $car = null;

        if ($carId > 0) {
            $car = $cars->get($carId);
        }

        return $this->render('Settings/Cars/modal_content.html.twig', [
            'car' => $car
        ]);
    }

    /**
     * @Route("/settings/cars", methods={"POST"}, name="settings_cars_add")
     */
    public function addCarAction(CarRepositoryInterface $cars, Request $request): Response
    {
        $plate = $request->request->get('plate');

        $cars->add($plate);

        return new JsonResponse('Car added!');
    }

    /**
     * @Route("/settings/cars/{carId}", methods={"PUT"}, name="settings_cars_edit")
     */
    public function editCarAction(CarRepositoryInterface $cars, int $carId, Request $request): Response
    {
        $plate = $request->request->get('plate');

        $cars->edit($carId, $plate);

        return new JsonResponse('Car added!');
    }

    /**
     * @Route("/settings/cars/{carId}/visible", methods={"PUT"}, name="settings_cars_visible")
     */
    public function setCarsVisibilityAction(CarRepositoryInterface $cars, int $carId, Request $request): Response
    {
        $visible = $request->request->getBoolean('visible');

        $cars->setVisibility($carId, $visible);

        return new JsonResponse('Position changed!');
    }
}
