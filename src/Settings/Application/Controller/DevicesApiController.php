<?php

declare(strict_types=1);

namespace ERP\Settings\Application\Controller;

use ERP\Settings\Domain\Device\Device;
use ERP\Settings\Infrastructure\DeviceRepositoryInterface;
use ERP\SharedKernel\Application\Controller\CoreController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DevicesApiController extends CoreController
{
    /**
     * @Route("/api/settings/devices", methods={"GET"})
     */
    public function getDevicesList(DeviceRepositoryInterface $devices): JsonResponse
    {
        return new JsonResponse($devices->getAll());
    }

    /**
     * @Route("/api/settings/devices/types", methods={"GET"})
     */
    public function getDevicesTypes(DeviceRepositoryInterface $devices): JsonResponse
    {
        return new JsonResponse($devices->getDeviceTypes());
    }

    /**
     * @Route("/api/settings/device", methods={"POST"})
     */
    public function addDevice(Request $request, DeviceRepositoryInterface $devices): JsonResponse
    {
        $requestData = json_decode($request->getContent(false), true);

        $device = new Device([
            Device::NAME_FIELD => $requestData['name'],
            Device::DEVICE_TYPE_ID_FIELD => $requestData['type'],
            Device::ACTIVE_FIELD => $requestData['active'],
            Device::POSITION_ID_FIELD => $requestData['position_id'],
            Device::HASH_FIELD => $requestData['hash']
        ]);

        $devices->add($device);

        return new JsonResponse('Device added!');
    }

    /**
     * @Route("/api/settings/device", methods={"PUT"})
     */
    public function editDevice(Request $request, DeviceRepositoryInterface $devices): JsonResponse
    {
        $requestData = json_decode($request->getContent(false), true);

        $device = new Device([
            Device::ID_FIELD => (int)$requestData['id'],
            Device::NAME_FIELD => $requestData['name'],
            Device::DEVICE_TYPE_ID_FIELD => $requestData['type'],
            Device::ACTIVE_FIELD => (int)$requestData['active'],
            Device::POSITION_ID_FIELD => (int)$requestData['position_id'],
            Device::HASH_FIELD => $requestData['hash']
        ]);

        $devices->edit($device->getId(), $device);

        return new JsonResponse('Device saved!');
    }
}
