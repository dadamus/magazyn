<?php

namespace ERP\Settings\Application\Controller;

use ERP\SharedKernel\Application\Controller\CoreController;
use ERP\Settings\Domain\Device\Device;
use ERP\Settings\Infrastructure\DeviceRepositoryInterface;
use ERP\Settings\Infrastructure\PositionRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DevicesController extends CoreController
{
    /**
     * @Route("/old/settings/devices", methods={"GET"}, name="settings_devices")
     */
    public function indexAction(DeviceRepositoryInterface $devices): Response
    {
        return $this->render('Settings/devices.html.twig', [
            'devices' => $devices->getAll()
        ]);
    }

    /**
     * @Route("/settings/devices/list", methods={"GET"}, name="settings_devices_list")
     */
    public function listAction(DeviceRepositoryInterface $devices): Response
    {
        return $this->render('Settings/Devices/list.html.twig', [
            'devices' => $devices->getAll()
        ]);
    }

    /**
     * @Route("/settings/devices/modal/{deviceId}", methods={"GET"}, defaults={"deviceId" = 0}, name="settings_devices_modal")
     */
    public function showDeviceModalAction(
        PositionRepositoryInterface $positions,
        DeviceRepositoryInterface $devices,
        int $deviceId = 0
    ): Response {
        $device = null;

        if ($deviceId > 0) {
            $device = $devices->get($deviceId);
        }

        return $this->render('Settings/Devices/modal_content.html.twig', [
            'device' => $device,
            'deviceTypes' => $devices->getDeviceTypes(),
            'positions' => $positions->getAll(true)
        ]);
    }

    /**
     * @Route("/settings/devices", methods={"POST"}, name="settings_devices_add")
     */
    public function addDeviceAction(DeviceRepositoryInterface $devices, Request $request): Response
    {
        $device = new Device([
            Device::NAME_FIELD => $request->request->get('name'),
            Device::DEVICE_TYPE_ID_FIELD => $request->request->getInt('type'),
            Device::ACTIVE_FIELD => $request->request->getBoolean('active'),
            Device::POSITION_ID_FIELD => $request->request->getInt('position'),
            Device::HASH_FIELD => $request->request->get('hash')
        ]);

        $devices->add($device);

        return new JsonResponse('Device added!');
    }

    /**
     * @Route("/settings/devices/{deviceId}", methods={"PUT"}, name="settings_devices_edit")
     */
    public function editDeviceAction(DeviceRepositoryInterface $devices, int $deviceId, Request $request): Response
    {
        $device = new Device([
            Device::NAME_FIELD => $request->request->get('name'),
            Device::DEVICE_TYPE_ID_FIELD => $request->request->getInt('type'),
            Device::ACTIVE_FIELD => $request->request->getBoolean('active'),
            Device::POSITION_ID_FIELD => $request->request->getInt('position'),
            Device::HASH_FIELD => $request->request->get('hash')
        ]);

        $devices->edit($deviceId, $device);

        return new JsonResponse('Device edited!');
    }
}
