<?php

namespace ERP\Settings\Application\Controller;

use ERP\SharedKernel\Application\Controller\CoreController;
use ERP\Settings\Infrastructure\PositionRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PositionsController extends CoreController
{
    /**
     * @Route("/old/settings/positions", methods={"GET"}, name="settings_positions")
     */
    public function indexAction(PositionRepositoryInterface $positions): Response
    {
        return $this->render('Settings/positions.html.twig', [
            'positions' => $positions->getAll()
        ]);
    }

    /**
     * @Route("/settings/positions/list", methods={"GET"}, name="settings_positions_list")
     */
    public function listAction(PositionRepositoryInterface $positions): Response
    {
        return $this->render('Settings/Positions/list.html.twig', [
            'positions' => $positions->getAll(),
        ]);
    }

    /**
     * @Route("/settings/positions/modal/{positionId}", defaults={"positionId": 0}, methods={"GET"}, name="settings_positions_modal")
     */
    public function showPositionModalAction(PositionRepositoryInterface $positions, int $positionId = 0): Response
    {
        $positionData = [];

        if ($positionId > 0) {
            $positionData = $positions->get($positionId);
        }

        return $this->render('Settings/Positions/modal_content.html.twig', [
            'position' => $positionData
        ]);
    }

    /**
     * @Route("/settings/positions", methods={"POST"}, name="settings_positions_add")
     */
    public function addPositionAction(PositionRepositoryInterface $positions, Request $request): Response
    {
        $description = '';

        if ($request->request->has('description')) {
            $description = $request->request->get('description');
        }

        $positions->add($request->request->get('name'), $description);

        return new JsonResponse('Position added!');
    }

    /**
     * @Route("/settings/positions/{positionId}", methods={"PUT"}, name="settings_positions_edit")
     */
    public function editPositionAction(
        PositionRepositoryInterface $positions,
        int $positionId,
        Request $request
    ): Response {
        $name = $request->request->get('name');
        $description = $request->request->get('description');

        $positions->edit($positionId, $name, $description);

        return new JsonResponse('Position edited!');
    }

    /**
     * @Route("/settings/positions/{positionId}/visible", methods={"PUT"}, name="settings_positions_visible")
     */
    public function setPositionVisibilityAction(
        PositionRepositoryInterface $positions,
        int $positionId,
        Request $request
    ): Response {
        $visible = $request->request->getBoolean('visible');

        $positions->setVisibility($positionId, $visible);

        return new JsonResponse('Position changed!');
    }
}
