<?php

declare(strict_types=1);

namespace ERP\Settings\Application\Controller;

use ERP\Settings\Infrastructure\CarRepositoryInterface;
use ERP\SharedKernel\Application\Controller\CoreController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CarsApiController extends CoreController
{
    /**
     * @Route("/api/settings/cars", methods={"GET"})
     */
    public function indexAction(CarRepositoryInterface $cars): JsonResponse
    {
        return new JsonResponse($cars->getAll());
    }

    /**
     * @Route("/api/settings/car/add", methods={"POST"})
     */
    public function addCarAction(CarRepositoryInterface $cars, Request $request): JsonResponse
    {
        $requestData = json_decode($request->getContent(false), true);

        $cars->add($requestData['plate']);

        return new JsonResponse('Car added!');
    }
}
