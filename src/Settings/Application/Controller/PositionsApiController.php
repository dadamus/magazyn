<?php

declare(strict_types=1);

namespace ERP\Settings\Application\Controller;

use ERP\Settings\Infrastructure\PositionRepositoryInterface;
use ERP\SharedKernel\Application\Controller\CoreController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PositionsApiController extends CoreController
{
    /**
     * @Route("/api/settings/positions", methods={"GET"})
     */
    public function getPositionsList(PositionRepositoryInterface $positions): JsonResponse
    {
        return new JsonResponse($positions->getAll());
    }

    /**
     * @Route("/api/settings/position", methods={"POST"})
     */
    public function addPosition(PositionRepositoryInterface $positions, Request $request): JsonResponse
    {
        $requestData = json_decode($request->getContent(false), true);

        $name = $requestData['name'];
        $description = $requestData['description'] ?? '';

        $positions->add($name, $description);

        return new JsonResponse('Position added!');
    }

    /**
     * @Route("/api/settings/position", methods={"PUT"})
     */
    public function updatePosition(PositionRepositoryInterface $positions, Request $request): JsonResponse
    {
        $requestData = json_decode($request->getContent(false), true);

        $id = (int)$requestData['id'];
        $name = $requestData['name'];
        $description = $requestData['description'] ?? '';

        $positions->edit($id, $name, $description);

        return new JsonResponse('Position added!');
    }
}
