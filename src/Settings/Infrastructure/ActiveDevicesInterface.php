<?php


namespace ERP\Settings\Infrastructure;


interface ActiveDevicesInterface
{
    public function getAll(): array;
}
