<?php

namespace ERP\Settings\Infrastructure\CarRepository;

use Doctrine\DBAL\Connection;
use ERP\Settings\Infrastructure\CarRepositoryInterface;

class PDO implements CarRepositoryInterface
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getAll(bool $onlyVisible = false): array
    {
        $filter = '';

        if ($onlyVisible) {
            $filter = 'WHERE visible = true';
        }

        return $this->connection->fetchAllAssociative('
            SELECT
            *
            FROM
            car
            ' . $filter . '
            ORDER BY id
        ');
    }

    public function get(int $carId): array
    {
        $data = $this->connection->fetchAssociative('
            SELECT
            *
            FROM
            car
            WHERE
            id = :id
        ', [
            'id' => $carId
        ]);

        if (!$data) {
            throw new CarNotFound($carId);
        }

        return $data;
    }

    public function add(string $plate): int
    {
        $this->connection->executeStatement('
            INSERT INTO
            car (plate) VALUES (:plate)
        ', [
            'plate' => $plate
        ]);

        return (int)$this->connection->lastInsertId();
    }

    public function edit(int $carId, string $plate): void
    {
        $this->connection->executeStatement('
            UPDATE car SET plate = :plate WHERE id = :id
        ', [
            'id' => $carId,
            'plate' => $plate
        ]);
    }

    public function setVisibility(int $carId, bool $visible): void
    {
        $this->connection->executeStatement('
            UPDATE car SET visible = :visible WHERE id = :id
        ', [
            'id' => $carId,
            'visible' => (int)$visible
        ]);
    }
}
