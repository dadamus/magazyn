<?php

namespace ERP\Settings\Infrastructure;

interface CarRepositoryInterface
{
    public function getAll(bool $onlyVisible = false): array;

    public function get(int $carId): array;

    public function add(string $plate): int;

    public function edit(int $carId, string $plate): void;

    public function setVisibility(int $carId, bool $visible): void;
}
