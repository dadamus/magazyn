<?php

namespace ERP\Settings\Infrastructure\ActiveDevice;

use ERP\Settings\Infrastructure\ActiveDevicesInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class Rabbit implements ActiveDevicesInterface
{
    private Client $client;

    public function __construct()
    {
        $host = getenv('RABBIT_HOST');
        $user = getenv('RABBIT_USER');
        $password = getenv('RABBIT_PASSWORD');

        $url = '192.168.100.164:15672/api/';

        $this->client = new Client([
            "base_uri" => $url,
            'auth' => [
                'root',
                'root'
            ]
        ]);
    }

    public function getAll(): array
    {
        $bindings = $this->getBindings(getenv('RABBIT_VHOST'));

        $response = [];

        foreach ($bindings as $device) {
            if ($device['source'] === 'device_finder') {
                $response[] = $device['routing_key'];
            }
        }

        return $response;
    }

    private function getConsumers(string $vhost): array
    {
        try {
            $request = $this->client->request("GET", "consumers/$vhost");
            $response = \GuzzleHttp\json_decode($request->getBody()->getContents(), true);
        } catch (RequestException $exception) {
            return [];
        }

        return $response;
    }

    private function getQueues(string $vhost): array
    {
        $request = $this->client->request("GET", "/api/queues/$vhost");
        $response = \GuzzleHttp\json_decode($request->getBody()->getContents(), true);

        return $response;
    }

    private function getExchanges(string $vhost): array
    {
        $request = $this->client->request("GET", "/api/exchanges/$vhost");
        $response = \GuzzleHttp\json_decode($request->getBody()->getContents(), true);

        return $response;
    }

    private function getBindings(string $vhost): array
    {
        try {
            $request = $this->client->request("GET", "/api/bindings/$vhost");

            return \GuzzleHttp\json_decode($request->getBody()->getContents(), true);
        } catch (ClientException $clientException) {
            if ($clientException->getCode() === 404) {
                return [];
            } else {
                throw $clientException;
            }
        }
    }
}
