<?php

namespace ERP\Settings\Infrastructure\PositionRepository;

use Doctrine\DBAL\Connection;
use ERP\Infrastructure\Exception\PositionNotFound;
use ERP\Settings\Infrastructure\PositionRepositoryInterface;

class PDO implements PositionRepositoryInterface
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getAll(bool $onlyVisible = false): array
    {
        $onlyVisibleFilter = '';

        if ($onlyVisible) {
            $onlyVisibleFilter = 'WHERE visible = 1';
        }

        return $this->connection->fetchAllAssociative('
            SELECT
            *
            FROM
            position
            ' . $onlyVisibleFilter . '
            ORDER BY id
        ');
    }

    public function get(int $positionId): array
    {
        $data = $this->connection->fetchAssociative('
            SELECT
            *
            FROM
            position
            WHERE
            id = :id
        ', [
            'id' => $positionId
        ]);

        if (!$data) {
            throw new PositionNotFound($positionId);
        }

        return $data;
    }

    public function add(string $name, string $description = ''): int
    {
        $this->connection->executeStatement('
            INSERT INTO position
            (name, description)
            VALUES (:name, :desciption)
        ', [
            'name' => $name,
            'desciption' => $description
        ]);

        return $this->connection->lastInsertId();
    }

    public function edit(int $positionId, string $name, string $description = ''): void
    {
        $this->connection->executeStatement('
            UPDATE position
            SET name = :name, description = :description
            WHERE
            id = :id
        ', [
            'id' => $positionId,
            'name' => $name,
            'description' => $description
        ]);
    }

    public function setVisibility(int $positionId, bool $visible): void
    {
        $this->connection->executeStatement('
            UPDATE position
            SET visible = :visible
            WHERE
            id = :id
        ', [
            'visible' => (int)$visible,
            'id' => $positionId
        ]);
    }
}
