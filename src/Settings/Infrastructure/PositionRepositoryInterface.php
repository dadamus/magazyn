<?php

namespace ERP\Settings\Infrastructure;

interface PositionRepositoryInterface
{
    public function getAll(bool $onlyVisible = false): array;

    public function get(int $positionId): array;

    public function add(string $name, string $description = ''): int;

    public function edit(int $positionId, string $name, string $description = ''): void;

    public function setVisibility(int $positionId, bool $visible): void;
}
