<?php

namespace ERP\Settings\Infrastructure;

use ERP\Settings\Domain\Device\Device;

interface DeviceRepositoryInterface
{
    public const DEVICE_ID = 'id';
    public const DEVICE_NAME = 'name';

    public function getAll(): array;

    public function get(int $deviceId): array;

    public function getDeviceTypes(): array;

    public function add(Device $device): int;

    public function edit(int $deviceId, Device $device): void;

    public function getByHash(string $hash): Device;

    public function addTravel(int $deviceId, int $travelId): void;

    public function clearTravel(int $deviceId): void;

    public function setLastSynchronizationDate(int $deviceId, string $ip): void;
}
