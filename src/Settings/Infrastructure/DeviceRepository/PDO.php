<?php


namespace ERP\Settings\Infrastructure\DeviceRepository;

use Doctrine\DBAL\Connection;
use ERP\Infrastructure\Exception\DeviceNotFound;
use ERP\Settings\Domain\Device\Device;
use ERP\Settings\Infrastructure\DeviceRepositoryInterface;

class PDO implements DeviceRepositoryInterface
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getAll(): array
    {
        return $this->connection->fetchAllAssociative('
            SELECT
            d.*,
            p.name as position_name,
            dt.id as type,
            dt.name as device_type_name,
            t.name as travel_name
            FROM
            device d
            LEFT JOIN position p ON p.id = d.position_id
            LEFT JOIN device_type dt ON dt.id = d.type_id
            LEFT JOIN travel t ON t.id = d.last_travel_id
            ORDER BY d.id
        ');
    }

    public function get(int $deviceId): array
    {
        $data = $this->connection->fetchAssociative('
            SELECT
            d.*,
            p.name as position_name,
            dt.name as device_type_name
            FROM
            device d
            LEFT JOIN position p ON p.id = d.position_id
            LEFT JOIN device_type dt ON dt.id = d.type_id
            WHERE
            d.id = :id
        ', [
            'id' => $deviceId
        ]);

        if (!$data) {
            throw new DeviceNotFound($deviceId);
        }

        return $data;
    }

    public function getDeviceTypes(): array
    {
        return $this->connection->fetchAllAssociative('
            SELECT
            *
            FROM
            device_type
        ');
    }

    public function add(Device $device): int
    {
        $this->connection->executeStatement('
            INSERT INTO
            device
            (name, type_id, active, position_id, hash)
            VALUES
            (:name, :type, :active, :positionId, :hash)
        ', [
            'name' => $device->getName(),
            'type' => $device->getType(),
            'active' => (int)$device->isActive(),
            'positionId' => $device->getPositionId(),
            'hash' => $device->getHash()
        ]);

        return $this->connection->lastInsertId();
    }

    public function edit(int $deviceId, Device $device): void
    {
        $this->connection->executeStatement('
            UPDATE
            device
            SET
            name = :name, type_id = :type, active = :active, position_id = :positionId, hash = :hash
            WHERE
            id = :id
        ', [
            'id' => $deviceId,
            'name' => $device->getName(),
            'type' => $device->getType(),
            'active' => (int)$device->isActive(),
            'positionId' => $device->getPositionId(),
            'hash' => $device->getHash()
        ]);
    }

    public function addTravel(int $deviceId, int $travelId): void
    {
        $this->connection->executeStatement('
            UPDATE device SET last_travel_id = :travelId, last_sync = :lastSync WHERE id = :deviceId
        ', [
            'travelId' => $travelId,
            'lastSync' => date('Y-m-d H:i:s'),
            'deviceId' => $deviceId
        ]);
    }

    public function clearTravel(int $deviceId): void
    {
        $this->connection->executeStatement('
            UPDATE device SET last_travel_id = null, last_sync = :lastSync WHERE id = :deviceId
        ', [
            'lastSync' => date('Y-m-d H:i:s'),
            'deviceId' => $deviceId
        ]);
    }

    public function getByHash(string $hash): Device
    {
        $response = $this->connection->fetchAssociative('
            SELECT
            *
            FROM
            device
            WHERE
            hash = :hash
        ', [
            'hash' => $hash
        ]);

        if (!$response) {
            throw new DeviceNotFound(0);
        }

        return new Device($response);
    }

    public function setLastSynchronizationDate(int $deviceId, string $ip): void
    {
        $this->connection->executeStatement('
            UPDATE device SET last_sync = CURRENT_TIMESTAMP, last_ip = :ip WHERE id = :deviceId
        ', [
            'ip' => $ip,
            'deviceId' => $deviceId
        ]);
    }
}
