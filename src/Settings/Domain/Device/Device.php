<?php

declare(strict_types=1);

namespace ERP\Settings\Domain\Device;

use ERP\SharedKernel\Domain\DataObject;

class Device extends DataObject
{
    public const ID_FIELD = 'id';
    public const NAME_FIELD = 'name';
    public const DEVICE_TYPE_ID_FIELD = 'type_id';
    public const ACTIVE_FIELD = 'active';
    public const HASH_FIELD = 'hash';
    public const POSITION_ID_FIELD = 'position_id';
    public const LAST_TRAVEL_ID_FIELD = 'last_travel_id';

    public function getId(): int
    {
        return (int)$this->getData(self::ID_FIELD);
    }

    public function getName(): string
    {
        return $this->getData(self::NAME_FIELD);
    }

    public function getType(): DeviceType
    {
        $deviceTypeId = (int)$this->getData(self::DEVICE_TYPE_ID_FIELD);

        return new DeviceType($deviceTypeId);
    }

    public function isActive(): bool
    {
        return (bool)$this->getData(self::ACTIVE_FIELD);
    }

    public function getHash(): string
    {
        return $this->getData(self::HASH_FIELD);
    }

    public function getLastTravelId(): ?int
    {
        if (!$this->getData(self::LAST_TRAVEL_ID_FIELD)) {
            return null;
        }

        return (int)$this->getData(self::LAST_TRAVEL_ID_FIELD);
    }

    public function getPositionId(): int
    {
        return (int)$this->getData(self::POSITION_ID_FIELD);
    }
}
