<?php

declare(strict_types=1);

namespace ERP\Settings\Domain\Device;

use ERP\SharedKernel\Domain\DataObject;

class DevicePosition extends DataObject
{
    public const ID_FIELD = 'id';
    public const NAME_FIELD = 'name';

    public function getId(): int
    {
        return (int)$this->getData(self::ID_FIELD);
    }

    public function getName(): string
    {
        return $this->getData(self::NAME_FIELD);
    }
}
