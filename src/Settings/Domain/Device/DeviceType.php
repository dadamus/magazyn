<?php

declare(strict_types=1);

namespace ERP\Settings\Domain\Device;

use MyCLabs\Enum\Enum;

/**
 * @method static DeviceType PRODUCTION()
 * @method static DeviceType TRAVEL()
 */
class DeviceType extends Enum
{
    public const PRODUCTION = 1;
    public const TRAVEL = 2;
}
