<?php

namespace ERP\SharedKernel\Domain\DataTable;

interface AbstractFilterAliasTranslator
{
    public function getTranslator(): array;
}
