<?php

namespace ERP\SharedKernel\Domain\DataTable\Data;

use ERP\SharedKernel\Domain\DataTable\AbstractFilterData;
use Symfony\Component\HttpFoundation\Request;

class Post extends AbstractFilterData
{
    public function __construct(Request $request)
    {
        $this->columns = (array)$request->request->get('columns');
        $this->draw = $request->request->getInt('draw');
        $this->start = $request->request->getInt('start');
        $this->length = $request->request->getInt('length');
        $this->order = (array)$request->request->get('order');
    }
}
