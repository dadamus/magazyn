<?php

namespace ERP\SharedKernel\Domain\DataTable\Data;

use ERP\SharedKernel\Domain\DataTable\AbstractFilterData;

class Memory extends AbstractFilterData
{
    public function __construct(array $columns, array $order, int $start, int $length, int $draw)
    {
        $this->columns = $columns;
        $this->draw = $draw;
        $this->order = $order;
        $this->start = $start;
        $this->length = $length;
    }
}
