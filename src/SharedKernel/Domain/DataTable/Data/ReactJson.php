<?php

declare(strict_types=1);

namespace ERP\SharedKernel\Domain\DataTable\Data;

use ERP\SharedKernel\Domain\DataTable\AbstractFilterData;
use Symfony\Component\HttpFoundation\Request;

class ReactJson extends AbstractFilterData
{
    public function __construct(Request $request)
    {
        $requestData = json_decode($request->getContent(false), true);

        $this->columns = $requestData['columns'];
        $this->draw = $requestData['filters']['length'];
        $this->start = ($requestData['filters']['page'] - 1) * $requestData['filters']['length'];
        $this->length = $requestData['filters']['length'];
        $this->order = [];

        if (isset($requestData['filters']['order'])) {
            $this->order = $requestData['filters']['order'];
        }
    }
}
