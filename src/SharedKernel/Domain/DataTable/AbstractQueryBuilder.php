<?php

namespace ERP\SharedKernel\Domain\DataTable;

abstract class AbstractQueryBuilder
{
    private string $select;

    private string $from;

    private array $joins = [];

    private int $limit;

    private int $offset;

    private string $orderBy;

    private Filters $filters;

    private array $where = [];

    public function addSelect(AbstractFilterAliasTranslator $translator): self
    {
        $selectData = '';

        foreach ($translator->getTranslator() as $key => $value) {
            if ($selectData !== '') {
                $selectData .= ', ';
            }

            $selectData .= $value . ' as ' . $key;
        }

        $this->select = $selectData;

        return $this;
    }

    public function setSelect(string $select): self
    {
        $this->select = $select;

        return $this;
    }

    public function addFrom(string $from): self
    {
        $this->from = $from;

        return $this;
    }

    public function addJoin(string $joinValue): self
    {
        $this->joins[] = $joinValue;

        return $this;
    }

    public function addWhere(string $where): self
    {
        $this->where[] = $where;

        return $this;
    }

    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }

    public function setOffset(int $offset): void
    {
        $this->offset = $offset;
    }

    public abstract function build(AbstractFilterAliasTranslator $translator): void;

    public function setOrder(string $orderBy): void
    {
        $this->orderBy = $orderBy;
    }

    public function getSelect(): string
    {
        return $this->select;
    }

    public function getFrom(): string
    {
        return $this->from;
    }

    public function getJoins(): array
    {
        return $this->joins;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getOrderBy(): string
    {
        return $this->orderBy;
    }

    public function getFilters(): Filters
    {
        return $this->filters;
    }

    public function setFilters(Filters $filters): void
    {
        $this->filters = $filters;
    }

    public function getWhere(): array
    {
        return $this->where;
    }
}
