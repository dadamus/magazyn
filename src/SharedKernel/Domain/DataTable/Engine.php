<?php


namespace ERP\SharedKernel\Domain\DataTable;


use ERP\SharedKernel\Domain\DataTable\Data\Memory;
use ERP\SharedKernel\Infrastructure\DataTableRepositoriesInterface;

class Engine
{
    private AbstractFilterData $filterData;

    /**
     * @var AbstractQueryBuilder[]
     */
    private array $queryBuilders = [];

    private DataTableRepositoriesInterface $repositories;

    private AbstractFilterAliasTranslator $translator;

    private array $lastSearch = [];

    public function __construct(DataTableRepositoriesInterface $repositories)
    {
        $this->repositories = $repositories;
    }

    public function setFilterData(AbstractFilterData $filterData): void
    {
        $this->filterData = $filterData;
    }

    public function setQueryBuilder(AbstractQueryBuilder $queryBuilder): void
    {
        $this->queryBuilders[] = $queryBuilder;
    }

    public function init(AbstractFilterAliasTranslator ...$translators): void
    {
        $translator = reset($translators);

        foreach ($this->queryBuilders as $queryBuilder) {
            $queryBuilder->build($translator);

            $this->translator = $translator;
            $queryBuilder->setFilters(new Filters($this->filterData, $translator));
            $queryBuilder->setLimit($this->filterData->getLength());
            $queryBuilder->setOffset($this->filterData->getStart());
            $queryBuilder->setOrder($this->generateOrder($translator));

            $translator = next($translators);
        }
    }

    private function generateOrder(AbstractFilterAliasTranslator $translator): string
    {
        $ordersData = $this->filterData->getOrder();
        $columns = $this->filterData->getColumns();
        $translatorData = $translator->getTranslator();

        $orders = '';

        foreach ($ordersData as $orderData) {
            if ($orders !== '') {
                $orders .= ', ';
            }
            $orders .= 'ORDER BY ' . $translatorData[$orderData['column']] . ' ' . $orderData['dir'];
        }

        return $orders;
    }

    private function countTotal(): int
    {
        $total = 0;

        foreach ($this->queryBuilders as $queryBuilder) {
            $query = clone $queryBuilder;
            $query->setSelect('count(*) as total');

            $columns = $this->filterData->getColumns();
            foreach ($columns as $key => $column) {
                $columns[$key]['search']['value'] = null;
            }

            $filterData = new Memory(
                $columns,
                $this->filterData->getOrder(),
                0,
                0,
                $this->filterData->getDraw()
            );

            $query->setFilters(new Filters($filterData, $this->translator));
            $query->setLimit(0);
            $query->setOffset(0);
            $data = $this->repositories->getData($query);

            $total += reset($data)['total'];
        }

        return $total;
    }

    private function countAllFiltered(): int
    {
        $total = 0;

        foreach ($this->queryBuilders as $queryBuilder) {
            $query = clone $queryBuilder;
            $query->setSelect('count(*) as total');

            $filterData = new Memory(
                $this->filterData->getColumns(),
                $this->filterData->getOrder(),
                0,
                0,
                $this->filterData->getDraw()
            );

            $query->setLimit(0);
            $query->setOffset(0);
            $query->setFilters(new Filters($filterData, $this->translator));
            $data = $this->repositories->getData($query);

            $total += reset($data)['total'];
        }

        return $total;
    }

    private function addAdditionalSorting(array $data): array
    {
        foreach ($this->filterData->getOrder() as $order) {
            $column = $order['column'];
            $direction = $order['dir'];

            if ($column === 'date') {
                if ($direction === 'asc') {
                    usort($data,
                        fn(array $a, array $b) => strtotime($a['date']) < strtotime($b['date']) ? 1 : -1);
                } else {
                    usort($data,
                        fn(array $a, array $b) => strtotime($a['date']) < strtotime($b['date']) ? -1 : 1);
                }
            }
        }

        return $data;
    }

    public function search(): array
    {
        $data = [];

        foreach ($this->queryBuilders as $queryBuilder) {
            $result = $this->repositories->getData($queryBuilder);

            $data = array_merge($data, $result);
        }

        if (count($this->queryBuilders) > 1) {
            $data = $this->addAdditionalSorting($data);
        }

        return [
            'draw' => $this->filterData->getDraw(),
            'data' => $data,
            'recordsTotal' => $this->countTotal(),
            'recordsFiltered' => $this->countAllFiltered()
        ];
    }
}
