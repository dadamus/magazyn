<?php

namespace ERP\SharedKernel\Domain\DataTable;

abstract class AbstractFilterData
{
    protected array $columns;

    protected int $draw;

    protected int $start;

    protected int $length;

    protected array $order;

    public function addColumnFilter(string $column, string $type, $value): void
    {
        $this->columns[$column] = [
            'data' => $column,
            'search' => [
                'value' => $value,
                'type' => $type
            ]
        ];
    }

    public function getColumns(): array
    {
        return $this->columns;
    }

    public function getDraw(): int
    {
        return $this->draw;
    }

    public function getStart(): int
    {
        return $this->start;
    }

    public function getLength(): int
    {
        return $this->length;
    }

    public function getOrder(): array
    {
        return $this->order;
    }
}
