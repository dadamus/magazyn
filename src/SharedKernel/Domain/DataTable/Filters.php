<?php

namespace ERP\SharedKernel\Domain\DataTable;

class Filters
{
    private array $filters = [];

    public function __construct(AbstractFilterData $filterData, AbstractFilterAliasTranslator $translator)
    {
        foreach ($filterData->getColumns() as $column) {
            $searchValue = $column['search']['value'];
            $filterType = '';

            if (isset($column['search']['type'])) {
                $filterType = $column['search']['type'];
            }

            if ($searchValue === null || $searchValue === '') {
                continue;
            }

            $translatorData = $translator->getTranslator();

            $this->filters[] = [
                'name' => $translatorData[$column['data']],
                'value' => $searchValue,
                'type' => $filterType
            ];
        }
    }

    public function getFilters(): array
    {
        return $this->filters;
    }
}
