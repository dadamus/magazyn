<?php

declare(strict_types=1);

namespace ERP\SharedKernel\Domain;

abstract class DataObject
{
    private array $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getData(?string $name = null)
    {
        if ($name === null) {
            return $this->data;
        }

        return $this->data[$name] ?? null;
    }

    public function setData(string $name, $value): void
    {
        $this->data[$name] = $value;
    }
}
