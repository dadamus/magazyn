<?php

declare(strict_types=1);

namespace ERP\SharedKernel\Domain\Security;

use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationFailureHandler;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class JWTAuthentication extends AbstractAuthenticator
{
    private AuthenticationSuccessHandler $successHandler;

    private AuthenticationFailureHandler $failureHandler;

    private JWTUserProvider $userProvider;

    public function __construct(
        AuthenticationSuccessHandler $successHandler,
        AuthenticationFailureHandler $failureHandler,
        JWTUserProvider $userProvider
    ) {
        $this->successHandler = $successHandler;
        $this->failureHandler = $failureHandler;
        $this->userProvider = $userProvider;
    }

    public function supports(Request $request): ?bool
    {
        return true;
    }

    public function authenticate(Request $request): Passport
    {
        $credentials = $this->getCredentials($request);

        $user = $this->getUser($credentials['username'], $this->userProvider);

        if (!$user) {
            throw new AuthenticationCredentialsNotFoundException();
        }

        if (!$this->checkCredentials($credentials['password'], $user)) {
            throw new AuthenticationCredentialsNotFoundException();
        }

        $passport = new SelfValidatingPassport(new UserBadge($user->getUserIdentifier()));
        $passport->setAttribute('payload', $credentials);

        return $passport;
    }

    public function getCredentials(Request $request): array
    {
        $data = json_decode($request->getContent(), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \HttpException(400, 'Invalid json');
        }
        if (!isset($data['username'])) {
            throw new \HttpException(400, 'Username is required');
        }

        return $data;
    }

    public function getUser($username, UserProviderInterface $userProvider): ?UserInterface
    {
        if (null === $username) return null;

        return $userProvider->loadUserByUsername($username);
    }

    public function checkCredentials($password, UserInterface $user): bool
    {
        if (md5($password) === $user->getPassword()) {
            return true;
        }

        return false;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return $this->failureHandler->onAuthenticationFailure($request, $exception);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): ?Response
    {
        return $this->successHandler->onAuthenticationSuccess($request, $token);
    }

    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        return new Response('Auth header required', 401);
    }
}
