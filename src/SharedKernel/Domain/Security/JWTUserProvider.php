<?php

declare(strict_types=1);

namespace ERP\SharedKernel\Domain\Security;

use Doctrine\DBAL\Connection;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class JWTUserProvider implements UserProviderInterface
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function loadUserByUsername(string $username): UserInterface
    {
        $userData = $this->connection->fetchAssociative('
            SELECT
            login,
            password,
            id
            FROM
            `user`
            WHERE
            login = :login
        ', [
            'login' => $username
        ]);

        if (!$userData) {
            throw new UsernameNotFoundException();
        }

        $userGroupsData = $this->connection->fetchAllAssociative('
            SELECT
            g.name
            FROM
            user_group ug
            LEFT JOIN `group` g ON g.id = ug.group_id
            WHERE
            ug.user_id = :userId
        ', [
            'userId' => $userData['id']
        ]);

        $userGroups = [];

        foreach ($userGroupsData as $userGroup) {
            $userGroups[] = 'ROLE_' . strtoupper($userGroup['name']);
        }

        return new User(
            (int)$userData['id'],
            $userData['login'],
            $userData['password'],
            $userGroups
        );
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        return $user;
    }

    public function supportsClass($class): bool
    {
        return User::class === $class;
    }

    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        return $this->loadUserByUsername($identifier);
    }
}
