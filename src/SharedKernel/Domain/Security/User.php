<?php

namespace ERP\SharedKernel\Domain\Security;

use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    private int $userId;

    private string $password;

    private array $roles = [];

    private string $username;

    public function __construct(int $userId, string $login, string $password, array $roles)
    {
        $this->userId = $userId;
        $this->password = $password;
        $this->roles = $roles;
        $this->username = $login;
    }

    public static function createFromPayload($username, array $payload)
    {
        if (isset($payload['roles'])) {
            return new static(0, $username, '', (array)$payload['roles']);
        }

        return new static(0, $username, '', []);
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getSalt(): ?string
    {
        return '';
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function eraseCredentials()
    {
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getUserIdentifier(): string
    {
        return (string)$this->getUsername();
    }
}
