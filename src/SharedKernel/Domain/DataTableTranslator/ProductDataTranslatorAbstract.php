<?php

namespace ERP\SharedKernel\Domain\DataTableTranslator;

use ERP\SharedKernel\Domain\DataTable\AbstractFilterAliasTranslator;

class ProductDataTranslatorAbstract implements AbstractFilterAliasTranslator
{
    public function getTranslator(): array
    {
        return [
            'id' => 'p.id',
            'name' => 'p.name',
            'description' => 'p.description',
            'category_id' => 'p.category_id',
            'type_id' => 'p.type_id',
            'category_name' => 'c.name',
            'type_name' => 't.name',
            'position_id' => 'ps.id',
            'position_name' => 'ps.name'
        ];
    }
}
