<?php

namespace ERP\SharedKernel\Domain\DataTableTranslator;

use ERP\SharedKernel\Domain\DataTable\AbstractFilterAliasTranslator;

class TravelProductsDataTranslatorAbstract implements AbstractFilterAliasTranslator
{
    public function getTranslator(): array
    {
        return [
            'scanned' => self::getScannedValueSql(),
            'correction_value' => 0,
            'id' => 'p.id',
            'name' => 'p.name',
            'planned' => 'tp.planned',
            'travel_id' => 'tp.travel_id'
        ];
    }

    private static function getScannedValueSql(): string
    {
        return <<<sql
            (
                SELECT
                    count(*)
                FROM
                    product_code_storage pcs
                LEFT JOIN product_code_usage pcu ON pcu.ean = pcs.ean
                WHERE
                    pcu.travel_id = tp.travel_id
                    AND pcs.product_id = p.id
                GROUP BY 
                    pcs.product_id
            )
sql;
    }

    private static function getCorrectionValueSql(): string
    {
        return <<<sql
            (
                SELECT
                    SUM(pc.value)
                FROM
                    product_corrections pc
                WHERE
                    pc.product_id
                    
                SELECT
            	    SUM(psl2.amount)
            	FROM
            	    product_code_view pcv2
            	LEFT JOIN `code` c ON c.id = pcv2.code_id
            	LEFT JOIN product_state_log psl2 ON psl2.code_id = c.id
            	WHERE
            	    pcv2.travel_id = tp.travel_id
            	AND pcv2.product_id = tp.product_id
            	AND c.type_id = 5
            )
sql;
    }
}