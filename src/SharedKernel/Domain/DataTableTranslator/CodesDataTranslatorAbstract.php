<?php

namespace ERP\SharedKernel\Domain\DataTableTranslator;

use ERP\SharedKernel\Domain\DataTable\AbstractFilterAliasTranslator;

class CodesDataTranslatorAbstract implements AbstractFilterAliasTranslator
{
    public function getTranslator(): array
    {
        return [
            'id' => 'pcs.id',
            'ean' => 'pcs.ean',
            'product_name' => 'p.name',
            'product_id' => 'p.id',
            'production_place_id' => 'pd.position_id',
            'travel_place_id' => 'td.position_id',
            'production_date' => 'pcu.production_scanned_at',
            'travel_date' => 'pcu.travel_scanned_at'
//            'id' => 'pcv.code_id',
//            'ean' => 'c.ean',
//            'code_type' => 'ct.name',
//            'product_name' => 'p.name',
//            'product_id' => 'p.id',
//            'production_date' => 'pcv.production_scanned_at',
//            'travel_date' => 'pcv.travel_scanned_at',
//            'created_at' => 'c.created_at',
//            'production_place_id' => 'pd.position_id',
//            'travel_place_id' => 'td.position_id',
        ];
    }
}
