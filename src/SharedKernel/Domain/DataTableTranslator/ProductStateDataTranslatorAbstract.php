<?php

namespace ERP\SharedKernel\Domain\DataTableTranslator;

use ERP\SharedKernel\Domain\DataTable\AbstractFilterAliasTranslator;

class ProductStateDataTranslatorAbstract implements AbstractFilterAliasTranslator
{
    public function getTranslator(): array
    {
        return [
            'code_id' => 'pcs.ean',
            'device_id' => 'd.id',
            'device_name' => 'd.name',
            'type_name' => '"Kod"',
            'code_position_id' => 'p.name',
            'date' => 'pcs.created_at'
        ];
    }
}
