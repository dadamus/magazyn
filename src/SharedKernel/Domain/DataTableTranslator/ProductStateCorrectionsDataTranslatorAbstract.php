<?php

namespace ERP\SharedKernel\Domain\DataTableTranslator;

use ERP\SharedKernel\Domain\DataTable\AbstractFilterAliasTranslator;

class ProductStateCorrectionsDataTranslatorAbstract implements AbstractFilterAliasTranslator
{
    public function getTranslator(): array
    {
        return [
            'code_id' => '"correction"',
            'device_id' => '""',
            'device_name' => 'pc.value',
            'code_position_id' => '""',
            'date' => 'pc.created_at'
        ];
    }
}