<?php

namespace ERP\SharedKernel\Domain\DataTableTranslator;

use ERP\SharedKernel\Domain\DataTable\AbstractFilterAliasTranslator;

class TravelProductCodeDataTranslatorAbstract implements AbstractFilterAliasTranslator
{
    public function getTranslator(): array
    {
        return [
            'code_id' => 'c.id',
            'ean' => 'c.ean',
            'travel_scanned_at' => 'pcv.travel_scanned_at'
        ];
    }
}
