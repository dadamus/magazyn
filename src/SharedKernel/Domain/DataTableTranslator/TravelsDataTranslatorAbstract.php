<?php

namespace ERP\SharedKernel\Domain\DataTableTranslator;

use ERP\SharedKernel\Domain\DataTable\AbstractFilterAliasTranslator;

class TravelsDataTranslatorAbstract implements AbstractFilterAliasTranslator
{
    public function getTranslator(): array
    {
        return [
            'id' => 't.id',
            'name' => 't.name',
            'date' => 't.started_at',
            'plate' => 'c.plate',
            'author' => 'a.name',
            'car_id' => 'c.id',
            'reported' => 'tr.reported'
        ];
    }
}
