<?php

namespace ERP\SharedKernel\Application\Controller;

use ERP\Product\Infrastructure\ProductStateLogsInterface;
use ERP\Settings\Infrastructure\ActiveDevicesInterface;
use ERP\Settings\Infrastructure\DeviceRepositoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends CoreController
{
//    public function indexAction(
//        ProductStateLogsInterface $logs,
//        DeviceRepositoryInterface $devices,
//        ActiveDevicesInterface $activeDevices
//    ): Response {
//        return $this->render('Hello/index.html.twig', [
//            'codes' => $logs->getAll(15),
//            'devices' => $devices->getAll(),
//            'active_devices' => $activeDevices->getAll()
//        ]);
//    }

    /**
     * @Route("/{reactRouting}", name="app_home", requirements={"reactRouting"="^(?!api|_profiler).+"}, defaults={"reactRouting": null})
     */
    public function indexAction()
    {
        return $this->render('react.html.twig');
    }

    /**
     * @Route("/api/devices", name="getDevices")
     */
    public function getDevices(DeviceRepositoryInterface $devices, ActiveDevicesInterface $activeDevices): Response
    {
        return new JsonResponse([
            'devices' => $devices->getAll(),
            'activeDevices' => $activeDevices->getAll()
        ]);
    }

    /**
     * @Route("/api/last-scanned-codes")
     */
    public function getLastScannedCodes(ProductStateLogsInterface $logs): Response
    {
        return new JsonResponse($logs->getAll());
    }
}
