<?php

namespace ERP\SharedKernel\Application\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends AbstractController
{
    public function loginFormAction(AuthenticationUtils $authenticationUtils): Response
    {
        return $this->render('Login/login.html.twig', [
            'error' => $authenticationUtils->getLastAuthenticationError()
        ]);
    }
}
