<?php

namespace ERP\SharedKernel\Infrastructure\DataTableRepository;

use Doctrine\DBAL\Connection;
use ERP\SharedKernel\Domain\DataTable\AbstractQueryBuilder;
use ERP\SharedKernel\Infrastructure\DataTableRepositoriesInterface;

class PDO implements DataTableRepositoriesInterface
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getData(AbstractQueryBuilder $queryBuilder): array
    {
        $join = '';

        foreach ($queryBuilder->getJoins() as $joinPart) {
            $join .= ' ' . $joinPart . ' ';
        }

        $filters = '';

        foreach ($queryBuilder->getFilters()->getFilters() as $filter) {
            if ($filters !== '') {
                $filters .= ' AND ';
            }

            if ($filter['type'] === 'date') {
                $dateValue = explode('|', $filter['value']);

                $filters .= sprintf('(%s > "%s" AND %s < "%s")', $filter['name'], $dateValue[0], $filter['name'], $dateValue[1]);
            } else {
                switch ($filter['type']) {
                    case 'number-strict':
                        $filters .= $filter['name'] . ' = ' . $filter['value'];
                        break;

                    case 'text-search':
                        $filters .= $filter['name'] . ' LIKE "%' . $filter['value'] . '%"';
                        break;
                }
            }
        }

        $whereSection = '';
        if ($filters !== '' || !empty($queryBuilder->getWhere())) {
            $whereSection .= ' WHERE ';
        }

        $parsedWhere = '';
        foreach ($queryBuilder->getWhere() as $singleWhere) {
            if ($parsedWhere !== '') {
                $parsedWhere .= ' AND ';
            }
            $parsedWhere .= $singleWhere . ' ';
        }

        $whereSection .= $parsedWhere;

        if ($filters !== '') {
            if (!empty($queryBuilder->getWhere())) {
                $whereSection .= ' AND ';
            }

            $whereSection .= $filters . ' ';
        }
        $select =
            'SELECT ' . $queryBuilder->getSelect()
            . ' FROM ' . $queryBuilder->getFrom() . $join . $whereSection
            . $queryBuilder->getOrderBy();

        if ($queryBuilder->getLimit() > 0) {
            $select .= ' LIMIT ' . $queryBuilder->getLimit() . ' OFFSET ' . $queryBuilder->getOffset();
        }

        return $this->connection->fetchAllAssociative($select);
    }

}
