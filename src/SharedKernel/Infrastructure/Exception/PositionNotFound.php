<?php

namespace ERP\SharedKernel\Infrastructure\Exception;

use Exception;
use Throwable;

class PositionNotFound extends Exception
{
    public function __construct(int $positionId)
    {
        parent::__construct('Stanowisko o id: ' . $positionId . ' nie zostało znalezione!');
    }
}
