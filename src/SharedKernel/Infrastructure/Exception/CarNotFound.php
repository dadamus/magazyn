<?php

namespace ERP\SharedKernel\Infrastructure\Exception;

use Exception;
use Throwable;

class CarNotFound extends Exception
{
    public function __construct(int $carId)
    {
        parent::__construct('Auto o id: ' . $carId . ' nie zostało znalezione!');
    }
}
