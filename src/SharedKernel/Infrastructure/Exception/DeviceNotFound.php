<?php

namespace ERP\SharedKernel\Infrastructure\Exception;

use Exception;
use Throwable;

class DeviceNotFound extends Exception
{
    public function __construct(int $deviceId)
    {
        parent::__construct('Urządzenie o id: ' . $deviceId . ' nie zostało znalezione!');
    }
}
