<?php

namespace ERP\SharedKernel\Infrastructure\Exception;

use Exception;

class TravelNotFound extends Exception
{
    public function __construct(int $travelId)
    {
        parent::__construct('Trasa o id: ' . $travelId . ' nie została znaleziona!');
    }
}
