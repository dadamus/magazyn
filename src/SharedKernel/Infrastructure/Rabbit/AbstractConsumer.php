<?php

declare(strict_types=1);

namespace ERP\SharedKernel\Infrastructure\Rabbit;

use AMQPConnectionException;
use Exception;
use PhpAmqpLib\Connection\AMQPStreamConnection;

abstract class AbstractConsumer
{
    protected const MAX_NUMBER_CONNECTION_ATTEMPTS = 5;

    private AMQPStreamConnection $connection;

    public function __construct(
        private readonly string $rabbitHost,
        private readonly int $rabbitPort,
        private readonly string $rabbitUser,
        private readonly string $rabbitPassword,
        private readonly string $rabbitVHost,
    )
    {
    }

    protected function initConnection()
    {
        for (
            $numberOfConnectionAttempts = 0;
            $numberOfConnectionAttempts < self::MAX_NUMBER_CONNECTION_ATTEMPTS;
            $numberOfConnectionAttempts++
        ) {
//            try {
            $this->connection = new AMQPStreamConnection($this->rabbitHost, $this->rabbitPort, $this->rabbitUser, $this->rabbitPassword, $this->rabbitVHost);

            return;
//            } catch (Exception $exception) {
//                sleep(2);
//                continue;
//            }
        }

        throw new AMQPConnectionException('Rabbit connection problems!', 500);
    }

    protected function createQueue(string $name, string $exchangeName, string $exchangeType): void
    {
        $channel = $this->connection->channel();

        $channel->queue_declare($name, false, true, false, false);
        $channel->exchange_declare($exchangeName, $exchangeType, false, true, false);
        $channel->queue_bind($name, $exchangeName);

        $channel->close();
    }

    protected function startConsumer(string $queueName, callable $callback): void
    {
        $channel = $this->connection->channel();
        $channel->basic_qos(null, 1, null);
        $channel->basic_consume($queueName, '', false, false, false, false, $callback);

        while (count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
    }
}
