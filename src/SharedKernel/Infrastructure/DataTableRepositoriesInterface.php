<?php

namespace ERP\SharedKernel\Infrastructure;

use ERP\SharedKernel\Domain\DataTable\AbstractQueryBuilder;

interface DataTableRepositoriesInterface
{
    public function getData(AbstractQueryBuilder $queryBuilder): array;
}
