<?php

namespace ERP\SharedKernel\Infrastructure\EventStore;

use Prooph\EventSourcing\AggregateChanged;
use Prooph\ServiceBus\EventBus;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\NoHandlerForMessageException;
use Symfony\Component\Messenger\MessageBusInterface;

class EventHandler
{
    private MessageBusInterface $sharedEventBus;

    public function __construct(EventBus $eventBus, MessageBusInterface $sharedEventBus)
    {
        $this->sharedEventBus = $sharedEventBus;

        $router = new EventRouter();

        $router->route(AggregateChanged::class)->to([$this, 'handle']);

        $router->attachToMessageBus($eventBus);
    }

    public function handle(AggregateChanged $aggregateChanged): void
    {
        try {
            $this->sharedEventBus->dispatch(new Envelope($aggregateChanged));
        } catch (NoHandlerForMessageException $exception) {
        }
    }
}
