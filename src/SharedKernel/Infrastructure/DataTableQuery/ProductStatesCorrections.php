<?php

namespace ERP\SharedKernel\Infrastructure\DataTableQuery;

use ERP\SharedKernel\Domain\DataTable\AbstractFilterAliasTranslator;
use ERP\SharedKernel\Domain\DataTable\AbstractQueryBuilder;

class ProductStatesCorrections extends AbstractQueryBuilder
{
    private int $productId;

    public function __construct(int $productId)
    {
        $this->productId = $productId;
    }

    public function build(AbstractFilterAliasTranslator $translator): void
    {
        $this
            ->addSelect($translator)
            ->addFrom('product_corrections pc')
            ->addWhere('pc.product_id = ' . $this->productId);
    }
}