<?php

namespace ERP\SharedKernel\Infrastructure\DataTableQuery;

use ERP\SharedKernel\Domain\DataTable\AbstractFilterAliasTranslator;
use ERP\SharedKernel\Domain\DataTable\AbstractQueryBuilder;

class ProductStates extends AbstractQueryBuilder
{
    private int $productId;

    public function __construct(int $productId)
    {
        $this->productId = $productId;
    }

    public function build(AbstractFilterAliasTranslator $translator): void
    {
        $this
            ->addSelect($translator)
            ->addFrom('product_code_storage pcs')
            ->addJoin('LEFT JOIN product_code_usage pcu ON pcu.ean = pcs.ean')
            ->addJoin('LEFT JOIN device d ON d.id = pcu.device_id')
            ->addJoin('LEFT JOIN position p ON p.id = d.position_id')
            ->addWhere('pcs.product_id = ' . $this->productId)
            ->addWhere('pcu.scanned_at IS NOT NULL');
    }
}
