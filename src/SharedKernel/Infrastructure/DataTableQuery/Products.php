<?php

namespace ERP\SharedKernel\Infrastructure\DataTableQuery;

use ERP\SharedKernel\Domain\DataTable\AbstractFilterAliasTranslator;
use ERP\SharedKernel\Domain\DataTable\AbstractQueryBuilder;

class Products extends AbstractQueryBuilder
{
    function build(AbstractFilterAliasTranslator $translator): void
    {
        $this
            ->addSelect($translator)
            ->addFrom('product p')
            ->addJoin('LEFT JOIN category c ON c.id = p.category_id')
            ->addJoin('LEFT JOIN type t ON t.id = p.type_id')
            ->addJoin('LEFT JOIN product_position pp ON pp.product_id = p.id')
            ->addJoin('LEFT JOIN position ps ON ps.id = pp.position_id');
    }
}
