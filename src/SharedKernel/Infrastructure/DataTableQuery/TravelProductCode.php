<?php

namespace ERP\SharedKernel\Infrastructure\DataTableQuery;

use ERP\SharedKernel\Domain\DataTable\AbstractFilterAliasTranslator;
use ERP\SharedKernel\Domain\DataTable\AbstractQueryBuilder;

class TravelProductCode extends AbstractQueryBuilder
{
    private int $travelId;

    private int $productId;

    public function __construct(int $travelId, int $productId)
    {
        $this->travelId = $travelId;
        $this->productId = $productId;
    }

    public function build(AbstractFilterAliasTranslator $translator): void
    {
        $this
            ->addSelect($translator)
            ->addFrom('product_code_view pcv')
            ->addJoin('LEFT JOIN `code` c ON c.id = pcv.code_id')
            ->addWhere('pcv.travel_id = ' . $this->travelId)
            ->addWhere('pcv.product_id = ' . $this->productId)
            ->addWhere('c.type_id = 1');
    }
}
