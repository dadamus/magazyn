<?php

namespace ERP\SharedKernel\Infrastructure\DataTableQuery;

use ERP\SharedKernel\Domain\DataTable\AbstractFilterAliasTranslator;
use ERP\SharedKernel\Domain\DataTable\AbstractQueryBuilder;

class Travels extends AbstractQueryBuilder
{
    public function build(AbstractFilterAliasTranslator $translator): void
    {
        $this
            ->addSelect($translator)
            ->addFrom('travel t')
            ->addJoin('LEFT JOIN user a ON a.id = t.author_id')
            ->addJoin('LEFT JOIN car c ON c.id = t.car_id')
            ->addJoin('LEFT JOIN travel_report tr ON tr.travel_id = t.id');
    }
}
