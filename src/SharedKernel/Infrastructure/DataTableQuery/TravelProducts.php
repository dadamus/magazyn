<?php

namespace ERP\SharedKernel\Infrastructure\DataTableQuery;

use ERP\SharedKernel\Domain\DataTable\AbstractFilterAliasTranslator;
use ERP\SharedKernel\Domain\DataTable\AbstractQueryBuilder;

class TravelProducts extends AbstractQueryBuilder
{
    public function build(AbstractFilterAliasTranslator $translator): void
    {
        $this
            ->addSelect($translator)
            ->addFrom('travel_product as tp')
            ->addJoin('LEFT JOIN `product` p ON p.id = tp.product_id');
    }
}