<?php

namespace ERP\SharedKernel\Infrastructure\DataTableQuery;

use ERP\SharedKernel\Domain\DataTable\AbstractFilterAliasTranslator;
use ERP\SharedKernel\Domain\DataTable\AbstractQueryBuilder;

class Codes extends AbstractQueryBuilder
{
    public function build(AbstractFilterAliasTranslator $translator): void
    {
        $this
            ->addSelect($translator)
            ->addFrom('product_code_storage pcs')
            ->addJoin('LEFT JOIN product p ON p.id = pcs.product_id')
            ->addJoin('LEFT JOIN product_code_usage pcu ON pcu.ean = pcs.ean')
            ->addJoin('LEFT JOIN device pd ON pd.id = pcu.production_device_id')
            ->addJoin('LEFT JOIN device td ON td.id = pcu.travel_device_id');
    }
}
