import React, {useEffect, useState} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import NewProductModal from "../Product/ProductList/NewProductModal";
import DataTable from "../SharedKernel/DataTable";
import Filters from "./CodeList/Filters";
import ProductCardButton from "../Product/ProductList/ProductCardButton";
import Actions from "../Product/ProductList/Actions";
import {post} from "../SharedKernel/Api";
import {toast} from "react-toastify";

export default function CodeList()
{
    const [filters, setFilters] = useState({page: 1, length: 50, filters: {}});
    const [rows, setRows] = useState([]);
    const [loading, setLoading] = useState(false);

    const columns = [
        {
            data: 'id',
            label: 'ID',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'ean',
            label: 'Kod',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'product_name',
            label: 'Produkt',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'production_date',
            label: 'Data produkcji',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'travel_date',
            label: 'Data trasy',
            searchable: true,
            search: {
                value: null
            }
        }
    ];

    const changeFilters = (name, value) => {
        let newFilters = filters;
        newFilters['filters'][name] = value;

        setFilters({...filters, ...newFilters});
    }

    useEffect(() => {
        search();
    }, []);

    const search = () => {
        if (!loading) {
            setLoading(true);
            post(
                'api/codes/list/filter', {columns, filters}
            ).then((response) => {
                return response.json();
            }).then((data) => {
                setRows(data.data)
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false)
            });
        }
    }

    return <>
        <Row className="subhead">
            <div className="title">
                Kody
                <div className="breadcrumb">
                    Lista kodów
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={12}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Lista kodów
                        </div>
                    </div>
                    <div className="portlet-body">
                        <Filters/>
                        <DataTable
                            filters={filters}
                            rows={rows}
                            loading={loading}
                            columns={columns}/>
                    </div>
                </div>
            </Col>
        </Row>
    </>
}
