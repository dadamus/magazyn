import React, {useState} from 'react';
import {Button, FormCheck, Modal} from "react-bootstrap";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Positions from "../../SharedKernel/Form/Product/PositionsSelector";
import DeviceTypeSelector from "../../SharedKernel/Form/DeviceTypeSelector";
import {post} from "../../SharedKernel/Api";
import {toast} from "react-toastify";

export default function NewDeviceModal(props) {
    const [loading, setLoading] = useState(false);
    const [deviceData, setDeviceData] = useState({});
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    let changeFormData = (name, value) => {
        let newData = {};
        newData[name] = value;

        setDeviceData({...deviceData, ...newData})
    }

    let handleSubmit = () => {
        setLoading(true);

        post(
            'api/settings/device/add',
            deviceData
        ).then((response) => {
            if (response.status !== 200) {
                throw 'Wystapil blad!';
            }

            return response.json();
        }).then((data) => {
            toast.success('Nowe urządzenie zostało dodane!');
            props.refreshList();
        }).catch(() => {
            toast.error('Wystąpił błąd!');
        }).finally(() => {
            setLoading(false);
            handleClose();
        })
    };

    return <>
        <Button onClick={handleShow}><FontAwesomeIcon icon={faPlus}/> Dodaj urządzenie</Button>

        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Formularz urządzenia</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form>
                    <div className="form-group">
                        <label htmlFor="name">Nazwa</label>
                        <input
                            type="text"
                            className="form-control"
                            itemID="name"
                            name="name"
                            placeholder="Nazwa"
                            value={deviceData.name}
                            onChange={event => changeFormData('name', event.target.value)}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="type">Tryb</label>
                        <DeviceTypeSelector
                            itemID="type"
                            name="type"
                            placeholder="Wybierz..."
                            onChange={event => changeFormData('type', event.target.value)}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="active">Aktywny</label>
                        <FormCheck
                            id="active"
                            onChange={event => changeFormData('active', event.target.value === 'on')}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="position">Stanowisko</label>
                        <Positions
                            itemID="position"
                            name="position"
                            placeholder="Wybierz..."
                            onChange={event => changeFormData('position', event.target.value)}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="name">Hash</label>
                        <input
                            type="text"
                            className="form-control"
                            itemID="hash"
                            name="hash"
                            placeholder="Hash"
                            value={deviceData.hash}
                            onChange={event => changeFormData('hash', event.target.value)}
                        />
                    </div>
                </form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose} disabled={loading}>
                    Zamknij
                </Button>
                <Button variant="primary" onClick={handleSubmit} disabled={loading}>
                    Dodaj
                </Button>
            </Modal.Footer>
        </Modal>
    </>;
}
