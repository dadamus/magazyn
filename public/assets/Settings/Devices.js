import React, {useEffect, useState} from 'react';
import {Button, Col, Container, Row, Table} from "react-bootstrap";
import NewDeviceModal from "./Devices/NewDeviceModal";
import ProductCardButton from "../Product/ProductList/ProductCardButton";
import Actions from "../Product/ProductList/Actions";
import DataTable from "../SharedKernel/DataTable";
import {get} from "../SharedKernel/Api";
import {toast} from "react-toastify";

export default function Devices() {
    const [rows, setRows] = useState([]);
    const [loading, setLoading] = useState(false);

    const columns = [
        {
            data: 'id',
            label: 'ID',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'name',
            label: 'Nazwa',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'device_type_name',
            label: 'Tryb',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'active',
            label: 'Aktywny',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'position_name',
            label: 'Stanowisko',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'hash',
            label: 'Hash',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'travel_name',
            label: 'Ostatnia trasa',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'last_sync',
            label: 'Ostatnia synchronizacja',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'last_ip',
            label: 'IP',
            searchable: true,
            search: {
                value: null
            }
        }
    ];

    useEffect(() => {
        search();
    }, []);

    const search = () => {
        if (!loading) {
            setLoading(true);
            get(
                'api/settings/devices'
            ).then((response) => {
                return response.json();
            }).then((data) => {
                setRows(data)
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false)
            });
        }
    }

    return <>
        <Row className="subhead">
            <div className="title">
                Ustawienia
                <div className="breadcrumb">
                    Urządzenia
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={12}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Lista urządzeń
                        </div>
                        <div className="toolbar">
                            <NewDeviceModal refreshList={search}/>
                        </div>
                    </div>
                    <div className="portlet-body">
                        <DataTable
                            columns={columns}
                            rows={rows}
                            loading={loading}
                        />
                    </div>
                </div>
            </Col>
        </Row>
    </>;
}
