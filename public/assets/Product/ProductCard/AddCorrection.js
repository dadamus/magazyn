import React, {useEffect, useState} from 'react';
import {Button, Modal} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAmbulance} from "@fortawesome/free-solid-svg-icons";
import {post} from "../../SharedKernel/Api";
import {toast} from "react-toastify";

export default function AddCorrection(props) {
    const [show, setShow] = useState(false);
    const [loading, setLoading] = useState(false);
    const [correctionNumber, setCorrectionNumber] = useState(0);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const positiveCorrection = () => {
        sendCorrection(correctionNumber);
    };

    const negativeCorrection = () => {
        sendCorrection(-correctionNumber);
    };

    const sendCorrection = (correctionValue) => {
        setLoading(true);
        post(
            '/api/product/card/' + props.productId + '/correction',
            {'correction-value': correctionValue}
        ).then((data) => {
            toast.success('Korekta dodana!');
        }).catch(() => {
            toast.error('Wystąpił błąd!');
        }).finally(() => {
            setLoading(false);
            handleClose();
        });
    };

    return <>
        <Button onClick={handleShow} variant={"outline-danger"}><FontAwesomeIcon icon={faAmbulance}/> Korekta</Button>


        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Dodaj korekte</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form>
                    <input
                        type="number"
                        className="form-control"
                        placeholder="Wartość korekty"
                        value={correctionNumber}
                        onChange={(event => setCorrectionNumber(event.target.value))}
                    />
                </form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" disabled={loading} onClick={handleClose}>
                    Zamknij
                </Button>
                <Button variant="primary" disabled={loading} onClick={positiveCorrection}>
                    +
                </Button>
                <Button variant="danger" disabled={loading} onClick={negativeCorrection}>
                    -
                </Button>
            </Modal.Footer>
        </Modal>
    </>
}
