import React, {useEffect, useState} from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import {Button, Modal} from "react-bootstrap";
import {post} from "../../SharedKernel/Api";
import {toast} from 'react-toastify';

export default function GenerateCodes(props) {
    const [show, setShow] = useState(false);
    const [loading, setLoading] = useState(false);
    const [printNumber, setPrintNumber] = useState(50);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handlePrint = () => {
        setLoading(true);
        post(
            'api/product/card/' + props.productId + '/generate',
            {code_length: printNumber}
        ).then((response) => {
            if (response.status !== 200) {
                throw 'Exception!';
            }

            return response.json();
        }).then((data) => {
            toast.success('Zlecono drukowanie kodów!');
        }).catch(() => {
            toast.error('Wystąpił błąd!');
        }).finally(() => {
            handleClose();
            setLoading(false);
        })
    };

    return <>
        <Button onClick={handleShow}><FontAwesomeIcon icon={faPlus}/> Generuj kody</Button>


        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Generuj kody</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form>
                    <div className="form-group">
                        <label htmlFor="name">Ilość</label>
                        <input
                            type="number"
                            value={printNumber}
                            onChange={(event) => {
                                setPrintNumber(event.target.value)
                            }}
                            className="form-control"
                            itemID="name"
                            name="name"
                            placeholder="Ilość"
                        />
                    </div>
                </form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" disabled={loading} onClick={handleClose}>
                    Zamknij
                </Button>
                <Button variant="primary" disabled={loading} onClick={handlePrint}>
                    Dodaj
                </Button>
            </Modal.Footer>
        </Modal>
    </>
}
