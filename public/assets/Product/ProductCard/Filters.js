import React, {useEffect, useState} from 'react';
import {Button, Col, Row} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch, faTimes} from "@fortawesome/free-solid-svg-icons";
import DeviceSelector from "../../SharedKernel/Form/DeviceSelector";
import Positions from "../../SharedKernel/Form/Product/PositionsSelector";

export default function Filters(props) {
    return <div className="filters">
        <form>
            <Row>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="type">Urządzenie:</label>
                        <DeviceSelector
                            itemID="device"
                            name="device"
                            placeholder="Wybierz"
                            onChange={event => props.changeFilters('device', event.target.value)}
                        />
                    </div>
                </Col>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="type">Stanowisko:</label>
                        <Positions
                            itemID="position"
                            name="position"
                            placeholder="Wybierz"
                            onChange={event => props.changeFilters('position', event.target.value)}
                        />
                    </div>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <div className="separator"/>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <Button onClick={props.search}><FontAwesomeIcon icon={faSearch}/> Szukaj</Button>
                    &nbsp;&nbsp;
                    <Button variant="secondary">
                        <FontAwesomeIcon icon={faTimes}/> Reset
                    </Button>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <div className="separator"/>
                </Col>
            </Row>
        </form>
    </div>
}
