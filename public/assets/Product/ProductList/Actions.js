import React, {useEffect, useState} from 'react';
import {DropdownButton, Dropdown} from "react-bootstrap";

export default function Actions(props) {
    return <DropdownButton id="dropdown-basic-button" title="Akcje">
        <Dropdown.Item href="#/action-1">Edytuj</Dropdown.Item>
    </DropdownButton>
}
