import React from 'react';
import {Table} from "react-bootstrap";
import {Spinner} from "react-bootstrap";
import {post} from "./Api";

export default function DataTable(props) {
    const ParsedData = () => {
        if (props.loading) {
            return <tr>
                <td colSpan={Object.entries(props.columns).length} style={{textAlign: 'center'}}><Spinner
                    animation="grow"/></td>
            </tr>;
        }

        if (Object.entries(props.rows).length === 0) {
            return <tr>
                <td colSpan={Object.entries(props.columns).length}
                    style={{textAlign: 'center'}}>{props.messageEmpty ?? "Brak wierszy"}</td>
            </tr>;
        }

        let index = 0;
        return props.rows.map((row) => {
            return <tr>{props.columns.map(column => {
                index++;

                if (typeof column.render !== "undefined") {
                    return <td key={index}>
                        <column.render row={row}/>
                    </td>
                }

                return <td key={index}>{row[column.data]}</td>
            })}</tr>
        })
            ;
    }

    return <Table bordered>
        <thead>
        <tr>
            {Object.keys(props.columns).map((index) => (
                <th key={index} width={props.columns[index]['width'] ?? 'auto'}>{props.columns[index]['label']}</th>
            ))}
        </tr>
        </thead>
        <tbody>
        <ParsedData/>
        </tbody>
    </Table>;
}
