import React, {useEffect, useState} from "react";
import Selector from "../Selector";

export default function Positions(props) {
    let parser = (data) => {
        return data.map((option) => {
            return {name: option.name, value: option.id}
        });
    }

    return <Selector
        itemID={props.itemID}
        name={props.name}
        placeholder={props.placeholder}
        path="api/product/positions"
        parseOptions={parser}
        onChange={props.onChange}
    />
}
