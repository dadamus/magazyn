import React, {useEffect, useState} from "react";
import {Col, Spinner} from "react-bootstrap";
import {get} from "../Api";

export default function Selector(props) {
    const [loaded, setLoaded] = useState(false);
    const [options, setOptions] = useState([]);

    useEffect(() => {
        if (!loaded) {
            let initOptions = [];
            if (typeof props.placeholder !== "undefined") {
                initOptions = [...options, {name: props.placeholder, value: ''}];
            }

            get(
                props.path
            ).then((response) => {
                return response.json();
            }).then((data) => {
                let parsedOptions = props.parseOptions(data);

                setOptions([...initOptions, ...parsedOptions]);
            }).finally(() => {
                setLoaded(true)
            });
        }
    }, []);

    let getOptions = () => {
        return options.map((option) => {
            return <option value={option.value} key={option.value}>{option.name}</option>
        });
    };

    let getSelect = () => {
        if (!loaded) {
            return <Col lg={2}><Spinner animation="grow"/></Col>
        }

        return <select
            className="form-control form-control-sm"
            itemID={props.itemID}
            name={props.name}
            placeholder={props.placeholder}
            onChange={props.onChange}
        >
            {getOptions()}
        </select>
    }

    return getSelect()
}
