let $filters = $('#kt_filters');

let productId = $('#travel_product_list_table').data('product-id');
let travelId = $('#travel_product_list_table').data('travelId');

let table = $('#travel_product_list_table').DataTable({
    "processing": true,
    "serverSide": true,
    "searching": false,
    "ajax": {
        "url": "/travel/card/" + travelId + "/codes/" + productId + "/list",
        "type": "POST",
        "data": function (data) {
            $('.kt-input').each(function () {
                let colIndex = $(this).data('col-index');
                data.columns[colIndex]['search']['value'] = $(this).val();
                data.columns[colIndex]['search']['type'] = $(this).data('filter-type');
            });
        }
    },
    "order": [[2, "desc"]],
    "columnDefs": [
        {
            "targets": 0,
            "data": "code_id"
        },
        {
            "targets": 1,
            "data": "ean"
        },
        {
            "targets": 2,
            "data": "travel_scanned_at"
        },
        {
            "targets": 3,
            "orderable": false,
            "render": function (data, type, full) {
                return `
                    <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-outline-danger dropdown-toggle"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Akcje
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" x-placement="top-end">
                        <button class="dropdown-item code-remove-button" data-code-id="` + full.code_id + `" type="button">
                        Usuń</button>
                    </div>
                </div>
                `;
            }
        }
    ]
});

let resetFilters = () => {
    $filters.find('input').each(function () {
        $(this).val('');
    });

    $filters.find('select').each(function () {
        $(this).val('');
    });
};

$(document).on('click', '.travel-edit-button', function (e) {
    let travelId = $(this).data('travel-id');

    loadModal(travelId);
});

$('#kt_search').on('click', function (e) {
    e.preventDefault();

    table.table().draw();
});

$('#kt_reset').on('click', function (e) {
    e.preventDefault();

    resetFilters();

    table.table().draw();
});


$(document).on('click', '.code-remove-button', function () {
    let codeId = $(this).data('code-id');
    let $this = $(this);

    KTApp.blockPage();

    $.ajax({
        method: 'DELETE',
        url: '/travel/card/' + travelId + '/codes/' + productId + '/remove',
        data: 'code-id=' + codeId
    }).always(function () {
        KTApp.unblockPage();
    }).done(function () {
        toastr.success('Kod został usunięty!');
        table.table().draw();
    }).fail(function () {
        toastr.error('Wystąpił błąd podczas usuwania kodu...');
    });
});