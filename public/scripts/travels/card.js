const $generateModal = $("#generate-modal");
const $addProductForm = $("#add-product-form");

const $correctionModal = $("#correction-modal");
const travelId = $('#travel-card').data('travel-id');
const allVisible = $("#travel-card").data('all-visible');

$('.datepicker').datepicker({
    'format': 'dd-mm-yyyy'
});

$("#product-search").select2({
    width: "100%",
    minimumInputLength: 3,
    placeholder: 'Wyszukaj produkt',
    ajax: {
        url: '/product/find'
    }
});

$("#show-generate-codes").on('click', function () {
    $generateModal.modal('show');
});

$("#submit-generate-codes").on('click', function (e) {
    e.preventDefault();

    $addProductForm.submit();
});

$("#show-travel-code").on('click', function (e) {
    KTApp.blockPage();

    let travelId = $(this).data('travel-id');

    $.ajax({
        method: 'POST',
        url: '/travel/card/' + travelId + '/code/generate'
    }).always(function () {
        KTApp.unblockPage();
    }).done(function (response) {
        let win = window.open('/print/codes/' + response, '_blank');
        win.focus();
    }).fail(function () {
        toastr.error("Wystąpił błąd podczas generowania kodu!");
    });
});

$('#save-report').on('click', function (e) {
    e.preventDefault();

    let travelId = $(this).data('travel-id');
    let reported = $('input#reported').is(':checked');
    let reportText = $("#report-text").val();

    KTApp.blockPage();

    $.ajax({
        method: 'POST',
        url: '/travel/card/' + travelId + '/report',
        data: 'reported=' + reported + "&report-text=" + reportText
    }).always(function () {
        KTApp.unblockPage()
    }).done(function () {
        toastr.success("Trasa została zgłoszona");
    }).fail(function () {
        toastr.error("Wystąpił błąd podczas zgłaszania trasy");
    });
});

$addProductForm.on('submit', function (e) {
    e.preventDefault();

    let data = $(this).serialize();
    let travelId = $(this).data('travel-id');

    $generateModal.modal('hide');
    KTApp.blockPage();

    $.ajax({
        method: 'POST',
        url: '/travel/card/' + travelId + '/product',
        data: data
    }).always(function () {
        KTApp.unblockPage();
    }).done(function () {
        toastr.success("Produkt został dodany!");
        reloadTable(travelId);
    }).fail(function () {
        toastr.error("Wystąpił błąd podczas dodawania produktu!");
    });
});

reloadTable = (travelId) => {
    KTApp.block('#kt_filters');

    $.ajax({
        method: 'GET',
        url: '/travel/card/' + travelId + '/list'
    }).always(function () {
        KTApp.unblock('#kt_filters')
    }).done(function (response) {
        $('#kt_filters').html(response);
    }).fail(function () {
        toastr.error("Wystąpił błąd podczas ładowania listy produktów!");
    });
};

let timeout;

$("#product_list_table").on('keyup', '.planned-quantity', function (e) {
    clearTimeout(timeout);

    let productId = $(this).data('product-id');
    let travelId = $(this).data('travel-id');
    let quantity = $(this).val();
    let $input = $(this);

    timeout = setTimeout(function () {
        KTApp.blockPage();

        $.ajax({
            url: '/travel/card/' + travelId + '/' + productId + '/change/quantity',
            method: 'POST',
            data: 'planned_quantity=' + quantity
        }).always(function () {
            KTApp.unblockPage();
        }).done(function () {
            $input.addClass('is-valid');
            $input.removeClass('is-invalid');
        }).fail(function () {
            $input.removeClass('is-valid');
            $input.addClass('is-invalid');
        });
    }, 1000);
});

let correctionProductId;

$('.show-correction').on('click', function () {
    correctionProductId = $(this).data('product-id');
    $correctionModal.modal('show');
});

$('#submit-positive-correction').on('click', function (e) {
    e.preventDefault();

    let correctionValue = $('#correction-value').val();

    if (correctionValue < 0) {
        toastr.error('Wartość korekty musi być dodatni!');
        return;
    }

    submitCorrection(correctionValue);
});

$('#submit-negative-correction').on('click', function (e) {
    e.preventDefault();

    let correctionValue = $('#correction-value').val();

    if (correctionValue < 0) {
        toastr.error('Wartość korekty musi być dodatni!');
        return;
    }

    submitCorrection(0 - correctionValue);
});

const submitCorrection = (value) => {
    KTApp.blockPage();

    $.ajax({
        method: 'POST',
        url: '/travel/card/' + travelId + '/correction',
        data: 'correction-value=' + value + "&product-id=" + correctionProductId
    }).always(function () {
        KTApp.unblockPage();
    }).done(function () {
        toastr.success("Korekta została dodana!");
    }).fail(function () {
        toastr.error("Wystąpił błąd podczas dodawania korekty!");
    });
};


///travel/card/{travelId}/codes/{productId}/list
