let $filters = $('#kt_filters');
let $editModal = $('#edit-modal');

$('.datepicker').datepicker({
    'format': 'dd-mm-yyyy'
});

let table = $('#travel_list_table').DataTable({
    "processing": true,
    "serverSide": true,
    "searching": false,
    "ajax": {
        "url": "/travels/list/filter",
        "type": "POST",
        "data": function (data) {
            $('.kt-input').each(function () {
                let colIndex = $(this).data('col-index');
                data.columns[colIndex]['search']['value'] = $(this).val();
                data.columns[colIndex]['search']['type'] = $(this).data('filter-type');
            });
        }
    },
    "columnDefs": [
        {
            "targets": 0,
            "data": "id"
        },
        {
            "targets": 1,
            "data": "name",
            "render": function (data, type, full) {
                let reportedIcon = '';

                if (full.reported) {
                    reportedIcon = '<i class="la la-exclamation-triangle"></i>';
                }

                return `
                    ` + reportedIcon + ` <a href="/travel/card/` + full.id + `">` + data + `</a>
                `;
            }
        },
        {
            "targets": 2,
            "data": "date",
            "render": function (data, type, full) {
                let date = new Date(data);

                let day = date.getDay();
                let month = date.getMonth();
                let year = date.getFullYear();

                if (day < 10) {
                    day = "0" + day;
                }

                if (month < 10) {
                    month = "0" + month
                }

                return day + '-' + month + '-' + year
            }
        },
        {
            "targets": 3,
            "data": "plate"
        },
        {
            "targets": 4,
            "data": "author"
        },
        {
            "targets": 5,
            "orderable": false,
            "render": function (data, type, full) {
                return `
                    <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-outline-danger dropdown-toggle"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Akcje
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" x-placement="top-end">
                        <button class="dropdown-item travel-edit-button" data-travel-id="` + full.id + `" type="button">Edytuj</button>
                    </div>
                </div>
                `;
            }
        },
        {
            "data": "car_id",
            "visible": false,
            "targets": 6
        },
        {
            "data": "reported",
            "visible": false,
            "targets": 7
        }
    ]
});

let resetFilters = () => {
    $filters.find('input').each(function () {
        $(this).val('');
    });

    $filters.find('select').each(function () {
        $(this).val('');
    });
};

$(document).on('click', '.travel-edit-button', function (e) {
    let travelId = $(this).data('travel-id');

    loadModal(travelId);
});

$('#kt_search').on('click', function (e) {
    e.preventDefault();

    table.table().draw();
});

$('#kt_reset').on('click', function (e) {
    e.preventDefault();

    resetFilters();

    table.table().draw();
});

$('#add-new-travel').on('click', function (e) {
    e.preventDefault();

    loadModal();
});

$('#submit-modal-form').on('click', function (e) {
    e.preventDefault();

    $editModal.find('form').submit();
});

/*
FORM SUBMIT
 */
$editModal.on('submit', '#travels-form', function (e) {
    e.preventDefault();
    KTApp.blockPage();

    let data = $(this).serialize();
    $editModal.modal('hide');

    let method = 'POST';
    let url = '/travels';

    if ($(this).data('travel-id') > 0) {
        method = 'PUT';
        url = '/travels/' + $(this).data('travel-id');
    }

    $.ajax({
        method: method,
        url: url,
        data: data
    }).always(function () {
        KTApp.unblockPage();
    }).done(function () {
        toastr.success('Nowy produkt został dodany!');

        table.table().draw();
    }).fail(function () {
        toastr.error('Wystąpił błąd podczas dodawania stanowiska!');
    });
});

/*
LOAD MODAL
 */
loadModal = (travelId) => {
    if (typeof travelId === 'undefined') {
        travelId = 0;
    }


    KTApp.blockPage();

    $.ajax({
        'method': 'get',
        'url': '/travels/modal/' + travelId
    }).always(function () {
        KTApp.unblockPage();
    }).done(function (response) {
        $editModal.find('.modal-body').html(response).find('.datepicker').datepicker({
            'format': 'dd-mm-yyyy'
        });
        $editModal.modal('show');
    }).fail(function () {
        toastr.error('Wystąpił błąd podczas ładowania!');
    });
};