$('.datepicker').datepicker({
    'format': 'dd-mm-yyyy'
});

$("#product_id").select2({
    width: "100%",
    minimumInputLength: 3,
    placeholder: 'Wyszukaj produkt',
    ajax: {
        url: '/product/find'
    }
});

const $filters = $("#report_filters");
const $report = $("#report");
const $reportChart = $("#report_chart");

let lastFormData;

$("#kt_generate").on('click', function (e) {
    e.preventDefault();
    $filters.submit();
});

$filters.on('submit', function (e) {
    e.preventDefault();

    lastFormData = $(this).serialize();

    KTApp.blockPage();

    $.ajax({
        method: "POST",
        url: "/report/product/list",
        data: lastFormData
    }).always(function () {
        KTApp.unblockPage();
    }).done(function (response) {
        $report.html(response);

        let data = JSON.parse($report.find('#report-data').html());
        generateChart(data);
    });
});

const generateChart = (data) => {
    let compactData = [];
    for (var i in data) {
        compactData.push(data[i]);
    }

    console.log(compactData);
    let chart = am4core.create($reportChart[0], am4charts.XYChart);
    chart.data = compactData;

    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "date_start";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 30;

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = "Ilość sztuk";
    valueAxis.title.fontWeight = 800;

    // Create series
    let series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueY = "production";
    series.dataFields.categoryX = "date_start";
    series.clustered = false;
    series.tooltipText = "Produkcja w {categoryX}: [bold]{valueY}[/]";

    let series2 = chart.series.push(new am4charts.LineSeries());
    series2.dataFields.valueY = "travel";
    series2.dataFields.categoryX = "date_start";
    series2.clustered = false;
    series2.stroke = am4core.color("#ff0000"); // red
    series2.strokeWidth = 3; // 3px
    series2.tooltipText = "Transport w {categoryX}: [bold]{valueY}[/]";

    let series3 = chart.series.push(new am4charts.LineSeries());
    series3.dataFields.valueY = "production_without_correction";
    series3.dataFields.categoryX = "date_start";
    series3.clustered = false;
    series3.stroke = am4core.color("#0000ff"); // red
    series3.strokeWidth = 3; // 3px
    series3.tooltipText = "Produckja bez korekty w {categoryX}: [bold]{valueY}[/]";

    //production_without_correction

    // let series3 = chart.series.push(new am4charts.ColumnSeries());
    // series3.dataFields.valueY = "planned";
    // series3.dataFields.categoryX = "date";
    // series3.clustered = false;
    // series3.columns.template.width = am4core.percent(50);
    // series3.tooltipText = "Planowane w {categoryX}: [bold]{valueY}[/]";

    chart.cursor = new am4charts.XYCursor();
    // chart.cursor.lineX.disabled = true;
    // chart.cursor.lineY.disabled = true;
};