$('.datepicker').datepicker({
    'format': 'dd-mm-yyyy'
});

const $filters = $("#report_filters");
const $report = $("#report");

let lastFormData;

$("#kt_generate").on('click', function () {
    $filters.submit();
});

$filters.on('submit', function (e) {
    e.preventDefault();

    lastFormData = $(this).serialize();

    KTApp.blockPage();

    $.ajax({
       method: "POST",
       url: "/report/car/list",
       data: lastFormData
    }).always(function () {
        KTApp.unblockPage();
    }).done(function (response) {
        $report.html(response);
    });
});

$report.on('click', '.show-full', function () {
    let mainTr = $(this).closest('tr');
    let productId = $(this).data('product-id');

    let $this = $(this);

    KTApp.blockPage();

    $.ajax({
        method: "GET",
        url: "/report/car/json",
        data: lastFormData + "&product_id=" + productId
    }).always(function () {
        KTApp.unblockPage();
    }).done(function (response) {
        mainTr.after("<tr><td colspan='4'></td></tr>");

        let chart = am4core.create(mainTr.next().find('td')[0], am4charts.XYChart);
        chart.data = response;

        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "date";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = "Ilość sztuk";
        valueAxis.title.fontWeight = 800;

        // Create series
        let series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = "scanned";
        series.dataFields.categoryX = "date";
        series.clustered = false;
        series.tooltipText = "Trasa w {categoryX}: [bold]{valueY}[/]";

        let series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.dataFields.valueY = "planned";
        series2.dataFields.categoryX = "date";
        series2.clustered = false;
        series2.columns.template.width = am4core.percent(50);
        series2.tooltipText = "Planowane w {categoryX}: [bold]{valueY}[/]";

        // let series3 = chart.series.push(new am4charts.ColumnSeries());
        // series3.dataFields.valueY = "planned";
        // series3.dataFields.categoryX = "date";
        // series3.clustered = false;
        // series3.columns.template.width = am4core.percent(50);
        // series3.tooltipText = "Planowane w {categoryX}: [bold]{valueY}[/]";

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        $this.remove();
    });
});