$(document).ready(function () {
    setInterval(function () {
        //active-devices
        $.ajax({
            url: '/active-devices'
        }).done(function (response) {
            $('#active-devices').html(response);
        });

        //devices
        $.ajax({
            url: '/devices'
        }).done(function (response) {
            $('#devices-table-body').html(response);
        });
    }, 5000);
});
