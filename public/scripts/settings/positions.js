let $editModal = $('#edit-modal');
let $tableContainer = $('#positions-list-container');

$('#show-insert-modal').on('click', function () {
    loadModal();
});

$('#submit-modal-form').on('click', function (e) {
    e.preventDefault();

    $editModal.find('form').submit();
});

$tableContainer.on('click', '.position-activate', function (e) {
    e.preventDefault();
    let positionId = $(this).closest('tr').data('positionId');
    changeVisibility(positionId, true);
});

$tableContainer.on('click', '.position-deactivate', function (e) {
    e.preventDefault();
    let positionId = $(this).closest('tr').data('positionId');
    changeVisibility(positionId, false);
});

/*
FORM SUBMIT
 */
$editModal.on('submit', '#positions-form', function (e) {
    e.preventDefault();
    KTApp.blockPage();

    let data = $(this).serialize();
    $editModal.modal('hide');

    let method = 'POST';
    let url = '/settings/positions';

    if ($(this).data('position-id') > 0) {
        method = 'PUT';
        url = '/settings/positions/' + $(this).data('position-id');
    }

    $.ajax({
        method: method,
        url: url,
        data: data
    }).always(function () {
        KTApp.unblockPage();
    }).done(function () {
        toastr.success('Nowe stanowisko zostało dodane!');
        reloadTable();
    }).fail(function () {
        toastr.error('Wystąpił błąd podczas dodawania stanowiska!');
    });
});

/*
EDIT POSITION
 */
$tableContainer.on('click', '.position-edit-button', function (e) {
    e.preventDefault();

    let positionId = $(this).closest('tr').data('position-id');
    loadModal(positionId);
});

/*
EDIT VISIBILITY
*/
changeVisibility = (positionId, isVisible) => {
    KTApp.blockPage();

    $.ajax({
        'method': 'PUT',
        'url': '/settings/positions/' + positionId + '/visible',
        'data': 'visible=' + isVisible
    }).always(function () {
        KTApp.unblockPage();
    }).done(function () {
        toastr.success('Zmiana wprowadzona pomyślnie!');
        reloadTable();
    }).fail(function () {
        toastr.error('Wystąpił błąd podczas wykonowania zmiany!');
    });
};


/*
LOAD MODAL
 */
loadModal = (positionId) => {
    if (typeof positionId === 'undefined') {
        positionId = 0;
    }


    KTApp.blockPage();

    $.ajax({
        'method': 'get',
        'url': '/settings/positions/modal/' + positionId
    }).always(function () {
        KTApp.unblockPage();
    }).done(function (response) {
        $editModal.find('.modal-body').html(response);
        $editModal.modal('show');
    }).fail(function () {
        toastr.error('Wystąpił błąd podczas ładowania!');
    });
};

/*
RELOAD TABLE
 */
reloadTable = () => {
    KTApp.block($tableContainer);

    $.ajax({
        method: 'GET',
        url: '/settings/positions/list'
    }).always(function () {
        KTApp.unblock($tableContainer);
    }).done(function (response) {
        $tableContainer.html(response);
    }).fail(function () {
        toastr.error('Wystąpił błąd podczas odświerzania listy stanowisk!');
    });
};