let $editModal = $('#edit-modal');
let $tableContainer = $('#cars-list-container');

$('#show-insert-modal').on('click', function () {
    loadModal();
});

$('#submit-modal-form').on('click', function (e) {
    e.preventDefault();

    $editModal.find('form').submit();
});

$tableContainer.on('click', '.car-activate', function (e) {
    e.preventDefault();
    let carId = $(this).closest('tr').data('carId');
    changeVisibility(carId, true);
});

$tableContainer.on('click', '.car-deactivate', function (e) {
    e.preventDefault();
    let carId = $(this).closest('tr').data('carId');
    changeVisibility(carId, false);
});

/*
FORM SUBMIT
 */
$editModal.on('submit', '#cars-form', function (e) {
    e.preventDefault();
    KTApp.blockPage();

    let data = $(this).serialize();
    $editModal.modal('hide');

    let method = 'POST';
    let url = '/settings/cars';

    if ($(this).data('car-id') > 0) {
        method = 'PUT';
        url = '/settings/cars/' + $(this).data('car-id');
    }

    $.ajax({
        method: method,
        url: url,
        data: data
    }).always(function () {
        KTApp.unblockPage();
    }).done(function () {
        toastr.success('Nowe stanowisko zostało dodane!');
        reloadTable();
    }).fail(function () {
        toastr.error('Wystąpił błąd podczas dodawania stanowiska!');
    });
});

/*
EDIT car
 */
$tableContainer.on('click', '.car-edit-button', function (e) {
    e.preventDefault();

    let carId = $(this).closest('tr').data('car-id');
    loadModal(carId);
});

/*
EDIT VISIBILITY
*/
changeVisibility = (carId, isVisible) => {
    KTApp.blockPage();

    $.ajax({
        'method': 'PUT',
        'url': '/settings/cars/' + carId + '/visible',
        'data': 'visible=' + isVisible
    }).always(function () {
        KTApp.unblockPage();
    }).done(function () {
        toastr.success('Zmiana wprowadzona pomyślnie!');
        reloadTable();
    }).fail(function () {
        toastr.error('Wystąpił błąd podczas wykonowania zmiany!');
    });
};


/*
LOAD MODAL
 */
loadModal = (carId) => {
    if (typeof carId === 'undefined') {
        carId = 0;
    }


    KTApp.blockPage();

    $.ajax({
        'method': 'get',
        'url': '/settings/cars/modal/' + carId
    }).always(function () {
        KTApp.unblockPage();
    }).done(function (response) {
        $editModal.find('.modal-body').html(response);
        $editModal.modal('show');
    }).fail(function () {
        toastr.error('Wystąpił błąd podczas ładowania!');
    });
};

/*
RELOAD TABLE
 */
reloadTable = () => {
    KTApp.block($tableContainer);

    $.ajax({
        method: 'GET',
        url: '/settings/cars/list'
    }).always(function () {
        KTApp.unblock($tableContainer);
    }).done(function (response) {
        $tableContainer.html(response);
    }).fail(function () {
        toastr.error('Wystąpił błąd podczas odświerzania listy stanowisk!');
    });
};