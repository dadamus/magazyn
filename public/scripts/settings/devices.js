let $editModal = $('#edit-modal');
let $tableContainer = $('#devices-list-container');

$('#show-insert-modal').on('click', function () {
    loadModal();
});


$('#submit-modal-form').on('click', function (e) {
    e.preventDefault();

    $editModal.find('form').submit();
});

/*
EDIT POSITION
 */
$tableContainer.on('click', '.device-edit-button', function (e) {
    e.preventDefault();

    let deviceId = $(this).closest('tr').data('device-id');
    loadModal(deviceId);
});


/*
FORM SUBMIT
 */
$editModal.on('submit', '#devices-form', function (e) {
    e.preventDefault();
    KTApp.blockPage();

    let data = $(this).serialize();
    $editModal.modal('hide');

    let method = 'POST';
    let url = '/settings/devices';

    if ($(this).data('device-id') > 0) {
        method = 'PUT';
        url = '/settings/devices/' + $(this).data('device-id');
    }

    $.ajax({
        method: method,
        url: url,
        data: data
    }).always(function () {
        KTApp.unblockPage();
    }).done(function () {
        toastr.success('Nowe stanowisko zostało dodane!');
        reloadTable();
    }).fail(function () {
        toastr.error('Wystąpił błąd podczas dodawania stanowiska!');
    });
});

/*
LOAD MODAL
 */
loadModal = (deviceId) => {
    if (typeof deviceId === 'undefined') {
        deviceId = 0;
    }

    KTApp.blockPage();

    $.ajax({
        'method': 'get',
        'url': '/settings/devices/modal/' + deviceId
    }).always(function () {
        KTApp.unblockPage();
    }).done(function (response) {
        $editModal.find('.modal-body').html(response);
        $editModal.modal('show');
    }).fail(function () {
        toastr.error('Wystąpił błąd podczas ładowania!');
    });
};

/*
RELOAD TABLE
 */
reloadTable = () => {
    KTApp.block($tableContainer);

    $.ajax({
        method: 'GET',
        url: '/settings/devices/list'
    }).always(function () {
        KTApp.unblock($tableContainer);
    }).done(function (response) {
        $tableContainer.html(response);
    }).fail(function () {
        toastr.error('Wystąpił błąd podczas odświerzania listy stanowisk!');
    });
};