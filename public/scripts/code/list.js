let $filters = $('#kt_filters');
let $editModal = $('#edit-modal');

$('.datepicker').datepicker({
    'format': 'dd-mm-yyyy'
});

let table = $('#code_list_table').DataTable({
    "processing": true,
    "serverSide": true,
    "searching": false,
    "ajax": {
        "url": "/codes/list/filter",
        "type": "POST",
        "data": function (data) {
            $('.kt-input').each(function () {
                let colIndex = $(this).data('col-index');
                data.columns[colIndex]['search']['value'] = $(this).val();
                data.columns[colIndex]['search']['type'] = $(this).data('filter-type');
            });
        }
    },
    "columnDefs": [
        {
            "targets": 0,
            "data": "id"
        },
        {
            "targets": 1,
            "data": "ean"
        },
        {
            "targets": 2,
            "data": "code_type"
        },
        {
            "targets": 3,
            "data": "product_name"
        },
        {
            "targets": 4,
            "data": "production_date",
            "render": function (data) {
                if (data === null) {
                    return '-';
                }

                return data;
            }
        },
        {
            "targets": 5,
            "data": "travel_date",
            "render": function (data) {
                if (data === null) {
                    return '-';
                }

                return data;
            }
        },
        {
            "targets": 6,
            "data": "created_at"
        },
        {
            "targets": 7,
            "data": "production_place_id",
            "visible": false
        },
        {
            "targets": 8,
            "data": "travel_place_id",
            "visible": false
        },
        {
            "targets": 9,
            "data": "product_id",
            "visible": false
        }
    ]
});

$("#product-search").select2({
    width: "100%",
    minimumInputLength: 3,
    placeholder: 'Wyszukaj produkt',
    ajax: {
        url: '/product/find'
    }
});

let resetFilters = () => {
    $filters.find('input').each(function () {
        $(this).val('');
    });

    console.log($filters.find('select'));

    $filters.find('select').val('');
};

$(document).on('click', '.code-edit-button', function (e) {
    let codeId = $(this).data('code-id');

    loadModal(codeId);
});

$('#kt_search').on('click', function (e) {
    e.preventDefault();

    table.table().draw();
});

$('#kt_reset').on('click', function (e) {
    e.preventDefault();

    resetFilters();

    table.table().draw();
});

$('#add-new-code').on('click', function (e) {
    e.preventDefault();

    loadModal();
});

$('#submit-modal-form').on('click', function (e) {
    e.preventDefault();

    $editModal.find('form').submit();
});
