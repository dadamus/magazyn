let $filters = $('#kt_filters');
let $editModal = $('#edit-modal');

let table = $('#product_list_table').DataTable({
    "processing": true,
    "serverSide": true,
    "searching": false,
    "ajax": {
        "url": "/products/list/filter",
        "type": "POST",
        "data": function (data) {
            $('.kt-input').each(function () {
                let colIndex = $(this).data('col-index');
                data.columns[colIndex]['search']['value'] = $(this).val();
                data.columns[colIndex]['search']['type'] = $(this).data('filter-type');
            });
        }
    },
    "columnDefs": [
        {
            "targets": 0,
            "data": "id"
        },
        {
            "targets": 1,
            "data": "name",
            "render": function (data, type, full) {
                return `
                    <a href="/product/card/` + full.id + `">` + data + `</a>
                `;
            }
        },
        {
            "targets": 2,
            "data": "position_name"
        },
        {
            "targets": 3,
            "data": "category_name"
        },
        {
            "targets": 4,
            "data": "type_name"
        },
        {
            "targets": 5,
            "orderable": false,
            "render": function (data, type, full) {
                return `
                    <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-outline-danger dropdown-toggle"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Akcje
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" x-placement="top-end">
                        <button class="dropdown-item product-edit-button" data-product-id="` + full.id + `" type="button">Edytuj</button>
                    </div>
                </div>
                `;
            }
        },
        {
            "data": "position_id",
            "visible": false,
            "targets": 6
        },
        {
            "data": "category_id",
            "visible": false,
            "targets": 7
        },
        {
            "data": "type_id",
            "visible": false,
            "targets": 8
        }
    ]
});

let resetFilters = () => {
    $filters.find('input').each(function () {
        $(this).val('');
    });

    $filters.find('select').each(function () {
        $(this).val('');
    });
};

$(document).on('click', '.product-edit-button', function (e) {
    let productId = $(this).data('product-id');

    loadModal(productId);
});

$('#kt_search').on('click', function (e) {
    e.preventDefault();

    table.table().draw();
});

$('#kt_reset').on('click', function (e) {
    e.preventDefault();

    resetFilters();

    table.table().draw();
});

$('#add-new-product').on('click', function (e) {
    e.preventDefault();

    loadModal();
});

$('#submit-modal-form').on('click', function (e) {
    e.preventDefault();

    $editModal.find('form').submit();
});

/*
FORM SUBMIT
 */
$editModal.on('submit', '#products-form', function (e) {
    e.preventDefault();
    KTApp.blockPage();

    let data = $(this).serialize();
    $editModal.modal('hide');

    let method = 'POST';
    let url = '/products';

    if ($(this).data('product-id') > 0) {
        method = 'PUT';
        url = '/products/' + $(this).data('product-id');
    }

    $.ajax({
        method: method,
        url: url,
        data: data
    }).always(function () {
        KTApp.unblockPage();
    }).done(function () {
        toastr.success('Nowy produkt został dodany!');

        table.table().draw();
    }).fail(function () {
        toastr.error('Wystąpił błąd podczas dodawania stanowiska!');
    });
});

/*
LOAD MODAL
 */
loadModal = (productId) => {
    if (typeof productId === 'undefined') {
        productId = 0;
    }


    KTApp.blockPage();

    $.ajax({
        'method': 'get',
        'url': '/products/modal/' + productId
    }).always(function () {
        KTApp.unblockPage();
    }).done(function (response) {
        $editModal.find('.modal-body').html(response);
        $editModal.modal('show');
    }).fail(function () {
        toastr.error('Wystąpił błąd podczas ładowania!');
    });
};