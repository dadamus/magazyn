let $filters = $('#kt_filters');
const $generateCodeForm = $("#generate-code-form");
const $generateModal = $("#generate-modal");
const $correctionModal = $("#correction-modal");

const productId = $("#product_state_log_table").data('product-id');

const table = $("#product_state_log_table").DataTable({
    "processing": true,
    "serverSide": true,
    "searching": false,
    "ajax": {
        "url": "/product/card/" + productId + "/state",
        "type": "POST",
        "data": function (data) {
            $('.kt-input').each(function () {
                let colIndex = $(this).data('col-index');
                data.columns[colIndex]['search']['value'] = $(this).val();
                data.columns[colIndex]['search']['type'] = $(this).data('filter-type');
            });
        }
    },
    "columnDefs": [
        {
            "targets": 0,
            "data": "code_id"
        },
        {
            "targets": 1,
            "data": "type_name",
        },
        {
            "targets": 2,
            "data": "device_name"
        },
        {
            "targets": 3,
            "data": "date"
        },
        {
            "targets": 4,
            "render": function (data, type, full) {
                switch (parseInt(full.type_id)) {
                    //Production
                    case 1:
                        return `Stanowisko: ` + full.code_position_name;

                    //Travel
                    case 2:
                        return '-';
                        break;

                    case 4:
                        return '-';

                    //Correction
                    default:
                        return '-';
                        break;
                }
            }
        },
        {
            "targets": 5,
            "data": "device_id",
            "visible": false
        },
        {
            "targets": 6,
            "data": "code_position_id",
            "visible": false
        }
    ]
});

let resetFilters = () => {
    $filters.find('input').each(function () {
        $(this).val('');
    });

    $filters.find('select').each(function () {
        $(this).val('');
    });
};


$('#kt_search').on('click', function (e) {
    e.preventDefault();

    table.table().draw();
});

$('#kt_reset').on('click', function (e) {
    e.preventDefault();

    resetFilters();

    table.table().draw();
});

$('#show-generate-codes').on('click', function () {
    $generateModal.modal('show');
});

$('#show-correction').on('click', function () {
    $correctionModal.modal('show');
});

$('#submit-positive-correction').on('click', function (e) {
    e.preventDefault();

    let correctionValue = $('#correction-value').val();

    if (correctionValue < 0) {
        toastr.error('Wartość korekty musi być dodatnia!');
        return;
    }

    submitCorrection(correctionValue);
});

$('#submit-negative-correction').on('click', function (e) {
    e.preventDefault();

    let correctionValue = $('#correction-value').val();

    if (correctionValue < 0) {
        toastr.error('Wartość korekty musi być dodatnia!');
        return;
    }

    submitCorrection(0 - correctionValue);
});

const submitCorrection = (value) => {
    KTApp.blockPage();

    $.ajax({
        method: 'POST',
        url: '/product/card/' + productId + '/correction',
        data: 'correction-value=' + value
    }).always(function () {
        KTApp.unblockPage();
    }).done(function () {
        toastr.success("Korekta została dodana!");
    }).fail(function () {
        toastr.error("Wystąpił błąd podczas dodawania korekty!");
    });
};

$('#submit-generate-codes').on('click', function () {
    $generateCodeForm.submit();
});

$generateCodeForm.on('submit', function (e) {
    e.preventDefault();

    let productId = $(this).data('product-id');

    KTApp.blockPage();
    $generateModal.modal('hide');

    let data = $(this).serialize();

    $.ajax({
        method: 'POST',
        url: '/product/card/' + productId + '/generate',
        data: data
    }).always(function () {
        KTApp.unblockPage();
    }).done(function (response) {
        toastr.success("Kody zostały wygenerowane!");

        let data = JSON.parse(response);

        if (!data.ipPrinterEnabled) {
            let win = window.open('/print/codes/' + data.printerGroupId, '_blank');
            win.focus();
        }
    }).fail(function () {
        toastr.error("Wystąpił błąd podczas generowania kodów!");
    });
});
