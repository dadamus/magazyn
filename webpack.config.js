const Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('public/build/')
    // // public path used by the web server to access the output path
    .setPublicPath('/build')
    //
    .addEntry('app', './assets/index.js')

    .enableReactPreset()
    .enableVersioning()

    .enableSingleRuntimeChunk()
;

module.exports = Encore.getWebpackConfig();
