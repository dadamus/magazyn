create table car
(
    id      int auto_increment
        primary key,
    plate   varchar(10)          not null,
    visible tinyint(1) default 1 null
)
    collate = utf8_unicode_ci;

create table category
(
    id      int auto_increment
        primary key,
    name    varchar(255)         null,
    visible tinyint(1) default 1 null,
    constraint category_name_idx
        unique (name)
)
    collate = utf8_unicode_ci;

INSERT INTO warehouse.category (id, name, visible) VALUES (1, 'test', 1);

create table code
(
    id         int auto_increment
        primary key,
    ean        varchar(13) null,
    type_id    int         null,
    created_at datetime    null,
    created_by int         null,
    constraint code_code_type_id_fk
        foreign key (type_id) references code_type (id),
    constraint code_user_id_fk
        foreign key (created_by) references user (id)
);

create table code_type
(
    id   int auto_increment
        primary key,
    name varchar(32) null
);

INSERT INTO warehouse.code_type (id, name) VALUES (1, 'produkcja');
INSERT INTO warehouse.code_type (id, name) VALUES (2, 'trasa');
INSERT INTO warehouse.code_type (id, name) VALUES (3, 'stop');

create table device
(
    id             int auto_increment
        primary key,
    name           varchar(255)         not null,
    type_id        int                  not null,
    active         tinyint(1) default 1 null,
    position_id    int                  not null,
    hash           varchar(255)         not null,
    last_sync      datetime             null,
    last_travel_id int                  null,
    last_ip        varchar(12)          null,
    constraint device_device_type_id_fk
        foreign key (type_id) references device_type (id),
    constraint device_position_id_fk
        foreign key (position_id) references position (id)
)
    collate = utf8_unicode_ci;

create table device_type
(
    id   int auto_increment
        primary key,
    name varchar(128) not null
)
    collate = utf8_unicode_ci;

INSERT INTO warehouse.device_type (id, name) VALUES (1, 'produkcja');
INSERT INTO warehouse.device_type (id, name) VALUES (2, 'trasa');

create table event_streams
(
    no               bigint auto_increment
        primary key,
    real_stream_name varchar(150) not null,
    stream_name      char(41)     not null,
    metadata         json         null,
    category         varchar(150) null,
    constraint ix_rsn
        unique (real_stream_name)
)
    collate = utf8_bin;

create index ix_cat
    on event_streams (category);

create table `group`
(
    id   int auto_increment
        primary key,
    name varchar(128) null,
    constraint group_name_uindex
        unique (name)
)
    collate = utf8_unicode_ci;

INSERT INTO warehouse.`group` (id, name) VALUES (1, 'user');
INSERT INTO warehouse.`group` (id, name) VALUES (2, 'visioner');

create table position
(
    id          int auto_increment
        primary key,
    name        varchar(255)         not null,
    description mediumtext           null,
    visible     tinyint(1) default 1 null
)
    collate = utf8_unicode_ci;

INSERT INTO warehouse.position (id, name, description, visible) VALUES (1, 'test', '', 1);

create table product
(
    id          int auto_increment
        primary key,
    name        varchar(255) null,
    description mediumtext   null,
    category_id int          null,
    type_id     int          null,
    constraint product_category_id_fk
        foreign key (category_id) references category (id),
    constraint product_type_id_fk
        foreign key (type_id) references type (id)
)
    collate = utf8_unicode_ci;

create table product_code_view
(
    code_id               int      not null,
    product_id            int      null,
    production_scanned_at datetime null,
    production_device_id  int      null,
    travel_scanned_at     datetime null,
    travel_device_id      int      null,
    travel_id             int      null,
    constraint product_code_view_code_id_uindex
        unique (code_id),
    constraint product_code_view_code_id_fk
        foreign key (code_id) references code (id),
    constraint product_code_view_device_id_fk
        foreign key (production_device_id) references device (id),
    constraint product_code_view_device_id_fk_2
        foreign key (travel_device_id) references device (id),
    constraint product_code_view_product_id_fk
        foreign key (product_id) references product (id),
    constraint product_code_view_travel_id_fk
        foreign key (travel_id) references travel (id)
);

alter table product_code_view
    add primary key (code_id);

create table product_position
(
    product_id  int not null,
    position_id int not null,
    constraint product_position_position_id_fk
        foreign key (position_id) references position (id),
    constraint product_position_product_id_fk
        foreign key (product_id) references product (id)
);

create table product_state_log
(
    product_id int      null,
    code_id    int      null,
    device_id  int      null,
    amount     int      null,
    date       datetime null,
    constraint code_product_logs_code_id_fk
        foreign key (code_id) references code (id),
    constraint code_product_logs_device_id_fk
        foreign key (device_id) references device (id),
    constraint product_state_log_product_id_fk
        foreign key (product_id) references product (id)
);


create table projections
(
    no           bigint auto_increment
        primary key,
    name         varchar(150) not null,
    position     json         null,
    state        json         null,
    status       varchar(28)  not null,
    locked_until char(26)     null,
    constraint ix_name
        unique (name)
)
    collate = utf8_bin;


create table travel
(
    id          int auto_increment
        primary key,
    name        varchar(255) null,
    description mediumtext   null,
    author_id   int          not null,
    car_id      int          null,
    started_at  datetime     null,
    ended_at    datetime     null,
    created_at  datetime     null,
    modified_at datetime     null,
    modified_by int          null,
    constraint travel_car_id_fk
        foreign key (car_id) references car (id),
    constraint travel_user_id_fk
        foreign key (author_id) references user (id),
    constraint travel_user_id_fk_2
        foreign key (modified_by) references user (id)
)
    collate = utf8_unicode_ci;

create table travel_code
(
    travel_id int not null,
    code_id   int not null,
    constraint travel_code_code_id_uindex
        unique (code_id),
    constraint travel_code_travel_id_uindex
        unique (travel_id),
    constraint travel_code_code_id_fk
        foreign key (code_id) references code (id),
    constraint travel_code_travel_id_fk
        foreign key (travel_id) references travel (id)
);

create table travel_product
(
    travel_id  int null,
    product_id int null,
    planned    int null,
    constraint travel_product_product_id_fk
        foreign key (product_id) references product (id),
    constraint travel_product_travel_id_fk
        foreign key (travel_id) references travel (id)
);

create table travel_report
(
    travel_id   int                  null,
    reported    tinyint(1) default 0 null,
    text        varchar(255)         null,
    modified_at datetime             null,
    modified_by int                  null,
    constraint travel_report_travel_id_fk
        foreign key (travel_id) references travel (id),
    constraint travel_report_user_id_fk
        foreign key (modified_by) references user (id)
);


create table type
(
    id      int auto_increment
        primary key,
    name    varchar(255)         null,
    visible tinyint(1) default 1 null,
    constraint type_name_idx
        unique (name)
)
    collate = utf8_unicode_ci;

INSERT INTO warehouse.type (id, name, visible) VALUES (1, 'test', 1);
create table user
(
    id       int auto_increment
        primary key,
    login    varchar(255) not null,
    password varchar(255) not null,
    name     varchar(255) null
)
    collate = utf8_unicode_ci;

INSERT INTO warehouse.user (id, login, password, name) VALUES (1, 'root', '63a9f0ea7bb98050796b649e85481845', 'Admin');
create table user_group
(
    user_id  int null,
    group_id int null,
    constraint user_group_group_id_fk
        foreign key (group_id) references `group` (id),
    constraint user_group_user_id_fk
        foreign key (user_id) references user (id)
)
    collate = utf8_unicode_ci;

INSERT INTO warehouse.user_group (user_id, group_id) VALUES (1, 1);
