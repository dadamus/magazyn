<?php

declare(strict_types=1);

namespace ERP\Tests\Domain\CodeScanner\Handler;

use ERP\Code\Domain\ProductCode;
use ERP\Code\Domain\CodeType;
use ERP\Code\Domain\ProductCode;
use ERP\Code\Domain\View\ProductCodeView;
use ERP\Code\Infrastructure\ProductCodeRepositoryInterface;
use ERP\Code\Infrastructure\ProductCodeViewRepositoryInterface;
use ERP\ScannerConsumer\Domain\BlinkerResponseInterface;
use ERP\ScannerConsumer\Domain\Handler\ProductCodeOnProductionDevice;
use ERP\ScannerConsumer\Domain\ResponseBlinker\DefaultResponseBlinker;
use ERP\ScannerConsumer\Infrastructure\DeviceMessengersInterface;
use ERP\Settings\Domain\Device\Device;
use ERP\Settings\Domain\Device\DeviceType;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductCodeOnProductionDeviceTest extends WebTestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    public function testClassSupportsProductCodeOnProductionDevice()
    {
        $codeProduct = ProductCode::create(1);
        $handler = $this->handlerProvider($codeProduct);
        $device = $this->deviceProvider(DeviceType::PRODUCTION());

        $supportResponse = $handler->support(CodeType::PRODUCTION(), $device);
        $this->assertTrue($supportResponse);
    }

    public function testClassDoesNotSupportTravelCodeOnProductionDevice()
    {
        $codeProduct = ProductCode::create(1);
        $handler = $this->handlerProvider($codeProduct);
        $device = $this->deviceProvider(DeviceType::PRODUCTION());

        $supportResponse = $handler->support(CodeType::TRAVEL(), $device);
        $this->assertFalse($supportResponse);
    }

    public function testClassDoesNotSupportProductionCodeOnTravelDevice()
    {
        $codeProduct = ProductCode::create(1);
        $handler = $this->handlerProvider($codeProduct);
        $device = $this->deviceProvider(DeviceType::PRODUCTION());

        $supportResponse = $handler->support(CodeType::TRAVEL(), $device);
        $this->assertFalse($supportResponse);
    }

    public function testClassDoesNotSupportTravelCodeOnTravelDevice()
    {
        $codeProduct = ProductCode::create(1);
        $handler = $this->handlerProvider($codeProduct);
        $device = $this->deviceProvider(DeviceType::TRAVEL());

        $supportResponse = $handler->support(CodeType::TRAVEL(), $device);
        $this->assertFalse($supportResponse);
    }

    public function testSuccessfulHandledCode()
    {
        $codeProduct = ProductCode::create(1);
        $handler = $this->handlerProvider($codeProduct);
        $device = $this->deviceProvider(DeviceType::PRODUCTION());

        $code = new ProductCode([
            ProductCode::ID_FIELD => '1',
            ProductCode::EAN_FIELD => 'test',
            ProductCode::TYPE_FIELD => CodeType::PRODUCTION
        ]);

        $handler->handle($code, $device, new \DateTimeImmutable());

        $this->assertEquals(1, $codeProduct->getScannedOnProduction());
    }

    public function deviceProvider(DeviceType $deviceType): Device
    {
        return new Device([
            Device::ID_FIELD => 1,
            Device::HASH_FIELD => 'test',
            Device::DEVICE_TYPE_ID_FIELD => $deviceType->getValue(),
            Device::LAST_TRAVEL_ID_FIELD => 0,
            Device::NAME_FIELD => 'name',
            Device::POSITION_ID_FIELD => 1
        ]);
    }

    public function handlerProvider(ProductCode $codeProduct): ProductCodeOnProductionDevice
    {
        $codeProducts = $this->getMockBuilder(ProductCodeRepositoryInterface::class)->getMock();

        $codeProducts->method('get')->willReturn($codeProduct);

        $productCodesView = $this->getMockBuilder(ProductCodeViewRepositoryInterface::class)->getMock();

        $codeViewData = new ProductCodeView([
            ProductCodeView::CODE_ID_FIELD => 1,
            ProductCodeView::POSITION_ID_FIELD => 1,
        ]);
        $productCodesView->method('getByCodeId')->willReturn($codeViewData);

        $deviceMessengers = $this->getMockBuilder(DeviceMessengersInterface::class)->getMock();
        $responseBlinker = $this->getMockBuilder(DefaultResponseBlinker::class)
                                ->setConstructorArgs([$deviceMessengers])
                                ->getMock();

        return new ProductCodeOnProductionDevice($codeProducts, $productCodesView, $responseBlinker);
    }
}
