<?php

declare(strict_types=1);

namespace ERP\Tests\Domain\CodeScanner\Handler;

use ERP\Code\Domain\CodeType;
use ERP\Code\Infrastructure\TravelCodeViewRepositoryInterface;
use ERP\ScannerConsumer\Domain\Handler\TravelCode;
use ERP\ScannerConsumer\Domain\ResponseBlinker\DefaultResponseBlinker;
use ERP\ScannerConsumer\Infrastructure\DeviceMessengersInterface;
use ERP\Settings\Domain\Device\Device;
use ERP\Settings\Domain\Device\DeviceType;
use ERP\Settings\Infrastructure\DeviceRepositoryInterface;
use PHPUnit\Framework\TestCase;

class TravelCodeTest extends TestCase
{
    public function testClassSupportsTravelCodeOnTravelDevice()
    {
        $handler = $this->handlerProvider();
        $device = $this->deviceProvider(DeviceType::TRAVEL());

        $supportResponse = $handler->support(CodeType::TRAVEL(), $device);
        $this->assertTrue($supportResponse);
    }

    public function handlerProvider(): TravelCode
    {
        $travelCodesViews = $this->getMockBuilder(TravelCodeViewRepositoryInterface::class)->getMock();
        $deviceMessengers = $this->getMockBuilder(DeviceMessengersInterface::class)->getMock();
        $responseBlinker = $this->getMockBuilder(DefaultResponseBlinker::class)
                                ->setConstructorArgs([$deviceMessengers])
                                ->getMock();

        $devices = $this->getMockBuilder(DeviceRepositoryInterface::class)->getMock();

        return new TravelCode($travelCodesViews, $devices, $responseBlinker);
    }

    public function deviceProvider(DeviceType $deviceType): Device
    {
        return new Device([
            Device::ID_FIELD => 1,
            Device::HASH_FIELD => 'test',
            Device::DEVICE_TYPE_ID_FIELD => $deviceType->getValue(),
            Device::LAST_TRAVEL_ID_FIELD => 1,
            Device::NAME_FIELD => 'name',
            Device::POSITION_ID_FIELD => 1
        ]);
    }
}
