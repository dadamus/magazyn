<?php

declare(strict_types=1);

namespace ERP\Tests\Report\Domain\Builder;

use ERP\Report\Domain\Builder\Daily;
use ERP\Report\Domain\Data\DateRange;
use ERP\Report\Infrastructure\Daily\InMemory;
use PHPUnit\Framework\TestCase;
use ERP\Report\Domain\Builder\Daily\Day as DayBuilder;
use ERP\Report\Domain\Builder\Daily\Made as MadeBuilder;
use ERP\Report\Domain\Builder\Daily\Performance as PerformanceBuilder;
use ERP\Report\Domain\Builder\Daily\Charged as ChargedBuilder;
use ERP\Report\Domain\Builder\Daily\Balance as BalanceBuilder;
use ERP\Report\Domain\Builder\Daily\Day\Performance as DayPerformanceBuilder;
use ERP\Report\Domain\Builder\Daily\Day\Balance as DayBalanceBuilder;
use ERP\Report\Domain\Builder\Daily\Day\Change as DayChangeBuilder;
use ERP\Report\Domain\Builder\Daily\Day\Charged as DayChargedBuilder;
use ERP\Report\Domain\Builder\Daily\Day\Made as DayMadeBuilder;
use ERP\Report\Domain\Builder\Daily\Day\Record as DayRecordBuilder;

class DailyTest extends TestCase
{
    public function testBuild(): void
    {
        $dailyBuilder = $this->mockDayChangeBuilder([
            [
                'production_scanned_at' => '2022-02-02 10:11:00',
                'travel_scanned_at' => '2022-02-02 13:20:00',
                'type_id' => 1
            ],
            [
                'production_scanned_at' => '2022-02-02 15:11:00',
                'travel_scanned_at' => '2022-02-03 13:20:00',
                'type_id' => 1
            ],
            [
                'production_scanned_at' => '2022-02-02 14:11:00',
                'travel_scanned_at' => '2022-02-03 13:20:00',
                'type_id' => 1
            ],
            [
                'production_scanned_at' => '2022-02-03 10:11:00',
                'travel_scanned_at' => '2022-02-03 13:20:00',
                'type_id' => 1
            ],
            [
                'production_scanned_at' => '2022-02-02 10:11:00',
                'travel_scanned_at' => '2022-02-04 13:20:00',
                'type_id' => 1
            ]
        ]);

        $data = $dailyBuilder->build(
            new DateRange(
                new \DateTimeImmutable('2022-02-02'),
                new \DateTimeImmutable('2022-02-05')
            ),
            1
        );

        var_dump($data);
    }

    private function mockDailyBuilder(array $db): Daily
    {
        $madeBuilder = new MadeBuilder();
        $performanceBuilder = new PerformanceBuilder();
        $chargedBuilder = new ChargedBuilder();
        $balanceBuilder = new BalanceBuilder();

        return new Daily(
            $this->mockDayBuilder($db),
            $madeBuilder,
            $performanceBuilder,
            $chargedBuilder,
            $balanceBuilder
        );
    }

    private function mockDayBuilder(array $db): DayBuilder
    {
        $madeBuilder = new DayMadeBuilder();
        $performanceBuilder = new DayPerformanceBuilder();
        $chargedBuilder = new DayChargedBuilder();
        $balanceBuilder = new DayBalanceBuilder();

        return new DayBuilder(
            $madeBuilder,
            $performanceBuilder,
            $chargedBuilder,
            $balanceBuilder,
            $this->mockDayChangeBuilder($db)
        );
    }

    private function mockDayChangeBuilder(array $db): DayChangeBuilder
    {
        $repository = new InMemory($db);
        $recordBuilder = new DayRecordBuilder();

        return new DayChangeBuilder($repository, $recordBuilder);
    }
}
