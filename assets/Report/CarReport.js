import React, {useState} from 'react';
import {Col, Row, Button, Spinner} from "react-bootstrap";
import FromToDatePicker from "../SharedKernel/Form/FromToDatePicker";
import Positions from "../SharedKernel/Form/Product/PositionsSelector";
import CarsSelector from "../SharedKernel/Form/CarsSelector";

export default function CarReport(props) {
    return <>
        <Row className="subhead">
            <div className="title">
                Raporty
                <div className="breadcrumb">
                    Auta zbiorczy
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={12}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Raport auta zbiorczy
                        </div>
                    </div>
                    <div className="portlet-body">
                        <Row>
                            <Col lg={3}>
                                <div className="form-group">
                                    <label htmlFor="name">Przedział czasowy:</label>
                                    <FromToDatePicker
                                        setFrom={value => setFilters('from', value)}
                                        setTo={value => setFilters('to', value)}
                                    />
                                </div>
                            </Col>

                            <Col lg={3}>
                                <div className="form-group">
                                    <label htmlFor="name">Auto:</label>
                                    <CarsSelector/>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={12}>
                                <div className="separator"/>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={12}>
                                <table className='table table-striped table-bordered'>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Detal</th>
                                        <th colSpan={2}>Załadowano</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            Test
                                        </td>
                                        <td>
                                            58
                                        </td>
                                        <td>
                                            58
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </Col>
                        </Row>
                    </div>
                </div>
            </Col>
        </Row>
    </>
}
