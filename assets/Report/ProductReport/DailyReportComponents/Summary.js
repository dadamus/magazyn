import React, {useState} from 'react';
import {Col, Row} from "react-bootstrap";
import './Sumary.css';
import ChartWrapper from "../../../SharedKernel/ChartWrapper";
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, ReferenceLine, ResponsiveContainer } from 'recharts';

export default function Summary(props) {
    const getDate = () => {
        const date = new Date(props.data.date.timestamp * 1000);

        let d = date.getDate();
        let m = date.getMonth() + 1; //Month from 0 to 11
        let y = date.getFullYear();

        return '' + (d <= 9 ? '0' + d : d) + '-' + (m <= 9 ? '0' + m : m) + '' + y;
    }

    const getDataForSummaryChart = () => {
        let response = [];

        props.data.days.forEach((day) => {
            day.changes.forEach((change) => {
                response.push(...change.records);
            });
        });

        return response;
    }

    return <>
        <Row className='summary'>
            <Col lg={2}>
                <Row>
                    <Col lg={12}>Wyprodukowano:</Col>
                </Row>
                <Row>
                    <Col lg={6}>
                        {props.data.made.quantity}
                    </Col>
                    <Col lg={6}>
                        {props.data.made.quantityWithCorrection}
                    </Col>
                </Row>
            </Col>
            <Col lg={2}>
                <Row>
                    <Col lg={12}>Wydajność średnia:</Col>
                </Row>
                <Row>
                    <Col lg={6}>
                        {props.data.performance.percentage}%
                    </Col>
                    <Col lg={6}>
                        {props.data.performance.percentageWithCorrection}%
                    </Col>
                </Row>
            </Col>
            <Col lg={2}>
                <Row>
                    <Col lg={12}>Załadowano:</Col>
                </Row>
                <Row>
                    <Col lg={6}>
                        {props.data.charged.quantity}
                    </Col>
                    <Col lg={6}>
                        {props.data.charged.quantityWithCorrection}
                    </Col>
                </Row>
            </Col>
            <Col lg={2}>
                <Row>
                    <Col lg={12}>Bilans:</Col>
                </Row>
                <Row>
                    <Col lg={6}>
                        {props.data.balance.quantity}
                    </Col>
                    <Col lg={6}>
                        {props.data.balance.quantityWithCorrection}
                    </Col>
                </Row>
            </Col>
        </Row>
        <Row>
            <Col lg={12}>
                <ChartWrapper>
                    <ResponsiveContainer width='100%' height='100%'>
                        <LineChart
                            width={800}
                            height={300}
                            data={getDataForSummaryChart()}
                        >
                            <CartesianGrid strokeDasharray="3 3"/>
                            <XAxis dataKey="label"/>
                            <YAxis/>
                            <Tooltip/>

                            <Line type="monotone" dataKey="productionQty" name="Produkcja" stroke="#8884d8" activeDot={{ r: 8 }} />
                            <ReferenceLine y={props.data.standard} stroke="red" strokeDasharray="3 3" />
                        </LineChart>
                    </ResponsiveContainer>
                </ChartWrapper>
            </Col>
        </Row>
    </>
}
