import React, {useState} from 'react';
import {Row, Col} from "react-bootstrap";
import {BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';
import ChartWrapper from "../../../SharedKernel/ChartWrapper";

export default function WorkChange(props) {
    return <Row>
        <Col lg={12}>
            <p style={{textAlign: 'center'}}>Zmiana {props.lp}</p>
            <ChartWrapper>
                <ResponsiveContainer width='100%' height='100%'>
                    <BarChart
                        width={800}
                        height={300}
                        data={props.data.records}
                    >
                        <CartesianGrid strokeDasharray="3 3"/>
                        <XAxis dataKey="label"/>
                        <YAxis/>
                        <Tooltip/>
                        <Bar dataKey="productionQty" fill="#8884d8" name="Produkcja"/>
                        <Bar dataKey="travelQty" fill="#82ca9d" name="Trasa"/>
                    </BarChart>
                </ResponsiveContainer>
            </ChartWrapper>
        </Col>
    </Row>
}
