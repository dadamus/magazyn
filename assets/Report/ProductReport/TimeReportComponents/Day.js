import React from 'react';
import {Col, Row} from "react-bootstrap";

export default function Day(props) {
    const getDate = () => {
        const date = new Date(props.data.date.timestamp * 1000);

        let d = date.getDate();
        let m = date.getMonth() + 1; //Month from 0 to 11
        let y = date.getFullYear();

        return '' + (d <= 9 ? '0' + d : d) + '-' + (m <= 9 ? '0' + m : m) + '-' + y;
    }


    return <Row className='day'>
        <Col lg={4}>
            <Row>
                <Col lg={12}>Dzień:</Col>
            </Row>
            <Row>
                <Col lg={12}>
                    {getDate()}
                </Col>
            </Row>
        </Col>
        <Col lg={2}>
            <Row>
                <Col lg={12}>Wyprodukowano:</Col>
            </Row>
            <Row>
                <Col lg={6}>
                    {props.data.made.quantity}
                </Col>
                <Col lg={6}>
                    {props.data.made.quantityWithCorrection}
                </Col>
            </Row>
        </Col>
        <Col lg={2}>
            <Row>
                <Col lg={12}>Wydajność:</Col>
            </Row>
            <Row>
                <Col lg={6}>
                    {props.data.performance.percentage}%
                </Col>
                <Col lg={6}>
                    {props.data.performance.percentageWithCorrection}%
                </Col>
            </Row>
        </Col>
        <Col lg={2}>
            <Row>
                <Col lg={12}>Załadowano:</Col>
            </Row>
            <Row>
                <Col lg={6}>
                    {props.data.charged.quantity}
                </Col>
                <Col lg={6}>
                    {props.data.charged.quantityWithCorrection}
                </Col>
            </Row>
        </Col>
        <Col lg={2}>
            <Row>
                <Col lg={12}>Bilans:</Col>
            </Row>
            <Row>
                <Col lg={6}>
                    {props.data.balance.quantity}
                </Col>
                <Col lg={6}>
                    {props.data.balance.quantityWithCorrection}
                </Col>
            </Row>
        </Col>
    </Row>
}
