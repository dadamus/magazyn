import React, {useState} from 'react';
import Summary from "./DailyReportComponents/Summary";
import {Button, Col, Row, Spinner} from "react-bootstrap";
import Day from "./TimeReportComponents/Day";

export default function TimeReport(props) {
    const generate = () => {
        if (!props.loading) {
            props.loadData();
        }
    }

    const generateButton = () => {
        if (!props.loading) {
            return <Button onClick={generate}>Generuj</Button>;
        }

        return <Row><Col lg={12}><Spinner animation="grow"/></Col></Row>
    };

    const renderSummary = () => {
        if (props.data === null) {
            return;
        }

        return <Summary data={props.data}/>
    }

    const renderDays = () => {
        if (props.data === null) {
            return;
        }

        return props.data.days.map((day) => {
            return <Day data={day} standard={props.data.standard}/>
        });
    }

    return <>
        <Row style={{marginBottom: '20px'}}>
            <Col lg={12}>
                {generateButton()}
            </Col>
        </Row>
        {renderSummary()}
        {renderDays()}
    </>;
}
