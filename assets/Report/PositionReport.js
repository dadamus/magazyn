import React, {useState} from 'react';
import {Col, Row, Button, Spinner} from "react-bootstrap";
import FromToDatePicker from "../SharedKernel/Form/FromToDatePicker";
import Positions from "../SharedKernel/Form/Product/PositionsSelector";

export default function PositionReport(props) {
    return <>
        <Row className="subhead">
            <div className="title">
                Raporty
                <div className="breadcrumb">
                    Stanowiska zbiorczy
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={12}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Raport stanowiska zbiorczy
                        </div>
                    </div>
                    <div className="portlet-body">
                        <Row>
                            <Col lg={3}>
                                <div className="form-group">
                                    <label htmlFor="name">Przedział czasowy:</label>
                                    <FromToDatePicker
                                        setFrom={value => setFilters('from', value)}
                                        setTo={value => setFilters('to', value)}
                                    />
                                </div>
                            </Col>

                            <Col lg={3}>
                                <div className="form-group">
                                    <label htmlFor="name">Stanowiso:</label>
                                    <Positions
                                        itemID="position"
                                        name="position"
                                        placeholder="Wybierz"
                                        // onChange={event => props.changeFilters('code_position_id', event.target.value)}
                                    />
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={12}>
                                <div className="separator"/>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={12}>
                                <table className='table table-striped table-bordered'>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Detal</th>
                                        <th colSpan={2}>Wyprodukowano</th>
                                        <th colSpan={2}>Załadowano</th>
                                        <th colSpan={2}>Bilans</th>
                                        <th colSpan={2}>Magazyn</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            Test
                                        </td>
                                        <td>
                                            5
                                        </td>
                                        <td>
                                            5
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            5
                                        </td>
                                        <td>
                                            5
                                        </td>
                                        <td>
                                            20
                                        </td>
                                        <td>
                                            20
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={12}>
                                <div className="separator"/>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={12}>
                                <table className='table table-striped table-bordered'>
                                    <thead>
                                    <tr>
                                        <th colSpan={10}>2022-05-01</th>
                                    </tr>
                                    <tr>
                                        <th>#</th>
                                        <th>Detal</th>
                                        <th colSpan={2}>Wyprodukowano</th>
                                        <th colSpan={2}>Załadowano</th>
                                        <th colSpan={2}>Bilans</th>
                                        <th colSpan={2}>Magazyn</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            Test
                                        </td>
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={12}>
                                <table className='table table-striped table-bordered'>
                                    <thead>
                                    <tr>
                                        <th colSpan={10}>2022-05-02</th>
                                    </tr>
                                    <tr>
                                        <th>#</th>
                                        <th>Detal</th>
                                        <th colSpan={2}>Wyprodukowano</th>
                                        <th colSpan={2}>Załadowano</th>
                                        <th colSpan={2}>Bilans</th>
                                        <th colSpan={2}>Magazyn</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            Test
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            20
                                        </td>
                                        <td>
                                            20
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </Col>
                        </Row>
                    </div>
                </div>
            </Col>
        </Row>
    </>
}
