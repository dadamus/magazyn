import React, {useState} from 'react';
import {Button, Col, Container, Row} from "react-bootstrap";
import FromToDatePicker from "../SharedKernel/Form/FromToDatePicker";
import ProductSelector from "../SharedKernel/Form/ProductSelector";
import DailyReport from "./ProductReport/DailyReport";
import TimeReport from "./ProductReport/TimeReport";
import {get} from "../SharedKernel/Api";

export default function ProductReport(props) {
    const [loading, setLoading] = useState(false);
    const [dailyData, setDailyData] = useState(null);

    const [reportFilters, setReportFilters] = useState({
        from: null,
        to: null,
        product: null,
        report: 1,
        standard: null
    });

    const setFilters = (name, value) => {
        let newFilters = reportFilters;

        if (name === "from" || name === "to") {
            let date = new Date(value);
            let d = date.getDate();
            let m = date.getMonth() + 1; //Month from 0 to 11
            let y = date.getFullYear();
            value = '' + y + '-' + (m <= 9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
        }

        newFilters[name] = value;

        setReportFilters({...reportFilters, ...newFilters});
    }

    const renderReport = () => {
        if (reportFilters.report === null) {
            return;
        }

        switch (parseInt(reportFilters.report)) {
            case 1:
                return <DailyReport data={dailyData} loadData={loadDailyData} loading={loading}/>
            case 2:
                return <TimeReport data={dailyData} loadData={loadDailyData} loading={loading}/>
        }
    }

    const loadDailyData = () => {
        if (!loading) {
            setLoading(true);
            setDailyData(null);
            get(
                'api/report/product/daily?from=' + reportFilters.from
                + "&to=" + reportFilters.to
                + "&product=" + reportFilters.product
                + "&standard=" + reportFilters.standard
            ).then((response) => {
                return response.json();
            }).then((data) => {
                setDailyData(data);
            }).finally(() => {
                setLoading(false);
            });
        }
    }

    return <>
        <Row className="subhead">
            <div className="title">
                Raporty
                <div className="breadcrumb">
                    Produktu
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={12}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Raport produktu
                        </div>
                    </div>
                    <div className="portlet-body">
                        <Row>
                            <Col lg={3}>
                                <div className="form-group">
                                    <label htmlFor="name">Przedział czasowy:</label>
                                    <FromToDatePicker
                                        setFrom={value => setFilters('from', value)}
                                        setTo={value => setFilters('to', value)}
                                    />
                                </div>
                            </Col>

                            <Col lg={3}>
                                <div className='form-group'>
                                    <label htmlFor="standard">Norma:</label>
                                    <input className='form-control' type='number'
                                           onChange={event => setFilters('standard', event.target.value)}/>
                                </div>
                            </Col>

                            <Col lg={3}>
                                <div className="form-group">
                                    <label htmlFor="name">Produkt:</label>
                                    <ProductSelector
                                        placeholder="Wybierz produkt..."
                                        onChange={value => setFilters('product', value.value)}
                                    />
                                </div>
                            </Col>

                            <Col lg={3}>
                                <div className="form-group">
                                    <label htmlFor="name">Rodzaj raportu:</label>
                                    <select
                                        className="form-control form-control-sm"
                                        name="report-type"
                                        placeholder="Wybierz rodzaj..."
                                        onChange={event => setFilters('report', event.target.value)}
                                    >
                                        <option value={1}>Dzienny</option>
                                        <option value={2}>Czasowy</option>
                                    </select>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={12}>
                                <div className="separator"/>
                            </Col>
                        </Row>
                        {renderReport()}
                    </div>
                </div>
            </Col>
        </Row>
    </>;
}
