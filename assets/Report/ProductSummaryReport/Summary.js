import React from 'react';
import Place from "./Place";

export default function Summary(props) {
    const renderPlaces = () => {
        return props.places.map((place) => {
            console.log(place);
            if (place.empty) {
                return;
            }

            return <Place data={place}/>
        });
    }

    return renderPlaces();
}
