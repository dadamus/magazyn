import React from 'react';
import {Col, Row} from "react-bootstrap";

export default function Place(props) {
    const renderDetails = () => {
        let lp=0;

        return props.data.details.map((detail) => {
            if (detail.empty) {
                return;
            }
            lp++;

            return <tr>
                <td>{lp}</td>
                <td>{detail.name}</td>
                <td>{detail.made.quantity}</td>
                <td>{detail.made.quantityWithCorrection}</td>
                <td>{detail.charged.quantity}</td>
                <td>{detail.charged.quantityWithCorrection}</td>
                <td>{detail.balance.quantity}</td>
                <td>{detail.balance.quantityWithCorrection}</td>
                <td></td>
                <td></td>
            </tr>
        });
    }

    return <>
        <Row>
            <Col lg={12}>{props.data.name}</Col>
        </Row>
        <Row>
            <Col lg={12}>
                <table className='table table-striped table-bordered'>
                    <thead>
                    <tr>
                        <th width='10%'>#</th>
                        <th width='20%'>Detal</th>
                        <th width='17.5%' colSpan={2}>Wyprodukowano</th>
                        <th width='17.5%' colSpan={2}>Załadowano</th>
                        <th width='17.5%' colSpan={2}>Bilans</th>
                        <th width='17.5%' colSpan={2}>Magazyn</th>
                    </tr>
                    </thead>
                    <tbody>
                    {renderDetails()}
                    </tbody>
                </table>
            </Col>
        </Row>
    </>
}
