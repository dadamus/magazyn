import React from 'react';
import Summary from "./Summary";
import {Col, Row} from "react-bootstrap";

export default function Days(props) {
    const formatTime = (timestamp) => {
        const date = new Date(timestamp * 1000);

        let month = date.getMonth() + 1;
        let day = date.getDate();

        if (day < 10) {
            day = "0" + day;
        }

        if (month < 10) {
            month = "0" + month;
        }

        return day + "-" + month + "-" + date.getFullYear();
    };

    const filterDays = (days) => {
        return days.filter((day) => {
            let empty = true;

            day.places.forEach((place) => {
                if (!place.empty) {
                    empty = false;
                }
            });

            return !empty;
        });
    }

    const renderPlaces = () => {
        return filterDays(props.days).map((day) => {
            return <>
                <Row>
                    <Col lg={12}>
                        {formatTime(day.date.timestamp)}
                    </Col>
                </Row>
                <Summary places={day.places}/>
            </>
        });
    }

    return renderPlaces();
}
