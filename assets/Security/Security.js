import React, {useEffect, useState} from 'react';
import {Redirect, Route} from 'react-router-dom';
import LoginPage from "./LoginPage";
import Menu from "../Page/Menu";

export default function Security(props) {
    const [token, setToken] = useState(localStorage.getItem('token'));

    useEffect(() => {
        if (token === null) {
            if (window.location.pathname !== "/login") {
                window.location.href = "/login";
            }
        }
    }, []);

    let RenderMenu = () => {
        if (token === null) {
            return <></>;
        }

        return <Menu/>;
    }

    return <RenderMenu/>;
}
