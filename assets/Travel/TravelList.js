import React, {useEffect, useState} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import DataTable from "../SharedKernel/DataTable";
import Filters from "./TravelList/Filters";
import {post} from "../SharedKernel/Api";
import {toast} from "react-toastify";
import AddTravel from "./TravelList/AddTravel";
import TravelCardButton from "./TravelList/TravelCardButton";

export default function TravelList() {
    const [filters, setFilters] = useState({page: 1, length: 50, filters: {}});
    const [rows, setRows] = useState([]);
    const [loading, setLoading] = useState(false);
    const [recordFiltered, setRecordFiltered] = useState(0);
    const [draw, setDraw] = useState(0);

    const columns = [
        {
            data: 'id',
            label: 'ID',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'name',
            label: 'Nazwa',
            searchable: true,
            search: {
                value: null
            },
            render: TravelCardButton
        },
        {
            data: 'date',
            label: 'Data',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'plate',
            label: 'Nr Rej',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'author',
            label: 'Autor',
            searchable: true,
            search: {
                value: null
            }
        }
    ];

    const changeFilters = (name, value) => {
        let newFilters = filters;
        newFilters['filters'][name] = value;

        setFilters({...filters, ...newFilters});
    }

    useEffect(() => {
        search();
    }, []);

    const search = () => {
        if (!loading) {
            setLoading(true);
            post(
                'api/travels/list/filter', {columns, filters}
            ).then((response) => {
                return response.json();
            }).then((data) => {
                setRows(data.data);
                setRecordFiltered(data.recordsFiltered);
                setDraw(data.draw);
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false)
            });
        }
    }

    return <>
        <Row className="subhead">
            <div className="title">
                Trasy
                <div className="breadcrumb">
                    Lista tras
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={12}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Lista tras
                        </div>
                        <div className="toolbar">
                            <AddTravel refreshRows={search}/>
                        </div>
                    </div>
                    <div className="portlet-body">
                        <Filters/>
                        <DataTable
                            filters={filters}
                            rows={rows}
                            loading={loading}
                            columns={columns}
                            search={search}
                            draw={draw}
                            recordsFiltered={recordFiltered}/>
                    </div>
                </div>
            </Col>
        </Row>
    </>
}
