import React, {useEffect, useState} from 'react';
import {post} from "../../SharedKernel/Api";
import {toast} from "react-toastify";

export default function PlannedInput(props) {
    const [planned, setPlanned] = useState(props.planned);
    const [blocked, setBlocked] = useState(false);
    const [saveTimeout, setSaveTimeout] = useState(null);

    const handleChangePlanned = (value) => {
        if (saveTimeout > 0) {
            clearTimeout(saveTimeout);
        }

        setPlanned(value);

        if (value > 0) {
            setSaveTimeout(setTimeout(() => {
                savePlanned(value)
            }, 1000));
        }
    }

    const savePlanned = (plannedValue) => {
        setBlocked(true);

        post(
            'api/travel/card/' + props.travelId + '/' + props.productId + '/change/quantity',
            {'planned_quantity': plannedValue}
        ).then((data) => {
            toast.success('Planowana ilość zapisana');
        }).catch(() => {
            toast.error('Wystąpił błąd!');
        }).finally(() => {
            setBlocked(false);
        });
    }

    return <input
        type="number"
        className="form-control"
        value={planned}
        disabled={blocked}
        onChange={e => handleChangePlanned(e.target.value)}
    />;
}