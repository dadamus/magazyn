import React, {useEffect, useState} from 'react';
import {Button, Modal} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import ProductSelector from "../../SharedKernel/Form/ProductSelector";
import {post} from "../../SharedKernel/Api";
import {toast} from "react-toastify";
import {parse} from "@fortawesome/fontawesome-svg-core";

export function AddDetailButton(props) {
    const [show, setShow] = useState(false);
    const [loading, setLoading] = useState(false);
    const [product, setProduct] = useState(null);
    const [planned, setPlanned] = useState(0);

    const handleClose = () => {
        setShow(false);
    }

    const handleShow = () => {
        setShow(true);
    }

    const handleSubmit = () => {
        setLoading(true);

        post(
            'api/travel/card/' + props.travelId + '/product',
            {'planned': planned, 'product_id': product}
        ).then((data) => {
            toast.success('Produkt dodany');
        }).catch(() => {
            toast.error('Wystąpił błąd!');
        }).finally(() => {
            setLoading(false);
            setShow(false);
            props.search();
        });
    }

    return <>
        <Button variant="secondary" onClick={handleShow}>Dodaj detal</Button>

        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Dodaj detal</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form>
                    <div className="form-group">
                        <label htmlFor="name">Produkt:</label>
                        <ProductSelector placeholder="Wybierz produkt..." onChange={value => setProduct(value.value)}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="name">Planowana ilość:</label>
                        <input type="number" className="form-control" onChange={event => setPlanned(parseInt(event.target.value))}/>
                    </div>
                </form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" disabled={loading} onClick={handleClose}>
                    Zamknij
                </Button>
                <Button variant="primary" disabled={loading} onClick={handleSubmit}>
                    Dodaj
                </Button>
            </Modal.Footer>
        </Modal></>
}