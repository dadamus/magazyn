import React, {useEffect, useState} from 'react';
import {Button} from "react-bootstrap";
import {deleteRequest} from "../../SharedKernel/Api";
import {toast} from "react-toastify";

export default function DeleteProduct(props) {
    const [disabled, setDisabled] = useState(false);

    const deleteAction = () => {
        setDisabled(true);

        //api/travel/card/{travelId}/codes/{productId}/remove
        deleteRequest(
            'api/travel/card/' + props.travelId + '/codes/' + props.productId + '/remove'
        ).then((data) => {
            toast.success('Produkt usunety');
        }).catch(() => {
            toast.error('Wystąpił błąd!');
        }).finally(() => {
            props.search();
        });
    }

    return <Button variant='danger' disabled={disabled} onClick={deleteAction}>Usuń</Button>
}