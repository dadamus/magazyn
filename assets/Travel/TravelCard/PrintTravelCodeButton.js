import React, {useEffect, useState} from 'react';
import {Button} from "react-bootstrap";
import {post} from "../../SharedKernel/Api";
import {toast} from "react-toastify";

export default function PrintTravelCodeButton(props) {
    const [disabled, setDisabled] = useState(false);

    const print = () => {
        setDisabled(true);

        post(
            'api/travel/card/' + props.travelId + '/code/generate', {}
        ).then(
            (data) => toast.success('Zlecono druk kodu!')
        ).catch(() => {
            toast.error('Wystąpił błąd!');
        }).finally(() => {
            setDisabled(false);
        });
    }

    return <Button variant="primary" disabled={disabled} onClick={print}>Kod trasy</Button>
}