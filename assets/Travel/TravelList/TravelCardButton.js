import React from 'react';
import {Link} from "react-router-dom";

export default function TravelCardButton(props) {
    return <Link to={"/travel/card/" + props.row.id}>{props.row.name}</Link>
}