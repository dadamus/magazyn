import React, {useEffect, useState} from 'react';
import {useParams} from "react-router-dom";
import {ButtonGroup, Col, Container, Row} from "react-bootstrap";
import {get, post} from "../SharedKernel/Api";
import {toast} from "react-toastify";
import PrintTravelCodeButton from "./TravelCard/PrintTravelCodeButton";
import DataTable from "../SharedKernel/DataTable";
import {AddDetailButton} from "./TravelCard/AddDetailButton";
import PlannedInput from "./TravelCard/PlannedInput";
import DeleteProduct from "./TravelCard/DeleteProduct";

export default function TravelCard() {
    let {id} = useParams();
    const [filters, setFilters] = useState({page: 1, length: 50, filters: {}});
    const [loading, setLoading] = useState(false);
    const [travelData, setTravelData] = useState({});
    const [allVisible, setAllVisible] = useState(false);
    const [rows, setRows] = useState([]);
    const [recordFiltered, setRecordFiltered] = useState(0);
    const [draw, setDraw] = useState(0);

    useEffect(() => {
        loadCardDetails();
        search();
    }, [])

    const loadCardDetails = () => {
        setLoading(true);
        get(
            'api/travel/card/' + id
        ).then((response) => {
            return response.json();
        }).then((data) => {
            setTravelData(data.travel);
            setAllVisible(data.allVisible);
        }).catch(() => {
            toast.error('Wystąpił błąd!');
        }).finally(() => {
            setLoading(false)
        });
    }

    const search = () => {
        if (!loading) {
            setLoading(true);
            post(
                'api/travel/card/' + id + '/list/filter', {columns, filters}
            ).then((response) => {
                return response.json();
            }).then((data) => {
                setRows(data.data);
                setRecordFiltered(data.recordsFiltered);
                setDraw(data.draw);
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false)
            });
        }
    }

    const columns = [
        {
            data: 'id',
            label: 'ID',
            width: '20%',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'name',
            label: 'Nazwa',
            width: '40%',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'scanned',
            label: 'Zeskanowano',
            width: '20%',
            searchable: true,
            search: {
                value: null
            },
            render: (data) => {
                return data.row.scanned ?? 0
            }
        },
        {
            data: 'planned',
            label: 'Planowano',
            width: '20%',
            searchable: false,
            search: {
                value: null
            },
            render: (data) => {
                return <PlannedInput travelId={id} productId={data.row.id} planned={data.row.planned ?? 0}/>
            }
        },
        {
            data: 'actions',
            label: 'Akcje',
            width: '20%',
            searchable: false,
            search: {
                value: null
            },
            render: (data) => {
                return <DeleteProduct search={search} travelId={id} productId={data.row.id}/>
            }
        }
    ];

    return <>
        <Row className="subhead">
            <div className="title">
                Trasy
                <div className="breadcrumb">
                    Lista tras
                </div>
                <div className="breadcrumb">
                    Karta trasy
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={12}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Karta trasy {travelData.name}
                        </div>
                        <div className="toolbar">
                            <ButtonGroup>
                                <PrintTravelCodeButton travelId={id}/>
                                <AddDetailButton search={search} travelId={id}/>
                            </ButtonGroup>
                        </div>
                    </div>
                    <div className="portlet-body">
                        <DataTable
                            filters={filters}
                            rows={rows}
                            loading={loading}
                            columns={columns}
                            search={search}
                            draw={draw}
                            recordsFiltered={recordFiltered}/>
                    </div>
                </div>
            </Col>
        </Row>
    </>;
}