import React, {useEffect, useState} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import ActiveDevices from "./ActiveDevices";
import LastScannedCodes from "./LastScannedCodes";
import Devices from "./Devices";
import {get} from "../SharedKernel/Api";

export default function () {
    const [activeDevices, setActiveDevices] = useState([]);
    const [devices, setDevices] = useState([]);
    const [devicesLoading, setDevicesLoading] = useState(false);
    const [lastScannedCodesLoading, setLastScannedCodesLoading] = useState(false);
    const [lastScannedCodes, setLastScannedCodes] = useState([]);

    const loadDevices = () => {
        if (!devicesLoading) {
            setDevicesLoading(true);
            get(
                'api/devices'
            ).then((response) => {
                return response.json();
            }).then((data) => {
                setActiveDevices(data.activeDevices);
                setDevices(data.devices);
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setDevicesLoading(false)
            });
        }
    };

    const loadLastScannedCodes = () => {
        if (!lastScannedCodesLoading) {
            setLastScannedCodesLoading(true);
            get(
                'api/last-scanned-codes'
            ).then((response) => {
                return response.json();
            }).then((data) => {
                console.log(data);
                setLastScannedCodes(data);
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLastScannedCodesLoading(false)
            });
        }
    }

    useEffect(() => {
        loadDevices();
        loadLastScannedCodes();
    }, []);

    return <>
        <Row className="subhead">
            <div className="title">
                Dashboard
                <div className="breadcrumb">
                    Dashboard
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={8}>
                <Devices loading={devicesLoading} devices={devices} activeDevices={activeDevices}/>
            </Col>
            <Col lg={4}>
                <ActiveDevices loading={devicesLoading} activeDevices={activeDevices}/>
                <LastScannedCodes codes={lastScannedCodes} loading={lastScannedCodesLoading}/>
            </Col>
        </Row>
    </>;
}
