import React from 'react';
import {Col, Row, Spinner} from "react-bootstrap";
import {faGenderless} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export default function (props) {
    const renderCodes = () => {
        if (props.loading) {
            return <Spinner animation="grow"/>
        }

        return props.codes.map((code) => {
            return <Row>
                <Col lg={2}>{getCodeDate(code)}</Col>
                <Col lg={1}><FontAwesomeIcon icon={faGenderless} style={{color: 'blue'}}/></Col>
                <Col lg={9}><b>{code.product_name}</b> - {code.device_name}</Col>
            </Row>
        });
    };

    const getCodeDate = (code) => {
        const date = code.date.split(' ');

        return date[1];
    }

    return <div className="portlet">
        <div className="portlet-head">
            <div className="label">
                Ostatnio zaczytane kody
            </div>
        </div>
        <div className="portlet-body" style={{fontSize: 'small'}}>
            {renderCodes()}
        </div>
    </div>;
}
