import React from 'react';
import {Col, Row, Spinner} from "react-bootstrap";

export default function (props) {
    const renderDevices = () => {
        if (props.loading) {
            return <tr><td><Spinner animation="grow"/></td></tr>
        }

        return props.devices.map((device) => {
            return <tr key={device.id}>
                <td>{deviceCode(device)} {device.name}</td>
                <td>{device.device_type_name}</td>
                <td>{device.travel_name ?? '-'}</td>
                <td>{device.last_sync ?? '-'} ({device.last_ip ?? 'Brak ip'})</td>
            </tr>
        });
    };

    const deviceCode = (device) => {
        const color = isActiveDevice(device.hash) ? 'green' : 'red';

        return <div style={{
            height: '15px',
            width: '15px',
            backgroundColor: color,
            borderRadius: '50%',
            display: 'inline-block'
        }}/>
    }

    const isActiveDevice = (name) => {
        return props.activeDevices.includes(name);
    }

    return <div className="portlet">
        <div className="portlet-head">
            <div className="label">
                Urządzenia
            </div>
        </div>
        <div className="portlet-body">
            <table className="table">
                <thead>
                <tr>
                    <td scope="col">Nazwa urządzenia</td>
                    <td scope="col">Tryb</td>
                    <td scope="col">Ostatnia trasa</td>
                    <td scope="col">Ostatnia synchronizacja</td>
                </tr>
                </thead>
                <tbody>
                {renderDevices()}
                </tbody>
            </table>
        </div>
    </div>;
}
