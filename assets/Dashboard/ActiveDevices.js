import React from 'react';
import {Col, Row, Spinner} from "react-bootstrap";

export default function (props) {

    const renderDevices = () => {
        if (props.loading) {
            return <Spinner animation="grow"/>
        }

        return props.activeDevices.map((deviceName) => {
            return <Row><Col lg={12}>{deviceName}</Col></Row>
        });
    }

    return <div className="portlet">
        <div className="portlet-head">
            <div className="label">
                Aktywne urzadzenia
            </div>
        </div>
        <div className="portlet-body" style={{fontSize: "small"}}>
                {renderDevices()}
        </div>
    </div>;
}
