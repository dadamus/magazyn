import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import Dashboard from "./Dashboard/Dashboard";
import {Container} from "react-bootstrap";
import Cars from "./Settings/Cars";
import Menu from "./Page/Menu";
import Devices from "./Settings/Devices";
import Positions from "./Settings/Positions";
import ProductList from "./Product/ProductList";
import CodeList from "./Code/CodeList";
import TravelList from "./Travel/TravelList";
import LoginPage from "./Security/LoginPage";
import Security from "./Security/Security";
import {ToastContainer} from 'react-toastify';
import ProductCard from "./Product/ProductCard";
import ProductReport from "./Report/ProductReport";
import ProductSummaryReport from "./Report/ProductSummaryReport";
import PositionReport from "./Report/PositionReport";
import CarReport from "./Report/CarReport";
import TravelCard from "./Travel/TravelCard";

ReactDOM.render(
    <React.StrictMode>
        <ToastContainer/>
        <Router>
            <Security/>
            <Container className="main-container" fluid>
                <Switch>
                    <Route path="/products/list"><ProductList/></Route>
                    <Route path="/product/card/:id"><ProductCard/></Route>
                    <Route path="/code/list"><CodeList/></Route>
                    <Route path="/travel/list"><TravelList/></Route>
                    <Route path="/travel/card/:id"><TravelCard/></Route>
                    <Route path="/settings/cars"><Cars/></Route>
                    <Route path="/settings/positions"><Positions/></Route>
                    <Route path="/settings/devices"><Devices/></Route>
                    <Route path="/report/product/summary"><ProductSummaryReport/></Route>
                    <Route path="/report/place/summary"><PositionReport/></Route>
                    <Route path="/report/car/summary"><CarReport/></Route>
                    <Route path="/report/product"><ProductReport/></Route>
                    <Route path="/login"><LoginPage/></Route>
                    <Route path="/"><Dashboard/></Route>
                </Switch>
            </Container>

        </Router>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
