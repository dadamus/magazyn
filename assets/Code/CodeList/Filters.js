import React, {useState} from 'react';
import {Button, Col, Row} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch, faTimes} from "@fortawesome/free-solid-svg-icons";
import FromToDatePicker from "../../SharedKernel/Form/FromToDatePicker";
import ProductSelector from "../../SharedKernel/Form/ProductSelector";
import Positions from "../../SharedKernel/Form/Product/PositionsSelector";

export default function Filters(props) {
    const [productionFrom, setProductionFrom] = useState();
    const [productionTo, setProductionTo] = useState();
    const [travelFrom, setTravelFrom] = useState();
    const [travelTo, setTravelTo] = useState();
    const [product, setProduct] = useState();
    const [productionPosition, setProductionPosition] = useState();
    const [travelPosition, setTravelPosition] = useState();

    const changeProductionFrom = (from) => {
        if (from === null) {
            from = '';
        } else {
            from = formatDate(from);
        }

        setProductionFrom(from);

        if (productionFrom === '' && productionTo === '') {
            props.changeFilters('production_date', null);
        } else {
            props.changeFilters('production_date', from + '|' + productionTo);
        }
    }

    const changeProductionTo = (to) => {
        if (to === null) {
            to = '';
        } else {
            to = formatDate(to);
        }

        setProductionTo(to);

        if (productionFrom === '' && productionTo === '') {
            props.changeFilters('production_date', null);
        } else {
            props.changeFilters('production_date', productionFrom + '|' + to);
        }
    }

    const changeTravelFrom = (from) => {
        if (from === null) {
            from = '';
        } else {
            from = formatDate(from);
        }

        setTravelFrom(from);

        if (travelFrom === '' && travelTo === '') {
            props.changeFilters('travel_date', null);
        } else {
            props.changeFilters('travel_date', from + '|' + travelTo);
        }
    }

    const changeTravelTo = (to) => {
        if (to === null) {
            to = '';
        } else {
            to = formatDate(to);
        }

        setTravelTo(to);

        if (travelFrom === '' && travelTo === '') {
            props.changeFilters('travel_date', null);
        } else {
            props.changeFilters('travel_date', travelFrom + '|' + to);
        }
    }

    const changeProduct = (productId) => {
        setProduct(productId);
        props.changeFilters('product_id', productId);
    }

    const changeProductPosition = (productionPosition) => {
        setProductionPosition(productionPosition)
        props.changeFilters('production_place_id', productionPosition);
    }

    const changeTravelPosition = (travelPosition) => {
        setTravelPosition(travelPosition)
        props.changeFilters('travel_place_id', travelPosition);
    }

    const formatDate = (dateToFormat) => {
        const date = new Date(dateToFormat);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();

        if (month < 10) {
            month = "0" + month;
        }

        if (day < 10) {
            day = "0" + day;
        }

        return year + "-" + month + "-" + day;
    }

    return <div className="filters">
        <form>
            <Row>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="category">Data produkcji:</label>
                        <FromToDatePicker setFrom={changeProductionFrom} setTo={changeProductionTo}/>
                    </div>
                </Col>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="category">Data trasy:</label>
                        <FromToDatePicker setFrom={changeTravelFrom} setTo={changeTravelTo}/>
                    </div>
                </Col>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="category">Produkt:</label>
                        <ProductSelector onChange={value => changeProduct(value.value)}/>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <div className="separator"/>
                </Col>
            </Row>
            <Row>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="category">Stanowisko produkcji:</label>
                        <Positions
                            value={productionPosition}
                            onChange={event => changeProductPosition(event.target.value)}
                        />
                    </div>
                </Col>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="category">Stanowisko załadunku:</label>
                        <Positions
                            value={travelPosition}
                            onChange={event => changeTravelPosition(event.target.value)}
                        />
                    </div>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <div className="separator"/>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <Button onClick={props.search}><FontAwesomeIcon icon={faSearch}/> Szukaj</Button>
                    &nbsp;&nbsp;
                    <Button variant="secondary">
                        <FontAwesomeIcon icon={faTimes}/> Reset
                    </Button>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <div className="separator"/>
                </Col>
            </Row>
        </form>
    </div>;

}
