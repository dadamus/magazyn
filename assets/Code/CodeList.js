import React, {useEffect, useState} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import ProductModal from "../Product/ProductList/ProductModal";
import DataTable from "../SharedKernel/DataTable";
import Filters from "./CodeList/Filters";
import ProductCardButton from "../Product/ProductList/ProductCardButton";
import Actions from "../Product/ProductList/Actions";
import {post} from "../SharedKernel/Api";
import {toast} from "react-toastify";
import dataTableFiltersTranslator from "../SharedKernel/DataTableFiltersTranslator";

export default function CodeList()
{
    const [filters, setFilters] = useState({page: 1, length: 50, filters: {}});
    const [rows, setRows] = useState([]);
    const [loading, setLoading] = useState(false);
    const [recordFiltered, setRecordFiltered] = useState(0);
    const [draw, setDraw] = useState(0);

    const columns = [
        {
            data: 'id',
            label: 'ID',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'ean',
            label: 'Kod',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'product_id',
            searchable: true,
            visible: false,
            search: {
                value: null,
                type: 'number-strict'
            }
        },
        {
            data: 'travel_place_id',
            searchable: true,
            visible: false,
            search: {
                value: null,
                type: 'number-strict'
            }
        },
        {
            data: 'production_place_id',
            searchable: true,
            visible: false,
            search: {
                value: null,
                type: 'number-strict'
            }
        },
        {
            data: 'product_name',
            label: 'Produkt',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'production_date',
            label: 'Data produkcji',
            searchable: true,
            search: {
                value: null,
                type: 'date'
            }
        },
        {
            data: 'travel_date',
            label: 'Data trasy',
            searchable: true,
            search: {
                value: null,
                type: 'date'
            }
        }
    ];

    const changeFilters = (name, value) => {
        let newFilters = filters;
        newFilters['filters'][name] = value;

        setFilters({...filters, ...newFilters});
    }

    useEffect(() => {
        search();
    }, []);

    const search = () => {
        if (!loading) {
            setLoading(true);
            dataTableFiltersTranslator(filters, columns)
            post(
                'api/codes/list/filter', {columns, filters}
            ).then((response) => {
                return response.json();
            }).then((data) => {
                setRows(data.data);
                setRecordFiltered(data.recordsFiltered);
                setDraw(data.draw);
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false)
            });
        }
    }

    return <>
        <Row className="subhead">
            <div className="title">
                Kody
                <div className="breadcrumb">
                    Lista kodów
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={12}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Lista kodów
                        </div>
                    </div>
                    <div className="portlet-body">
                        <Filters changeFilters={changeFilters} search={search}/>
                        <DataTable
                            filters={filters}
                            rows={rows}
                            loading={loading}
                            columns={columns}
                            search={search}
                            draw={draw}
                            recordsFiltered={recordFiltered}/>
                    </div>
                </div>
            </Col>
        </Row>
    </>
}
