import React, {useState} from 'react';
import {Button, Col, Container, Row, Table} from "react-bootstrap";
import ProductTypeSelector from "../../SharedKernel/Form/Product/TypeSelector";
import ProductCategorySelector from "../../SharedKernel/Form/Product/CategorySelector";
import ProductPositionsSelector from "../../SharedKernel/Form/Product/PositionsSelector";
import Modal from "../../SharedKernel/Modal";
import {post, put} from "../../SharedKernel/Api";
import {toast} from 'react-toastify';

export default function ProductModal(props) {
    const [loading, setLoading] = useState(false);
    const [productData, setProductData] = useState(props.productData ?? {});

    let changeFormData = (name, value) => {
        let newData = {};
        newData[name] = value;

        setProductData({...productData, ...newData})
    }


    const handleSubmit = () => {
        setLoading(true);

        if (productData.id) {
            put(
                'api/product',
                productData
            ).then((response) => {
                if (response.status !== 200) {
                    throw 'Wystapil blad!';
                }

                return response.json();
            }).then((data) => {
                toast.success('Zapisano!');
                props.refreshList();
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false);
            })
        } else {
            post(
                'api/product',
                productData
            ).then((response) => {
                if (response.status !== 200) {
                    throw 'Wystapil blad!';
                }

                return response.json();
            }).then((data) => {
                toast.success('Zapisano!');
                props.refreshList();
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false);
            })
        }
    }

    return <>
        <Modal
            loading={loading}
            showButtonText={props.showButtonText}
            saveButton={<Button variant="primary" onClick={handleSubmit} disabled={loading}>Zapisz</Button>}
        >
            <form>
                <div className="form-group">
                    <label htmlFor="name">Nazwa</label>
                    <input
                        type="text"
                        className="form-control"
                        itemID="name"
                        name="name"
                        placeholder="Nazwa"
                        value={productData.name}
                        onChange={event => {changeFormData('name', event.target.value)}}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="name">Opis</label>
                    <textarea
                        className="form-control"
                        itemID="description"
                        name="description"
                        onChange={event => {changeFormData('description', event.target.value)}}
                    >
                        {productData.description}
                    </textarea>
                </div>
                <div className="form-group">
                    <label htmlFor="position">Stanowisko</label>
                    <ProductPositionsSelector
                        itemID="position"
                        name="position"
                        value={productData.position_id}
                        placeholder="Wybierz..."
                        onChange={event => {changeFormData('position_id', event.target.value)}}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="position">Kategoria</label>
                    <ProductCategorySelector
                        itemID="category"
                        name="category"
                        value={productData.category_id}
                        placeholder="Wybierz..."
                        onChange={event => {changeFormData('category_id', event.target.value)}}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="position">Typ</label>
                    <ProductTypeSelector
                        itemID="type"
                        name="type"
                        value={productData.type_id}
                        placeholder="Wybierz..."
                        onChange={event => {changeFormData('type_id', event.target.value)}}
                    />
                </div>
            </form>
        </Modal>
    </>;
}
