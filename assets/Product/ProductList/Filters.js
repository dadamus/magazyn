import React, {useState} from "react";
import {Button, Col, Row} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch, faTimes} from "@fortawesome/free-solid-svg-icons";
import ProductTypeSelector from "../../SharedKernel/Form/Product/TypeSelector";
import ProductCategorySelector from "../../SharedKernel/Form/Product/CategorySelector";
import PositionsSelector from "../../SharedKernel/Form/Product/PositionsSelector";

export default function Filters(props) {
    const [name, setName] = useState();
    const [type, setType] = useState();
    const [category, setCategory] = useState();
    const [position, setPosition] = useState();

    const changeName = (name) => {
        setName(name);
        props.changeFilters('name', name);
    }

    const changeType = (type) => {
        setType(type);
        props.changeFilters('type_id', type);
    }

    const changeCategory = (category) => {
        setCategory(category);
        props.changeFilters('category_id', category);
    }

    const changePosition = (position) => {
        setPosition(position);
        props.changeFilters('position_id', position);
    }

    const reset = () => {
        changeName('');
        changeType(null);
        changeCategory(null);
        changePosition(null);
        props.search();
    }

    return <div className="filters">
        <form>
            <Row>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="name">Nazwa</label>
                        <input
                            type="text"
                            className="form-control form-control-sm"
                            value={name}
                            onChange={event => changeName(event.target.value)}
                            itemID="name"
                            name="name"
                            placeholder="Nazwa"
                        />
                    </div>
                </Col>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="type">Typ</label>
                        <ProductTypeSelector
                            itemID="type"
                            name="type"
                            value={type}
                            placeholder="Wybierz"
                            onChange={event => changeType(event.target.value)}
                        />
                    </div>
                </Col>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="category">Kategoria</label>
                        <ProductCategorySelector
                            itemID="category"
                            name="category"
                            placeholder="Wybierz"
                            value={category}
                            onChange={event => changeCategory(event.target.value)}
                        />
                    </div>
                </Col>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="category">Pozycja</label>
                        <PositionsSelector
                            itemID="position"
                            name="position"
                            placeholder="Wybierz"
                            value={position}
                            onChange={event => changePosition(event.target.value)}
                        />
                    </div>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <div className="separator"/>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <Button onClick={props.search}><FontAwesomeIcon icon={faSearch}/> Szukaj</Button>
                    &nbsp;&nbsp;
                    <Button variant="secondary" onClick={reset}>
                        <FontAwesomeIcon icon={faTimes}/> Reset
                    </Button>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <div className="separator"/>
                </Col>
            </Row>
        </form>
    </div>;
}
