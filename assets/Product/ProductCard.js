import React, {useEffect, useState} from 'react';
import {
    useParams
} from "react-router-dom";
import {ButtonGroup, Col, Row} from "react-bootstrap";
import ProductModal from "./ProductList/ProductModal";
import Filters from "./ProductCard/Filters";
import DataTable from "../SharedKernel/DataTable";
import GenerateCodes from "./ProductCard/GenerateCodes";
import AddCorrection from "./ProductCard/AddCorrection";
import ProductCardButton from "./ProductList/ProductCardButton";
import {post} from "../SharedKernel/Api";
import {toast} from "react-toastify";
import dataTableFiltersTranslator from "../SharedKernel/DataTableFiltersTranslator";

export default function ProductCard() {
    let {id} = useParams();

    const [filters, setFilters] = useState({page: 1, length: 50, filters: {}});
    const [rows, setRows] = useState([]);
    const [loading, setLoading] = useState(false);
    const [recordFiltered, setRecordFiltered] = useState(0);
    const [draw, setDraw] = useState(0);

    const translateType = (type) => {
        switch (type) {
            case 'production':
                return 'Produkcja';
            case 'travel':
                return 'Załadunek';
            case 'correction':
                return 'Korekta';
        }
    };

    const columns = [
        {
            data: 'id',
            label: 'ID',
            searchable: true,
            search: {
                value: null,
                type: 'number-strict'
            }
        },
        {
            data: 'type',
            label: 'Typ',
            searchable: true,
            search: {
                value: null
            },
            render: (data) => {
                return translateType(data.row.type)
            }
        },
        {
            data: 'device_name',
            label: 'Nazwa urządzenia',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'value',
            label: 'Wartość',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'device_id',
            visible: false,
            searchable: true,
            search: {
                value: null,
                type: 'number-strict'
            }
        },
        {
            data: 'code_position_id',
            visible: false,
            searchable: true,
            search: {
                value: null,
                type: 'number-strict'
            }
        },
        {
            data: 'date',
            label: 'Data',
            searchable: true,
            search: {
                value: null
            }
        }
    ];

    const changeFilters = (name, value) => {
        let newFilters = filters;
        newFilters['filters'][name] = value;

        setFilters({...filters, ...newFilters});
    }

    useEffect(() => {
        search();
    }, []);

    const search = () => {
        if (!loading) {
            setLoading(true);
            dataTableFiltersTranslator(filters, columns);
            post(
                'api/product/card/' + id + '/state', {columns, filters}
            ).then((response) => {
                return response.json();
            }).then((data) => {
                setRows(data.data)
                setRecordFiltered(data.recordsFiltered);
                setDraw(data.draw);
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false)
            });
        }
    }

    return <>
        <Row className="subhead">
            <div className="title">
                Produkty
                <div className="breadcrumb">
                    Lista produktów
                </div>
                <div className="breadcrumb">
                    Karta produktu
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={12}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Karta produktu
                        </div>
                        <div className="toolbar">
                            <ButtonGroup>
                                <AddCorrection productId={id}/>
                                <GenerateCodes productId={id}/>
                            </ButtonGroup>
                        </div>
                    </div>
                    <div className="portlet-body">
                        <Filters changeFilters={changeFilters} search={search}/>
                        <DataTable
                            filters={filters}
                            rows={rows}
                            loading={loading}
                            columns={columns}
                            search={search}
                            draw={draw}
                            recordsFiltered={recordFiltered}/>
                    </div>
                </div>
            </Col>
        </Row>
    </>
}
