import React, {useEffect, useState} from 'react';
import {Button, Col, Row} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch, faTimes} from "@fortawesome/free-solid-svg-icons";
import DeviceSelector from "../../SharedKernel/Form/DeviceSelector";
import Positions from "../../SharedKernel/Form/Product/PositionsSelector";
import FromToDatePicker from "../../SharedKernel/Form/FromToDatePicker";

export default function Filters(props) {
    const [device, setDevice] = useState();
    const [position, setPosition] = useState();
    const [type, setType] = useState();
    const [from, setFrom] = useState('');
    const [to, setTo] = useState('');

    const changeDevice = (device) => {
        setDevice(device);
        props.changeFilters('device_id', device);
    }

    const changePosition = (position) => {
        setPosition(position);
        props.changeFilters('position_id', position);
    }

    const changeType = (type) => {
        setType(type);
        props.changeFilters('type', type);
    }

    const changeFrom = (from) => {
        if (from === null) {
            from = '';
        } else {
            from = formatDate(from);
        }

        setFrom(from);

        if (from === '' && to === '') {
            props.changeFilters('date', null);
        } else {
            props.changeFilters('date', from + '|' + to);
        }
    }

    const changeTo = (to) => {
        if (to === null) {
            to = '';
        } else {
            to = formatDate(to);
        }

        setTo(to);
        if (from === '' && to === '') {
            props.changeFilters('date', null);
        } else {
            props.changeFilters('date', from + '|' + to);
        }
    }

    const formatDate = (dateToFormat) => {
        const date = new Date(dateToFormat);
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();

        if (month < 10) {
            month = "0" + month;
        }

        if (day < 10) {
            day = "0" + day;
        }

        return year + "-" + month + "-" + day;
    }

    return <div className="filters">
        <form>
            <Row>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="type">Urządzenie:</label>
                        <DeviceSelector
                            itemID="device"
                            name="device"
                            placeholder="Wybierz"
                            value={device}
                            onChange={event => changeDevice(event.target.value)}
                        />
                    </div>
                </Col>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="type">Stanowisko:</label>
                        <Positions
                            itemID="position"
                            name="position"
                            placeholder="Wybierz"
                            value={position}
                            onChange={event => changePosition(event.target.value)}
                        />
                    </div>
                </Col>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="type">Typ:</label>
                        <select
                            className="form-control form-control-sm"
                            placeholder="Wybierz"
                            value={type}
                            onChange={event => changeType(event.target.value)}
                        >
                            <option value="">Wybierz</option>
                            <option value="travel">Załadunek</option>
                            <option value="production">Produkcja</option>
                            <option value="correction">Korekta</option>
                        </select>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col lg={3}>
                    <div className="form-group">
                        <label htmlFor="category">Data:</label>
                        <FromToDatePicker setFrom={changeFrom} setTo={changeTo}/>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <div className="separator"/>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <Button onClick={props.search}><FontAwesomeIcon icon={faSearch}/> Szukaj</Button>
                    &nbsp;&nbsp;
                    <Button variant="secondary">
                        <FontAwesomeIcon icon={faTimes}/> Reset
                    </Button>
                </Col>
            </Row>
            <Row>
                <Col lg={12}>
                    <div className="separator"/>
                </Col>
            </Row>
        </form>
    </div>
}
