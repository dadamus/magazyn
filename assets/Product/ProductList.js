import React, {useEffect, useState} from 'react';
import {Button, Col, Container, Row} from "react-bootstrap";
import Filters from "./ProductList/Filters";
import DataTable from "../SharedKernel/DataTable";
import ProductModal from "./ProductList/ProductModal";
import {post} from "../SharedKernel/Api";
import {toast} from 'react-toastify';
import Actions from "./ProductList/Actions";
import ProductCardButton from "./ProductList/ProductCardButton";
import dataTableFiltersTranslator from "../SharedKernel/DataTableFiltersTranslator";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export default function ProductList() {
    const [filters, setFilters] = useState({page: 1, length: 50, filters: {}});
    const [rows, setRows] = useState([]);
    const [loading, setLoading] = useState(false);
    const [recordFiltered, setRecordFiltered] = useState(0);
    const [draw, setDraw] = useState(0);

    const columns = [
        {
            data: 'id',
            label: 'ID',
            searchable: true,
            search: {
                value: null,
                type: 'number-strict'
            }
        },
        {
            data: 'name',
            label: 'Nazwa',
            searchable: true,
            search: {
                value: null,
                type: 'text-search'
            },
            render: ProductCardButton
        },
        {
            data: 'position_name',
            label: 'Pozycja',
            searchable: true,
            search: {
                value: null,
                type: 'number-strict'
            }
        },
        {
            data: 'position_id',
            searchable: true,
            visible: false,
            search: {
                value: null,
                type: 'number-strict'
            }
        },
        {
            data: 'category_name',
            label: 'Kategoria',
            searchable: true,
            search: {
                value: null,
                type: 'number-strict'
            }
        },
        {
            data: 'category_id',
            searchable: true,
            visible: false,
            search: {
                value: null,
                type: 'number-strict'
            }
        },
        {
            data: 'type_name',
            label: 'Typ',
            searchable: true,
            search: {
                value: null,
                type: 'number-strict'
            }
        },
        {
            data: 'type_id',
            searchable: true,
            visible: false,
            search: {
                value: null,
                type: 'number-strict'
            }
        },
        {
            data: 'action',
            label: 'Akcje',
            searchable: false,
            search: {
                value: null
            },
            width: '10%',
            render: (data) => {return <ProductModal showButtonText="Edytuj" productData={data.row} refreshList={search}/>}
        }
    ];

    const changeFilters = (name, value) => {
        let newFilters = filters;
        newFilters['filters'][name] = value;

        setFilters({...filters, ...newFilters});
    }

    useEffect(() => {
        search();
    }, []);

    const search = () => {
        if (!loading) {
            setLoading(true);
            dataTableFiltersTranslator(filters, columns)
            post(
                'api/products/list/filter', {columns, filters}
            ).then((response) => {
                return response.json();
            }).then((data) => {
                setRows(data.data);
                setRecordFiltered(data.recordsFiltered);
                setDraw(data.draw);
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false)
            });
        }
    }

    return <>
        <Row className="subhead">
            <div className="title">
                Produkty
                <div className="breadcrumb">
                    Lista produktów
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={12}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Lista produktów
                        </div>
                        <div className="toolbar">
                            <ProductModal refreshList={search} showButtonText={<><FontAwesomeIcon icon={faPlus}/> Dodaj produkt</>}/>
                        </div>
                    </div>
                    <div className="portlet-body">
                        <Filters changeFilters={changeFilters} search={search}/>
                        <DataTable
                            filters={filters}
                            rows={rows}
                            loading={loading}
                            columns={columns}
                            search={search}
                            draw={draw}
                            recordsFiltered={recordFiltered}/>
                    </div>
                </div>
            </Col>
        </Row>
    </>;
}
