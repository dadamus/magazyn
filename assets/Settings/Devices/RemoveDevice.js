import React, {useState} from 'react';
import {Button} from "react-bootstrap";

export default function RemoveDevice(props) {
    const [loading, setLoading] = useState(false);

    return <Button variant="danger">Usuń</Button>
}
