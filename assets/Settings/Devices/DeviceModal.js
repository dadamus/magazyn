import React, {useState} from 'react';
import Positions from "../../SharedKernel/Form/Product/PositionsSelector";
import DeviceTypeSelector from "../../SharedKernel/Form/DeviceTypeSelector";
import {post, put} from "../../SharedKernel/Api";
import {toast} from "react-toastify";
import Modal from "../../SharedKernel/Modal";
import {Button, FormCheck} from "react-bootstrap";

export default function DeviceModal(props) {
    const [loading, setLoading] = useState(false);
    const [deviceData, setDeviceData] = useState(props.deviceData ?? {});

    let changeFormData = (name, value) => {
        let newData = {};
        newData[name] = value;

        console.log(deviceData);
        console.log(newData);

        setDeviceData({...deviceData, ...newData})
    }

    let handleSubmit = () => {
        setLoading(true);

        if (deviceData.id) {
            put(
                'api/settings/device',
                deviceData
            ).then((response) => {
                if (response.status !== 200) {
                    throw 'Wystapil blad!';
                }

                return response.json();
            }).then((data) => {
                toast.success('Zapisano!');
                props.refreshList();
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false);
            })
        } else {
            post(
                'api/settings/device',
                deviceData
            ).then((response) => {
                if (response.status !== 200) {
                    throw 'Wystapil blad!';
                }

                return response.json();
            }).then((data) => {
                toast.success('Zapisano!');
                props.refreshList();
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false);
            })
        }
    };

    return <Modal
            loading={loading}
            showButtonText={props.showButtonText}
            saveButton={<Button variant="primary" onClick={handleSubmit} disabled={loading}>Zapisz</Button>}
        >
            <form>
                <div className="form-group">
                    <label htmlFor="name">Nazwa</label>
                    <input
                        type="text"
                        className="form-control"
                        itemID="name"
                        name="name"
                        placeholder="Nazwa"
                        value={deviceData.name}
                        onChange={event => changeFormData('name', event.target.value)}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="type">Tryb</label>
                    <DeviceTypeSelector
                        itemID="type"
                        name="type"
                        placeholder="Wybierz..."
                        onChange={event => changeFormData('type', event.target.value)}
                        value={deviceData.type}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="active">Aktywny</label>
                    <FormCheck
                        id="active"
                        onChange={event => changeFormData('active', event.target.checked)}
                        checked={deviceData.active === 1 || deviceData.active === true}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="position">Stanowisko</label>
                    <Positions
                        itemID="position"
                        name="position"
                        placeholder="Wybierz..."
                        value={deviceData.position_id}
                        onChange={event => changeFormData('position_id', event.target.value)}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="name">Hash</label>
                    <input
                        type="text"
                        className="form-control"
                        itemID="hash"
                        name="hash"
                        placeholder="Hash"
                        value={deviceData.hash}
                        onChange={event => changeFormData('hash', event.target.value)}
                    />
                </div>
            </form>
        </Modal>
    ;
}
