import React, {useState} from 'react';
import {Button, FormCheck} from "react-bootstrap";
import {post, put} from "../../SharedKernel/Api";
import {toast} from "react-toastify";
import Modal from "../../SharedKernel/Modal";

export default function PositionModal(props) {
    const [loading, setLoading] = useState(false);
    const [positionData, setPositionData] = useState(props.positionData ?? {});

    let changeFormData = (name, value) => {
        let newData = {};
        newData[name] = value;

        setPositionData({...positionData, ...newData})
    }

    let handleSubmit = () => {
        setLoading(true);

        if (positionData.id) {
            put(
                'api/settings/position',
                positionData
            ).then((response) => {
                if (response.status !== 200) {
                    throw 'Wystapil blad!';
                }

                return response.json();
            }).then((data) => {
                toast.success('Zapisano!');
                props.refreshList();
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false);
            })
        } else {
            post(
                'api/settings/position',
                positionData
            ).then((response) => {
                if (response.status !== 200) {
                    throw 'Wystapil blad!';
                }

                return response.json();
            }).then((data) => {
                toast.success('Nowe stanowisko zostało dodane!');
                props.refreshList();
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false);
            })
        }
    };

    return <>


        <Modal
            loading={loading}
            showButtonText={props.showButtonText}
            saveButton={<Button variant="primary" onClick={handleSubmit} disabled={loading}>Zapisz</Button>}
        >
            <form>
                <div className="form-group">
                    <label htmlFor="name">Nazwa</label>
                    <input
                        type="text"
                        className="form-control"
                        itemID="name"
                        name="name"
                        placeholder="Nazwa"
                        value={positionData.name}
                        onChange={event => changeFormData('name', event.target.value)}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="desciription">Opis</label>
                    <textarea
                        className="form-control"
                        itemID="desciription"
                        name="desciription"
                        placeholder="Opis"
                        value={positionData.description}
                        onChange={event => changeFormData('description', event.target.value)}
                    />
                </div>
            </form>
        </Modal>
    </>;
}
