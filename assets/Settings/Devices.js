import React, {useEffect, useState} from 'react';
import {Button, Col, Container, Row, Table} from "react-bootstrap";
import DeviceModal from "./Devices/DeviceModal";
import DataTable from "../SharedKernel/DataTable";
import {get} from "../SharedKernel/Api";
import {toast} from "react-toastify";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import RemoveDevice from "./Devices/RemoveDevice";

export default function Devices() {
    const [rows, setRows] = useState([]);
    const [loading, setLoading] = useState(false);

    const columns = [
        {
            data: 'id',
            label: 'ID',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'name',
            label: 'Nazwa',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'device_type_name',
            label: 'Tryb',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'active_label',
            label: 'Aktywny',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'position_name',
            label: 'Stanowisko',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'hash',
            label: 'Hash',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'travel_name',
            label: 'Ostatnia trasa',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'last_sync',
            label: 'Ostatnia synchronizacja',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'last_ip',
            label: 'IP',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'actions',
            label: '',
            searchable: false,
            search: {
                value: null
            }
        },
    ];

    useEffect(() => {
        search();
    }, []);

    const search = () => {
        if (!loading) {
            setLoading(true);
            get(
                'api/settings/devices'
            ).then((response) => {
                return response.json();
            }).then((data) => {
                setRows(dataDecorator(data))
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false)
            });
        }
    }

    const dataDecorator = (data) => {
        return data.map((row) => {

            row.active_label = row.active === 1
                ? <span className='badge bg-success'>Tak</span>
                : <span className='badge bg-danger'>Nie</span>;

            const deviceData = {
                id: row.id,
                active: row.active,
                name: row.name,
                position_id: row.position_id,
                type: row.type,
                hash: row.hash
            };
            row.actions = <>
                <DeviceModal showButtonText="Edytuj" deviceData={deviceData} refreshList={search}/>&nbsp;
                {/*<RemoveDevice/>*/}
            </>
            ;

            return row;
        });
    }

    return <>
        <Row className="subhead">
            <div className="title">
                Ustawienia
                <div className="breadcrumb">
                    Urządzenia
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={12}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Lista urządzeń
                        </div>
                        <div className="toolbar">
                            <DeviceModal
                                refreshList={search}
                                showButtonText={<><FontAwesomeIcon icon={faPlus}/> Dodaj urządzenie</>}
                            />
                        </div>
                    </div>
                    <div className="portlet-body">
                        <DataTable
                            columns={columns}
                            rows={rows}
                            loading={loading}
                            disablePagginator={true}
                        />
                    </div>
                </div>
            </Col>
        </Row>
    </>
        ;
}
