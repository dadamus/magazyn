import React, {useEffect, useState} from 'react';
import {Col, Container, Row, Table} from "react-bootstrap";
import PositionModal from "./Positions/PositionModal";
import DataTable from "../SharedKernel/DataTable";
import {get} from "../SharedKernel/Api";
import {toast} from "react-toastify";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export default function Positions() {
    const [rows, setRows] = useState([]);
    const [loading, setLoading] = useState(false);

    const columns = [
        {
            data: 'id',
            label: 'ID',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'name',
            label: 'Nazwa',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'description',
            label: 'Opis',
            searchable: true,
            search: {
                value: null
            }
        },
        {
            data: 'visible',
            label: 'Aktywny',
            searchable: true,
            search: {
                value: null
            },
            render: (column) => column.row.visible === 1 ? "Tak" : "Nie"
        },
        {
            data: 'actions',
            label: 'Akcje',
            searchable: false,
            search: {
                value: null
            }
        },
    ];

    useEffect(() => {
        search();
    }, []);

    const search = () => {
        if (!loading) {
            setLoading(true);
            get(
                'api/settings/positions'
            ).then((response) => {
                return response.json();
            }).then((data) => {
                setRows(dataDecorator(data))
            }).catch(() => {
                toast.error('Wystąpił błąd!');
            }).finally(() => {
                setLoading(false)
            });
        }
    }

    const dataDecorator = (data) => {
        return data.map((row) => {
            const positionData = {
                id: row.id,
                name: row.name,
                description: row.description
            };

            row.actions = <PositionModal refreshList={search} showButtonText="Edytuj" positionData={positionData}/>

            return row;
        });
    }

    return <>
        <Row className="subhead">
            <div className="title">
                Ustawienia
                <div className="breadcrumb">
                    Stanowiska
                </div>
            </div>
        </Row>
        <Row>
            <Col lg={12}>
                <div className="portlet">
                    <div className="portlet-head">
                        <div className="label">
                            Lista stanowisk
                        </div>
                        <div className="toolbar">
                            <PositionModal refreshList={search} showButtonText={<><FontAwesomeIcon icon={faPlus}/> Dodaj stanowisko</>}/>
                        </div>
                    </div>
                    <div className="portlet-body">
                        <DataTable
                            columns={columns}
                            rows={rows}
                            loading={loading}
                            disablePagginator={true}
                        />
                    </div>
                </div>
            </Col>
        </Row>
    </>;
}
