import React from 'react';

export default function dataTableFiltersTranslator(filters, columns) {
    for (let columnKey in columns) {
        for (let [filterField, filter] of Object.entries(filters.filters)) {
            if (columns[columnKey].data === filterField) {
                columns[columnKey]['search']['value']= filter;
            }
        }
    }

    return columns;
}
