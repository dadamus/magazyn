import React from 'react';

export default function ChartWrapper(props) {
    return <div style={{
        paddingBottom: '20.25%', /* 16:9 */
        position: 'relative',
        height: 0
    }}>
        <div style={{
            position: 'absolute',
            top: '0',
            left: '0',
            width: '100%',
            height: '100%'
        }}>
            {props.children}
        </div>
    </div>
}
