import React, {useState} from 'react';
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import {Col, InputGroup, Row} from "react-bootstrap";
import {faCalendar} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export default function FromToDatePicker(props) {
    const [from, setFrom] = useState(null);
    const [to, setTo] = useState(null);

    let setFromDate = (date) => {
        setFrom(date);
        if (typeof props.setFrom !== "undefined") {
            props.setFrom(date);
        }
    };

    let setToDate = (date) => {
        setTo(date);
        if (typeof props.setTo !== "undefined") {
            props.setTo(date);
        }
    };

    return (
        <Row>
            <Col lg={5}>
                <DatePicker
                    dateFormat="dd-MM-yyyy"
                    className="form-control form-control-sm"
                    placeholderText="Od"
                    selected={from}
                    onChange={date => setFromDate(date)}
                    selectsStart
                    startDate={from}
                    endDate={to}
                />
            </Col>
            <Col lg={2} style={{textAlign: 'center'}}>
                <FontAwesomeIcon icon={faCalendar}/>
            </Col>
            <Col lg={5}>
                <DatePicker
                    dateFormat="dd-MM-yyyy"
                    className="form-control form-control-sm"
                    placeholderText="Do"
                    selected={to}
                    onChange={date => setToDate(date)}
                    selectsEnd
                    startDate={from}
                    endDate={to}
                    minDate={from}
                />
            </Col>
        </Row>
    )
}
