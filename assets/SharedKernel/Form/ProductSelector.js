import React, {useEffect, useState} from 'react';
import AsyncSelect from 'react-select/async';
import AsyncSelector from "./AsyncSelector";
import {get} from "../Api";

export default function ProductSelector(props) {
    let parser = (data) => {
        return data.map((option) => {
            return {label: option.text, value: option.id}
        });
    }

    const promiseOptions = (inputValue) =>
        new Promise((resolve) => {
            get(
                'api/report/product/list?search=' + inputValue
            ).then((response) => {
                return response.json();
            }).then((data) => {
                let parsedOptions = parser(data);

                resolve(parsedOptions);
            });
        });

    return <AsyncSelect loadOptions={promiseOptions} onChange={props.onChange}/>
}
