import React, {useEffect, useState} from "react";
import {Col, Spinner} from "react-bootstrap";

export default function AsyncSelector(props) {
    const [loaded, setLoaded] = useState(true);
    const [options, setOptions] = useState([]);
    const [typingTimeout, setTypingTimeout] = useState(0);

    let getOptions = () => {
        return options.map((option) => {
            return <option value={option.value} key={option.value}>{option.name}</option>
        });
    };

    let onChange = () => {
        if (typingTimeout) {
            clearTimeout(typingTimeout);
        }

        setTypingTimeout(setTimeout(function () {props.onChange(setLoaded, setOptions)}, 500));
    }

    let getSelect = () => {
        if (!loaded) {
            return <Col lg={2}><Spinner animation="grow"/></Col>
        }

        return <select
            className="form-control form-control-sm"
            itemID={props.itemID}
            name={props.name}
            placeholder={props.placeholder}
            onChange={onChange}
            value={props.value ?? ""}
        >
            {getOptions()}
        </select>
    }

    return getSelect();
}
