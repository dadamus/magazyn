import React, {useState} from 'react';
import {Button, Modal as BootstrapModal} from "react-bootstrap";

export default function Modal(props) {
    const [loading, setLoading] = useState(props.loading);
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return <>
        <Button onClick={handleShow}>{props.showButtonText}</Button>

        <BootstrapModal show={show} onHide={handleClose}>
            <BootstrapModal.Header closeButton>
                <BootstrapModal.Title>Uzupełnij dane</BootstrapModal.Title>
            </BootstrapModal.Header>

            <BootstrapModal.Body>
                {props.children}
            </BootstrapModal.Body>

            <BootstrapModal.Footer>
                <Button variant="secondary" onClick={handleClose} disabled={loading}>
                    Zamknij
                </Button>
                {props.saveButton}
            </BootstrapModal.Footer>
        </BootstrapModal>
    </>
}
