import React, {useState} from 'react';
import {Table} from "react-bootstrap";
import {Spinner} from "react-bootstrap";
import DataTablePaginator from "./DataTablePaginator";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSort, faSortDown, faSortUp} from "@fortawesome/free-solid-svg-icons";

export default function DataTable(props) {
    const [orderColumn, setOrderColumn] = useState(null);
    const [sortDirection, setSortDirection] = useState(null);

    const ParsedData = () => {
        if (props.loading) {
            return <tr>
                <td colSpan={Object.entries(props.columns).length} style={{textAlign: 'center'}}><Spinner
                    animation="grow"/></td>
            </tr>;
        }

        if (Object.entries(props.rows).length === 0) {
            return <tr>
                <td colSpan={Object.entries(props.columns).length}
                    style={{textAlign: 'center'}}>{props.messageEmpty ?? "Brak wierszy"}</td>
            </tr>;
        }

        let index = 0;
        let rowNumber = -1;
        return props.rows.map((row) => {
            rowNumber++;

            return <tr>{props.columns.map(column => {
                index++;

                if (typeof column.visible !== "undefined") {
                    if (column.visible === false) {
                        return <></>
                    }
                }

                if (typeof column.render !== "undefined") {
                    return <td key={index}>
                        <column.render row={row} rowNumber={rowNumber}/>
                    </td>
                }

                return <td key={index}>{row[column.data]}</td>
            })}</tr>
        });
    }

    const renderSortButton = (column) => {
        if (orderColumn !== column.data && orderColumn !== null) {
            return <></>;
        }

        if (!column.searchable) {
            return <></>;
        }

        let sortIcon = <FontAwesomeIcon icon={faSort}/>;
        if (sortDirection === 'asc') {
            sortIcon = <FontAwesomeIcon icon={faSortUp}/>;
        } else if (sortDirection === 'desc') {
            sortIcon = <FontAwesomeIcon icon={faSortDown}/>;
        }

        return <div style={{float: 'right', cursor: 'pointer'}} onClick={() => setOrder(column.data)}>
            {sortIcon}
        </div>
    }

    const setOrder = (columnId) => {
        let newSortDirection = null;

        switch (sortDirection) {
            case null:
                newSortDirection = 'asc';
                break;

            case 'asc':
                newSortDirection = 'desc';
                break;
        }

        if (newSortDirection === null) {
            setOrderColumn(null);
            props.filters.order = [];
        } else {
            setOrderColumn(columnId);
            props.filters.order = [
                {
                    column: columnId,
                    dir: newSortDirection
                }
            ];
        }

        setSortDirection(newSortDirection);
        props.search();
    }

    const renderColumns = () => {
        return props.columns.map((column, index) => {
            if (typeof column.visible !== "undefined") {
                if (column.visible === false) {
                    return <></>
                }
            }

            return <th key={index} width={props.columns[index]['width'] ?? 'auto'}>
                <div style={{float: 'left'}}>{props.columns[index]['label']}</div>
                {renderSortButton(props.columns[index])}
            </th>
        });
    };

    const changePage = (page) => {
        props.filters.page = page;

        props.search();
    }

    const renderPagginator = () => {
        if (props.disablePagginator) {
            return <></>
        }

        return <DataTablePaginator page={props.filters.page} draw={props.draw} recordsFiltered={props.recordsFiltered}
                                   changePage={changePage}/>
    }

    return <div className="row">
        <div className="col-lg-12">
            <Table bordered>
                <thead>
                <tr>
                    {renderColumns()}
                </tr>
                </thead>
                <tbody>
                <ParsedData/>
                </tbody>
            </Table>
        </div>
        <div className="col-lg-12">
            <div style={{marginRight: 0, float: 'right'}}>
                {renderPagginator()}
            </div>
        </div>
    </div>;
}
