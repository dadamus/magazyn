import React from 'react';

export default function DataTablePaginator(props) {
    const handleClick = (event, page) => {
        event.stopPropagation();
        event.preventDefault();

        props.changePage(page);
    }

    const renderPages = () => {
        const rows = [];

        let minPage = 1;
        let maxPage = 6;

        if (props.page > 3) {
            minPage = props.page - 3;
            maxPage = props.page + 3;
        }

        const maxRecords = props.recordsFiltered / props.draw;
        if (maxPage > maxRecords) {
            maxPage = Math.ceil(maxRecords);
        }

        for (let i = minPage; i <= maxPage; i++) {
            rows.push(
                <li key={i} className={"page-item" + (props.page == i ? ' active' : '')}>
                    <a className="page-link" href="#" onClick={(e) => handleClick(e, i)}>{i}</a>
                </li>
            );
        }

        return rows;
    }

    const handleFirstPage = (event) => {
        handleClick(event, 1);
    }

    const handleLastPage = (event) => {
        const maxRecords = props.recordsFiltered / props.draw;
        const lastPage = Math.ceil(maxRecords);

        event.stopPropagation();
        event.preventDefault();

        props.changePage(lastPage);
    }

    return <nav aria-label="pagination">
        <ul className="pagination">
            <li className="page-item">
                <a className="page-link" href="#" aria-label="First" onClick={handleFirstPage}>
                    <span aria-hidden="true">&laquo;</span>
                    <span className="sr-only">Previous</span>
                </a>
            </li>
            {renderPages()}
            <li className="page-item">
                <a className="page-link" href="#" aria-label="Last" onClick={handleLastPage}>
                    <span aria-hidden="true">&raquo;</span>
                    <span className="sr-only">Next</span>
                </a>
            </li>
        </ul>
    </nav>
}