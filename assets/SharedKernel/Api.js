import React from 'react';

// const apiUrl = 'http://127.0.0.1:8003/'
const apiUrl = 'http://192.168.100.164/'

export function get(path) {
    const token = localStorage.getItem('token');

    return fetch(
        apiUrl + path,
        {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        }
    ).then((response) => {
        if (response.status === 401) {
            localStorage.removeItem('token');
            window.location.href = "/login";
        }

        return response;
    });
}

export function post(path, data) {
    const token = localStorage.getItem('token');

    let headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    };

    return fetch(
        apiUrl + path,
        {
            method: 'POST',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: headers,
            body: JSON.stringify(data)
        }
    ).then((response) => {
        if (response.status === 401) {
            localStorage.removeItem('token');
            window.location.href = "/login";
        }

        return response;
    });
}

export function put(path, data) {
    const token = localStorage.getItem('token');

    let headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    };

    return fetch(
        apiUrl + path,
        {
            method: 'PUT',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: headers,
            body: JSON.stringify(data)
        }
    ).then((response) => {
        if (response.status === 401) {
            localStorage.removeItem('token');
            window.location.href = "/login";
        }

        return response;
    });
}

export function deleteRequest(path, data) {
    const token = localStorage.getItem('token');

    let headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    };

    return fetch(
        apiUrl + path,
        {
            method: 'DELETE',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: headers,
            body: JSON.stringify(data)
        }
    ).then((response) => {
        if (response.status === 401) {
            localStorage.removeItem('token');
            window.location.href = "/login";
        }

        return response;
    });
}

export function postUnauthorized(path, data) {
    let headers = {
        'Content-Type': 'application/json'
    };

    return fetch(
        apiUrl + path,
        {
            method: 'POST',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: headers,
            body: JSON.stringify(data)
        }
    )
}
