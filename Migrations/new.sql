CREATE TABLE `product_code_storage`
(
    `id`         int(11) unsigned NOT NULL AUTO_INCREMENT,
    `ean`        varchar(13) NOT NULL,
    `product_id` int(11) NOT NULL,
    `user_id`    int(11) NOT NULL,
    `created_at` datetime DEFAULT current_timestamp(),
    PRIMARY KEY (`id`),
    UNIQUE KEY `ean` (`ean`),
    KEY          `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

CREATE TABLE `product_code_usage`
(
    `id`                    int(11) unsigned NOT NULL AUTO_INCREMENT,
    `ean`                   varchar(13) NOT NULL,
    `production_device_id`  int(11) DEFAULT NULL,
    `travel_device_id`      int(11) DEFAULT NULL,
    `travel_id`             int(11) DEFAULT NULL,
    `production_scanned_at` datetime             DEFAULT NULL,
    `travel_scanned_at`     datetime             DEFAULT NULL,
    `created_at`            datetime    NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (`id`),
    UNIQUE KEY `ean` (`ean`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;

CREATE TABLE `product_corrections`
(
    `id`         int(11) unsigned NOT NULL AUTO_INCREMENT,
    `product_id` int(11) NOT NULL,
    `value`      int(11) NOT NULL,
    `user_id`    int(11) NOT NULL,
    `created_at` datetime NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (`id`),
    KEY          `product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
CREATE TABLE `travel_code_storage`
(
    `id`         int(11) unsigned NOT NULL AUTO_INCREMENT,
    `ean`        varchar(13) NOT NULL,
    `travel_id`  int(11) NOT NULL,
    `user_id`    int(11) NOT NULL,
    `created_at` datetime DEFAULT current_timestamp(),
    PRIMARY KEY (`id`),
    UNIQUE KEY `ean` (`ean`),
    KEY          `travel_id` (`travel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
CREATE TABLE `travel_corrections`
(
    `id`         int(11) unsigned NOT NULL AUTO_INCREMENT,
    `product_id` int(11) NOT NULL,
    `travel_id`  int(11) NOT NULL,
    `value`      int(11) NOT NULL,
    `created_at` int(11) NOT NULL,
    `created_by` datetime NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (`id`),
    KEY          `product_id` (`product_id`,`travel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
ALTER TABLE travel_product
    ADD UNIQUE KEY `travel_id` (`travel_id`,`product_id`);
