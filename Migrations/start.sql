# ************************************************************
# Sequel Ace SQL dump
# Version 2077
#
# https://sequel-ace.com/
# https://github.com/Sequel-Ace/Sequel-Ace
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.4.13-MariaDB)
# Database: warehouse
# Generation Time: 2020-09-22 15:45:43 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _47b503d8788f380554802c52c0424f3bb45319f8
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_47b503d8788f380554802c52c0424f3bb45319f8`;

CREATE TABLE `_47b503d8788f380554802c52c0424f3bb45319f8` (
  `no` bigint(20) NOT NULL AUTO_INCREMENT,
  `event_id` char(36) COLLATE utf8mb4_bin NOT NULL,
  `event_name` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `payload` longtext COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`payload`)),
  `metadata` longtext COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`metadata`)),
  `created_at` datetime(6) NOT NULL,
  PRIMARY KEY (`no`),
  UNIQUE KEY `ix_event_id` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table car
# ------------------------------------------------------------

DROP TABLE IF EXISTS `car`;

CREATE TABLE `car` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plate` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visible` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_name_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;

INSERT INTO `category` (`id`, `name`, `visible`)
VALUES
	(2,'test',1);

/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table code
# ------------------------------------------------------------

DROP TABLE IF EXISTS `code`;

CREATE TABLE `code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ean` varchar(13) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code_code_type_id_fk` (`type_id`),
  KEY `code_user_id_fk` (`created_by`),
  CONSTRAINT `code_code_type_id_fk` FOREIGN KEY (`type_id`) REFERENCES `code_type` (`id`),
  CONSTRAINT `code_user_id_fk` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `code` WRITE;
/*!40000 ALTER TABLE `code` DISABLE KEYS */;

INSERT INTO `code` (`id`, `ean`, `type_id`, `created_at`, `created_by`)
VALUES
	(3417,'122092000001',1,'2020-09-22 15:42:56',3),
	(3418,'122092000002',1,'2020-09-22 15:42:56',3);

/*!40000 ALTER TABLE `code` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table code_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `code_type`;

CREATE TABLE `code_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `code_type` WRITE;
/*!40000 ALTER TABLE `code_type` DISABLE KEYS */;

INSERT INTO `code_type` (`id`, `name`)
VALUES
	(1,'production'),
	(2,'travel'),
	(3,'stop'),
	(4,'product_correction'),
	(5,'travel_correction');

/*!40000 ALTER TABLE `code_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table device
# ------------------------------------------------------------

DROP TABLE IF EXISTS `device`;

CREATE TABLE `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT 1,
  `position_id` int(11) NOT NULL,
  `hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_sync` datetime DEFAULT NULL,
  `last_travel_id` int(11) DEFAULT NULL,
  `last_ip` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `device_device_type_id_fk` (`type_id`),
  KEY `device_position_id_fk` (`position_id`),
  CONSTRAINT `device_device_type_id_fk` FOREIGN KEY (`type_id`) REFERENCES `device_type` (`id`),
  CONSTRAINT `device_position_id_fk` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `device` WRITE;
/*!40000 ALTER TABLE `device` DISABLE KEYS */;

INSERT INTO `device` (`id`, `name`, `type_id`, `active`, `position_id`, `hash`, `last_sync`, `last_travel_id`, `last_ip`)
VALUES
	(28,'TEST',1,1,21,'TESTER',NULL,NULL,NULL);

/*!40000 ALTER TABLE `device` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table device_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `device_type`;

CREATE TABLE `device_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `device_type` WRITE;
/*!40000 ALTER TABLE `device_type` DISABLE KEYS */;

INSERT INTO `device_type` (`id`, `name`)
VALUES
	(1,'production'),
	(2,'travel'),
	(3,'stop');

/*!40000 ALTER TABLE `device_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table event_streams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event_streams`;

CREATE TABLE `event_streams` (
  `no` bigint(20) NOT NULL AUTO_INCREMENT,
  `real_stream_name` varchar(150) COLLATE utf8_bin NOT NULL,
  `stream_name` char(41) COLLATE utf8_bin NOT NULL,
  `metadata` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`metadata`)),
  `category` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`no`),
  UNIQUE KEY `ix_rsn` (`real_stream_name`),
  KEY `ix_cat` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `event_streams` WRITE;
/*!40000 ALTER TABLE `event_streams` DISABLE KEYS */;

INSERT INTO `event_streams` (`no`, `real_stream_name`, `stream_name`, `metadata`, `category`)
VALUES
	(3,X'636F64655F70726F6475637473',X'5F34376235303364383738386633383035353438303263353263303432346633626234353331396638',X'5B5D',NULL);

/*!40000 ALTER TABLE `event_streams` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group`;

CREATE TABLE `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_name_uindex` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;

INSERT INTO `group` (`id`, `name`)
VALUES
	(3,'user');

/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table position
# ------------------------------------------------------------

DROP TABLE IF EXISTS `position`;

CREATE TABLE `position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `visible` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `position` WRITE;
/*!40000 ALTER TABLE `position` DISABLE KEYS */;

INSERT INTO `position` (`id`, `name`, `description`, `visible`)
VALUES
	(21,'test','',1);

/*!40000 ALTER TABLE `position` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table print_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `print_group`;

CREATE TABLE `print_group` (
  `id` varchar(255) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  KEY `print_group_id_idx` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `print_group` WRITE;
/*!40000 ALTER TABLE `print_group` DISABLE KEYS */;

INSERT INTO `print_group` (`id`, `code`)
VALUES
	('0db3155d-9d4a-4013-b3c0-4857b3e706e3','122092000001'),
	('0db3155d-9d4a-4013-b3c0-4857b3e706e3','122092000002');

/*!40000 ALTER TABLE `print_group` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_category_id_fk` (`category_id`),
  KEY `product_type_id_fk` (`type_id`),
  CONSTRAINT `product_category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `product_type_id_fk` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;

INSERT INTO `product` (`id`, `name`, `description`, `category_id`, `type_id`)
VALUES
	(9,'test','',2,2);

/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product_code_view
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_code_view`;

CREATE TABLE `product_code_view` (
  `code_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `production_scanned_at` datetime DEFAULT NULL,
  `production_device_id` int(11) DEFAULT NULL,
  `travel_scanned_at` datetime DEFAULT NULL,
  `travel_device_id` int(11) DEFAULT NULL,
  `travel_id` int(11) DEFAULT NULL,
  UNIQUE KEY `product_code_view_code_id_uindex` (`code_id`),
  KEY `product_code_view_device_id_fk` (`production_device_id`),
  KEY `product_code_view_device_id_fk_2` (`travel_device_id`),
  KEY `product_code_view_product_id_fk` (`product_id`),
  KEY `product_code_view_travel_id_fk` (`travel_id`),
  CONSTRAINT `product_code_view_code_id_fk` FOREIGN KEY (`code_id`) REFERENCES `code` (`id`),
  CONSTRAINT `product_code_view_device_id_fk` FOREIGN KEY (`production_device_id`) REFERENCES `device` (`id`),
  CONSTRAINT `product_code_view_device_id_fk_2` FOREIGN KEY (`travel_device_id`) REFERENCES `device` (`id`),
  CONSTRAINT `product_code_view_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `product_code_view_travel_id_fk` FOREIGN KEY (`travel_id`) REFERENCES `travel` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product_code_view` WRITE;
/*!40000 ALTER TABLE `product_code_view` DISABLE KEYS */;

INSERT INTO `product_code_view` (`code_id`, `product_id`, `production_scanned_at`, `production_device_id`, `travel_scanned_at`, `travel_device_id`, `travel_id`)
VALUES
	(3417,9,NULL,NULL,NULL,NULL,NULL),
	(3418,9,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `product_code_view` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product_position
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_position`;

CREATE TABLE `product_position` (
  `product_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  KEY `product_position_position_id_fk` (`position_id`),
  KEY `product_position_product_id_fk` (`product_id`),
  CONSTRAINT `product_position_position_id_fk` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`),
  CONSTRAINT `product_position_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product_position` WRITE;
/*!40000 ALTER TABLE `product_position` DISABLE KEYS */;

INSERT INTO `product_position` (`product_id`, `position_id`)
VALUES
	(9,21);

/*!40000 ALTER TABLE `product_position` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product_state_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_state_log`;

CREATE TABLE `product_state_log` (
  `product_id` int(11) DEFAULT NULL,
  `code_id` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  KEY `code_product_logs_code_id_fk` (`code_id`),
  KEY `code_product_logs_device_id_fk` (`device_id`),
  KEY `product_state_log_product_id_fk` (`product_id`),
  CONSTRAINT `code_product_logs_code_id_fk` FOREIGN KEY (`code_id`) REFERENCES `code` (`id`),
  CONSTRAINT `code_product_logs_device_id_fk` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`),
  CONSTRAINT `product_state_log_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table projections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projections`;

CREATE TABLE `projections` (
  `no` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_bin NOT NULL,
  `position` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`position`)),
  `state` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`state`)),
  `status` varchar(28) COLLATE utf8_bin NOT NULL,
  `locked_until` char(26) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`no`),
  UNIQUE KEY `ix_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table travel
# ------------------------------------------------------------

DROP TABLE IF EXISTS `travel`;

CREATE TABLE `travel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_id` int(11) NOT NULL,
  `car_id` int(11) DEFAULT NULL,
  `started_at` datetime DEFAULT NULL,
  `ended_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `travel_car_id_fk` (`car_id`),
  KEY `travel_user_id_fk` (`author_id`),
  KEY `travel_user_id_fk_2` (`modified_by`),
  CONSTRAINT `travel_car_id_fk` FOREIGN KEY (`car_id`) REFERENCES `car` (`id`),
  CONSTRAINT `travel_user_id_fk` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`),
  CONSTRAINT `travel_user_id_fk_2` FOREIGN KEY (`modified_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table travel_code
# ------------------------------------------------------------

DROP TABLE IF EXISTS `travel_code`;

CREATE TABLE `travel_code` (
  `travel_id` int(11) NOT NULL,
  `code_id` int(11) NOT NULL,
  UNIQUE KEY `travel_code_code_id_uindex` (`code_id`),
  UNIQUE KEY `travel_code_travel_id_uindex` (`travel_id`),
  CONSTRAINT `travel_code_code_id_fk` FOREIGN KEY (`code_id`) REFERENCES `code` (`id`),
  CONSTRAINT `travel_code_travel_id_fk` FOREIGN KEY (`travel_id`) REFERENCES `travel` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table travel_product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `travel_product`;

CREATE TABLE `travel_product` (
  `travel_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `planned` int(11) DEFAULT NULL,
  KEY `travel_product_product_id_fk` (`product_id`),
  KEY `travel_product_travel_id_fk` (`travel_id`),
  CONSTRAINT `travel_product_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `travel_product_travel_id_fk` FOREIGN KEY (`travel_id`) REFERENCES `travel` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table travel_report
# ------------------------------------------------------------

DROP TABLE IF EXISTS `travel_report`;

CREATE TABLE `travel_report` (
  `travel_id` int(11) DEFAULT NULL,
  `reported` tinyint(1) DEFAULT 0,
  `text` varchar(255) DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  KEY `travel_report_travel_id_fk` (`travel_id`),
  KEY `travel_report_user_id_fk` (`modified_by`),
  CONSTRAINT `travel_report_travel_id_fk` FOREIGN KEY (`travel_id`) REFERENCES `travel` (`id`),
  CONSTRAINT `travel_report_user_id_fk` FOREIGN KEY (`modified_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `type`;

CREATE TABLE `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visible` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type_name_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `type` WRITE;
/*!40000 ALTER TABLE `type` DISABLE KEYS */;

INSERT INTO `type` (`id`, `name`, `visible`)
VALUES
	(2,'test',1);

/*!40000 ALTER TABLE `type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `login`, `password`, `name`)
VALUES
	(3,'admin','21232f297a57a5a743894a0e4a801fc3','admin');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_group`;

CREATE TABLE `user_group` (
  `user_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  KEY `user_group_group_id_fk` (`group_id`),
  KEY `user_group_user_id_fk` (`user_id`),
  CONSTRAINT `user_group_group_id_fk` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`),
  CONSTRAINT `user_group_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;

INSERT INTO `user_group` (`user_id`, `group_id`)
VALUES
	(3,3);

/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
